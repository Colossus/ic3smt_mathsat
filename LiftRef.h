/*
 * ConsecutionRefinement.h
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#ifndef LiftRef_H_
#define LiftRef_H_

#include "CTIGAR.h"

using namespace std;

namespace CTIGAR {

class LiftRef: public CTIGAR {

public:
	LiftRef(Model & model, AbstractDomain & d, Options & opt);

	virtual ~LiftRef();

protected:
	virtual bool checkCEXTrace(void);

	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls) = 0;

	virtual void backtrackRefine(const Obligation & obl, PriorityQueue & obls,
			bool enforceRefinement = true);

	void elimSpuriousTrans(size_t st);

	virtual bool interpolate(TermPtr & i, size_t st);

	bool updateAbstraction(size_t st);

	virtual bool liftAppropriate(AbstractState & lifted,
			const AbstractState & full, const size_t st, const size_t succ);

};

}

#endif /* CONSECUTIONREFINEMENT_H_ */
