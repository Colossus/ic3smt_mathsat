int main() {
  int n;
  int[] dyn;
  int[2] fixed;
  int sum, nondet, i;

  assume(n > 1);

  dyn = malloc(n);

  dyn[1] = 7;
  fixed[1] = 2;

  n = n + 1;
  dyn = realloc(n);

  nondet = *;
  assume(nondet >= 0);
  assume(nondet < n);
  assume(dyn[nondet] <= 10);
  assume(dyn[nondet] >= 0);
  nondet = 0;
  assume(nondet >= 0);
  assume(nondet < 3);
  assume(fixed[nondet] <= 10);
  assume(fixed[nondet] >= 0);
  i = 0;
  sum = 0;
  while (i < n) {
    sum = sum + dyn[i];
    ++i;
  }
  i = 0;
  while (i < 3) {
    sum = sum + fixed[i];
    ++i;
  }
  assert(sum <= 29);
}
