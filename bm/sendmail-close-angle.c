/*
 * Variant: This one just blindly copies the input into buffer and writes '>''\0' at the end.
 */

main()
{
  int in;
  int inlen;
  int bufferlen;
  int buf;
  int buflim;

  if(bufferlen <= 1) return 0;
  if(inlen <= 0) return 0;
  if(bufferlen >= inlen) return 0;
  buf = 0;
  in = 0;
  buflim = bufferlen - 2;
  while (*)
  {
    if (buf == buflim)
      break;
    // assert(0<=buf);
    // assert(buf<bufferlen); 
    buf++;
    in++;
    // assert(0<=in);
    assert(in<inlen);
  }

  // assert(0<=buf);
  // buf++;
  // assert(buf<bufferlen);
}
