int main() {
  int[2] arr;
  int sum, i;
  arr[0] = 5;
  arr[1] = 10;
  i = 0;
  sum = 0;
  while (i < 2) {
    sum = sum + arr[i];
    ++i;
  }
  assert(sum == 14);
}
