main() {
  int i, a;
  i = 0;
  a = 0;

  while (1) {
    if (i == 20) {
       break;
    } else {
       i++;
       a++;
    }

    assert (i == a);
  }

  assert (a == 20);
}

