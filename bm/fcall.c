int a, b;

int g(int y) {
  return 3+y*6;
}

int f(int a, int x) {
  int rv;
  int y, z;
  b = 3 * a;
  rv = g(b);
  assume(b > 4);
  return rv + b;
}

int main() {
  int rv;
  b = 4;
  rv = f(b+22,3);
  if (rv > 5) {
    assert(rv > 4);
  }
  while (a > 5) {
    while (*) {
      b = b - 4;
      if (*) break;
    }
    if (a > 3) {
      a = 0;
      if (b == 3) {
        return 0;
      } else {
        if (*) {
	  assume(a >= b);
	}
      }
    } else {
      b = 3;
      if (a == 5) {
        b = 7;
      }
    }
  }
  f(4,5);
  rv = 6;
}
