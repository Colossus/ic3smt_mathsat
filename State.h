/*
 * State.h
 *
 *  Created on: Jan 8, 2014
 *      Author: Johannes Birgmeier
 */

#ifndef STATE_H_
#define STATE_H_

#include "Assignment.h"
#include "Model.h"
#include "AbstractDomain.h"

using namespace std;

namespace CTIGAR {

// The State structures are for tracking trees of (lifted) CTIs.
// Because States are created frequently, I want to avoid dynamic
// memory management; instead their (de)allocation is handled via
// a vector-based pool.
struct State {
	AbstractState liftedAst; // lifted ast of full AsgnVec
	AbstractState fullAst;   // most precise ast of full AsgnVec
	size_t domainVersion;
	AbstractDomain * domain;

	size_t successor;  // successor State
	AsgnVec full;	   // complete latch assignment
	AsgnVec inputs;    // assignments of input variables
	AsgnVec lifted;	// lifted latch assignment that will lead into lifted successor
	size_t index;      // for pool
	bool used;         // for pool

	bool spuriousSucc;
	size_t spuriousLevel;
	size_t nSpurious; // number of spurious transitions in trace starting from this state

	State();

	void clear();

	void setAbstraction(AbstractState & full, AbstractState & lifted,
			AbstractDomain * domain);

	bool outdated(void);

	AbstractState & bestAbstraction();

	bool hasLiftedAst();

	size_t getPcVal();

	friend std::ostream & operator<<(std::ostream & stream, const State & asgn);

	bool liftedHasArray();

};

class StateManager {

protected:
	vector<State> states;

	size_t nextState;

public:
	StateManager(void) :
			nextState(0) {
	}

	State & state(size_t sti);

	size_t newState();

	void delState(size_t sti);

	void resetStates();

};

}

#endif /* STATE_H_ */
