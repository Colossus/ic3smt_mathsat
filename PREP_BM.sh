#! /usr/bin/zsh

rm -rf benchmarks/*.vars
rm -rf benchmarks/*.nondet
rm -rf benchmarks/*.asrts
rm -rf benchmarks/*.abstr
rm -rf benchmarks/*.tr
rm -rf benchmarks/*.trx
rm -rf benchmarks/*.init

for i in `ls benchmarks`
do
  echo $i
  echo $i >&2
  ./trgen2 benchmarks/$i
done
