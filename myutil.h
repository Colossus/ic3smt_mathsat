/*
 * util.h
 *
 *  Created on: Jul 3, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef MYUTIL_H_
#define MYUTIL_H_

#include <set>
#include <string>
#include <assert.h>

using namespace std;

namespace CTIGAR {

void split(set<string> & elems, const string & s, char delim);

bool isNumber(string str);

template<class Set1, class Set2>
bool is_disjoint(const Set1 &set1, const Set2 &set2) {
	if (set1.empty() || set2.empty())
		return true;

	typename Set1::const_iterator it1 = set1.begin(), it1End = set1.end();
	typename Set2::const_iterator it2 = set2.begin(), it2End = set2.end();

	if (*it1 > *set2.rbegin() || *it2 > *set1.rbegin())
		return true;

	while (it1 != it1End && it2 != it2End) {
		if (*it1 == *it2)
			return false;
		if (*it1 < *it2) {
			it1++;
		} else {
			it2++;
		}
	}

	return true;
}

template<class Collection1, class Set2>
long find_one(const Collection1 &c1, const Set2 &set2) {
	for (typename Collection1::const_iterator it = c1.begin(); it != c1.end(); ++it) {
		if (set2.find(*it) != set2.end())
			return *it;
	}
	assert(false);
	return 0;
}

}

#endif /* UTIL_H_ */
