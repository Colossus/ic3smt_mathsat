assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

main ()
{
  int x;
  int y;
  int z;

  x = y = 0;

  while (nondet()){
    x = x + 10;
    y = y + 1;
  }

  assert (x > z || y < z + 1);
}
