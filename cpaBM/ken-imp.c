assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

void main() {
  int i;
  int j;
  int x;
  int y;
  x = i;
  y = j;
  while(x!=0) {
	x--;
	y--;
  }
  assert(i!=j && y == 0);
}
