assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

void main() {
  int i,j,k,n;

  if( k != n) return 0;

  i = 0;
  while (i<n) {
    j = 2*i;
    while (j<n) {
      if(nondet()) {
        k = j;
	while (k<n) {
	  assert(k>=2*i);
	  k++;
	}
      }
      else {
	assert( k >= n );
	assert( k <= n );
      }
      j++;
    }
    i++;
  }
}
