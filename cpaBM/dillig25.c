assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

void main()
{
  int x;
  int y;
  int i;
  int j;
  x = y = i = j = 0;

  while(nondet())
  {
    while(nondet())
    {
       if(x==y)
          i++;
       else
          j++;
    }
    if(i>=j)
    {
       x++;
       y++;
    }
    else
       y++;
  }
  assert(i > j-1);
}
