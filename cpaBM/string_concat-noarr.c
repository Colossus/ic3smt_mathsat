assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

int main() {
  int i, j;
  i = 0;
  while(nondet()){
    i++;
  }
  if(i >= 100) return 0;
  j = 0;
  while(nondet()){
    i++;
    j++;
  }
  if(j >= 100) return 0;
  assert( i < 200 );
  return 0;
}
