assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

/*
 * IC3 motivating example
 */ 

void main()
{
 int x; int y;
 int t1, t2;
 x = y = 1;
 while(nondet()) {
   t1 = x;
   t2 = y;
   x = t1+ t2;
   y = t1 + t2;
 }
 assert(y >= 1);
}
