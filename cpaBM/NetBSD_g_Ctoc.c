assert(int x) {
  if (!x) { ERROR:; }
}

int nondet() {
  int x;
  return x;
}

int base_sz;

int main ()
{
  int i;
  int j;
  int len;
  len = base_sz;

  if(base_sz <= 0) return 0;

  assert( 0 <= base_sz-1 );

  if (len == 0)
    return 0; 
  
  i = 0;
  j = 0;
  while (1) {
    if ( len == 0 ){ 
      return 0;
    } else {
      assert(0<= j); assert(j < base_sz);
      assert(0<= i); assert(i < base_sz );
      if (nondet()) {
        i++;
        j++;
        return 0;
      }
    }
    i++;
    j++;
    len--;
  }

  return 0;
}

