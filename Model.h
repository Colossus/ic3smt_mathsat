/*
 * Model.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <set>
#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <algorithm>

#include "Assignment.h"
#include "SMTEnvironment.h"
#include "Term.h"
#include "KarrCFG.h"
#include "main.h"

using namespace std;

namespace CTIGAR {

typedef set<AbstractState, AbstractStateComp> CubeSet;

std::ostream & operator<<(std::ostream & stream, const AbstractState & st);

std::ostream & operator<<(std::ostream & stream, const AsgnVec & st);

std::ostream & operator<<(std::ostream & stream, const AsgnSet & asgnSet);

std::ostream & operator<<(std::ostream & stream, const CubeSet & st);

class RedundantTermException: public exception {
};

class Predicate {

	SMTEnvironment * env;

	TermPtr term;
	TermPtr boolVar;
	TermPtr boolTermEquiv;
	string boolVarName;
	size_t idx;
	set<string> vars;
	// all supporters must have an index < this.idx
	set<size_t> supporters;

public:

	Predicate(SMTEnvironment * env1, TermPtr term1, size_t idx1,
			const set<size_t> & supporters) :
			env(env1), term(term1), idx(idx1), supporters(supporters) {
		boolVarName = "domain.boolVar." + to_string(idx);
		env->declareBoolVar(boolVarName, false);
		TermFactory tf;
		boolVar = tf.mkBoolVar(boolVarName);
		boolTermEquiv = tf.mkAnd(tf.mkOr(tf.mkNot(boolVar), term),
				tf.mkOr(boolVar, tf.mkNot(term)));
		term->findVars(vars);
		assert(supporters.empty() || *supporters.rbegin() < idx);
	}

	friend std::ostream & operator<<(std::ostream & stream,
			const Predicate & v) {
		stream << v.idx << ": " << *v.term;
		return stream;
	}

	const set<size_t> & getSupporters() const {
		return supporters;
	}

	TermPtr getTerm(void) const {
		return term;
	}

	TermPtr getBoolVar(void) const {
		return boolVar;
	}

	TermPtr getBoolTermEquiv(void) const {
		return boolTermEquiv;
	}

	const string & getBoolVarName(void) const {
		return boolVarName;
	}

	size_t getIdx(void) const {
		return idx;
	}

	const set<string> & getVars(void) const {
		return vars;
	}

};

typedef map<string, set<string>> ConjunctMap;
typedef map<long, ConjunctMap> CFGMap;
typedef map<long, set<long>> PredecessorMap;

struct VarInfo {
	VarTypeMap all;
	VarTypeMap program;
	VarTypeMap inputs;
	VarPrimeMap toPrime;
	VarPrimeMap fromPrime;
	FixedSizeMap fixedArraySize;
	DynSizeMap dynArraySize;
	InputSizeMap inputArraySize;
};

class Model {
	vector<string> tr;
	string asrt;
	TermVec initial;
	TermVec properties;
	TermVec recommendedAbstractions;
	TermVec trxs;
	set<string> trStrings;
	map<long, vector<Affine>> lines;

	PredecessorMap preds;
	set<long> errorPreds;
	CFGMap cfgMap;
	long maxPC;
	set<string> errorVars;
	bool hasArrays;

	KarrCFGFactory * karrFactory;
	Karr * karr;

	TermFactory tf;

	Options & opt;

public:
	VarInfo vars;

	Model(string progPrefix, Options & opt);
	virtual ~Model();

	bool getHasArrays() {
		return hasArrays;
	}

	const TermVec & getRecommendedAbstractions(void) const {
		return recommendedAbstractions;
	}

	TermPtr getError(void) const;

	TermPtr getProperty(void) const;

	const TermVec & getInitialCondition(void) const;

	TermPtr getPrimedError(void) const;

	void loadInitialCondition(SMTEnvironment * env);

	void loadProperty(SMTEnvironment * env);

	void loadError(SMTEnvironment * env);

	void loadPrimedProperty(SMTEnvironment * env);

	void loadPrimedError(SMTEnvironment * env);

	TermPtr primeTerm(TermPtr) const;

	TermPtr unprimeTerm(TermPtr term) const;

	bool isUnprimedVariable(string var) const;

	bool isProgramCounter(string var) const;

	bool hasBoundVars(const TermPtr idx) const;

	void getPredecessors(set<long> & ret, long minPC, long maxPC);

	void safeArrayAccess(TermVec & safetyConditions, const TermPtr predicate);

	const set<long> & getErrorPredecessors(void) {
		return errorPreds;
	}

	const set<string> & getErrorVars() {
		return errorVars;
	}

	CFGMap & getCFGMap(void) {
		return cfgMap;
	}

	long getMaxPC() {
		return maxPC;
	}

	bool isInputVariable(string var) const {
		return vars.inputs.find(var) != vars.inputs.end();
	}

	const set<string> & getTRStrings(void) {
		return trStrings;
	}

	const map<long, vector<Affine>> & getLines(void) {
		return lines;
	}

	const TermVec & getTrxs(void) {
		return trxs;
	}

protected:

	void loadTrFile(ifstream & trFile);

	void loadAsrtsFile(ifstream & asrtsFile, SMTEnvironment * parser);

	void loadVarsFile(ifstream & varsFile);

	void loadNondetFile(ifstream & varsFile);

};

typedef map<TermPtr, long> PredicateMap;

typedef map<size_t, Assignment> AsgnMap;

}

#endif /* MODEL_H_ */
