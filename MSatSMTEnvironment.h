/*
 * SMTWrapper.h
 *
 *  Created on: Sep 5, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef SMTWRAPPER_H_
#define SMTWRAPPER_H_

#include <map>
#include <mathsat.h>
#include <set>

#include "SMTEnvironment.h"
#include "Assignment.h"

using namespace std;

namespace CTIGAR {

class AllSatCallback {
public:
	virtual ~AllSatCallback() {
	}
	virtual int callback(const SatModel & nextModel) const = 0;
};

class MSatSMTEnvironment;

struct MSatCallbackData {
	const AllSatCallback * cb;
	const msat_env * env;
	size_t * nQuery;
	MSatSMTEnvironment * smtEnv;
};

struct MSatAllSatData {
	const msat_env * env;
	set<SatModel> * mods;
};

typedef map<TermPtr, msat_term> ToMSatMap;
typedef map<size_t, TermPtr> FromMSatMap;

typedef tuple<size_t, long, long> ArrayWrite;
typedef map<long, long> ArrayReads;
typedef vector<ArrayWrite> ArrayWrites;

class MSatSMTEnvironment: public SMTEnvironment {

	msat_env env;

protected:
	VarTypeMap declaredVars;
	VarTypeMap modelVars;
	vector<msat_term> boolConsts;

	ToMSatMap toMSat;

	msat_term toMSatTerm(const TermPtr term);

	TermPtr fromMSatTerm(msat_term term);

	msat_term mkTrue(void) const;

	msat_term mkFalse(void) const;

	msat_term mkIntVar(string name) const;

	msat_term mkArrayVar(string name) const;

	msat_term mkInt(const long l) const;

	msat_term mkIntModCongr(long modulus, msat_term t1, msat_term t2);

	msat_term mkArraySelect(msat_term var, msat_term idx) const;

	msat_term mkArrayStore(msat_term var, msat_term idx, msat_term val) const;

	msat_term mkBoolVar(string name) const;

	msat_term mkErrorTerm(void) const;

	msat_term mkNot(msat_term term) const;

	msat_term mkOr(size_t size, msat_term * args) const;

	msat_term mkAnd(size_t size, const msat_term * args) const;

	msat_term mkOr(msat_term arg1, msat_term arg2) const;

	msat_term mkAnd(msat_term arg1, msat_term arg2) const;

	msat_term mkComparison(ComparisonType::E kind, msat_term arg0,
			msat_term arg1) const;

	msat_term mkPlus(msat_term arg1, msat_term arg2) const;

	msat_term mkTimes(msat_term arg1, msat_term arg2) const;

	msat_term copyFrom(const MSatSMTEnvironment * src, msat_term term) const;

	string varName(msat_term term) const;

	bool termIsKind(msat_term term, TermType::E kind) const;

	bool termIsVar(msat_term term) const;

	bool termIsArrayRead(msat_term term) const;

	bool termIsArrayWrite(msat_term term) const;

	bool termIsNumeral(msat_term term) const;

	bool termIsModCongr(msat_term term) const;

	bool termIsTrue(msat_term term) const;

	bool termIsFalse(msat_term term) const;

	void getArgs(vector<msat_term> & args, msat_term term) const;

	long termToLong(msat_term term) const;

	bool isErrorTerm(msat_term term) const;

	msat_term copyApp(msat_term term, msat_term * args) const;

	void findVars(set<string> & occuringVars, msat_term term) const;

	void termToNumber(mpz_t * number, msat_term term) const;

	void loadNamedTerms(map<string, msat_term> & namedMap,
			const string & namedStr) const;

	string numeralToString(msat_term term) const;

	msat_term termGetArg(msat_term term, size_t idx) const;

	msat_term getArg(msat_term term, size_t idx) const;

	void assertedTerms(vector<msat_term> & ret) const;

	string termToString(msat_term term) const;

	void getAllModels(vector<msat_term> & important, const AllSatCallback & cb);

	size_t termId(msat_term term) const;

public:

	MSatSMTEnvironment(string name, const VarTypeMap & vars,
			const Options & opt, bool model = false, bool core = false,
			bool interpolate = false);

	~MSatSMTEnvironment();

	virtual TermPtr parsePredicate(const string & str);

	TermPtr parseString(const string & str);

	virtual void assertFormula(TermPtr ast);

	virtual size_t assertAndTrack(TermPtr ast);

	void assertFormulas(vector<size_t> & ids, const TermVec & ts);

	virtual size_t assertFormula(TermPtr ast, TermPtr actLit);

	virtual bool sat(void);

	virtual bool sat(const TermVec & assumptions);

	virtual void getModel(SatModel & mod, bool full);

	virtual void printAssertedTerms(void) const;

	TermPtr mkActLit(void);

	TermPtr fromSmtLib2(string str);

	virtual void release(TermPtr actLit);

	virtual void push(void);

	virtual void pop(void);

	void getUnsatAssumps(vector<size_t> & assumps) const;

	void getUnsatCore(vector<size_t> & core) const;

	virtual void getBoolModel(SatModel & mod, set<string> & important);

	size_t termNumArgs(msat_term term) const;

	void setItpGroup(int group) const;

	int createItpGroup(void) const;

	bool interpolate(TermPtr & i, int ante);

	virtual void declareIntVar(string varName, bool inModel);

	virtual void declareBoolVar(string varName, bool inModel);

	void declareFixedArray(string varName, bool inModel);

	void declareDynArray(string varName, bool inModel);

	void declareInputArray(string varName, bool inModel);

	void printModel(void);

};

}

#endif /* SMTWRAPPER_H_ */
