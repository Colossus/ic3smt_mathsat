/*
 * Assignment.cpp
 *
 *  Created on: Aug 29, 2013
 *      Author: Johannes Birgmeier
 */

#include "Assignment.h"

#include <iostream>

using namespace std;

namespace CTIGAR {

bool operator==(const Assignment & asgn1, const Assignment & asgn2) {
	if (asgn1.getType() != asgn2.getType())
		return false;
	switch (asgn1.getType()) {
	case AsgnType::INT_ASGN:
		return asgn1.getNumVal() == asgn2.getNumVal();
	case AsgnType::BOOL_ASGN:
		return asgn1.isTrue() == asgn2.isTrue();
	case AsgnType::ARRAY_ASGN: {
		const vector<long> & arr1 = asgn1.getArrayVal();
		const vector<long> & arr2 = asgn2.getArrayVal();
		if (arr1.size() != arr2.size())
			return false;
		vector<long>::const_iterator it1 = arr1.begin();
		vector<long>::const_iterator it2 = arr2.begin();
		for (; it1 != arr1.end(); ++it1) {
			if (*it1 != *it2)
				return false;
			++it2;
		}
		return true;
	}
	default:
		assert(false);
		return false;
	}
}

bool operator<(const Assignment & asgn1, const Assignment & asgn2) {
	if (asgn1.getVar() != asgn2.getVar()) {
		return asgn1.getVar() < asgn2.getVar();
	}
	if (asgn1.getType() != asgn2.getType())
		return asgn1.getType() < asgn2.getType();
	if (asgn1.getType() == AsgnType::DYN_ARRAY_ASGN
			&& asgn1.getArraySizeVar() != asgn2.getArraySizeVar()) {
		return asgn1.getArraySizeVar() < asgn2.getArraySizeVar();
	}
	switch (asgn1.getType()) {
	case AsgnType::INT_ASGN:
		return asgn1.getNumVal() < asgn2.getNumVal();
	case AsgnType::BOOL_ASGN:
		return asgn1.isTrue() < asgn2.isTrue();
	case AsgnType::ARRAY_ASGN:
	case AsgnType::DYN_ARRAY_ASGN: {
		const vector<long> & arr1 = asgn1.getArrayVal();
		const vector<long> & arr2 = asgn2.getArrayVal();
		if (arr1.size() != arr2.size())
			return arr1.size() < arr2.size();
		vector<long>::const_iterator it1 = arr1.begin();
		vector<long>::const_iterator it2 = arr2.begin();
		for (; it1 != arr1.end(); ++it1) {
			if (*it1 != *it2)
				return *it1 < *it2;
			++it2;
		}
		return false;
	}
	default:
		assert(false);
		return false;
	}
}

std::ostream & operator<<(std::ostream & stream, const Assignment & asgn) {
	stream << asgn.getVar() << "=";
	switch (asgn.getType()) {
	case AsgnType::INT_ASGN:
		return stream << asgn.getNumVal();
	case AsgnType::BOOL_ASGN:
		return stream << (asgn.isTrue() ? "true" : "false");
	case AsgnType::ARRAY_ASGN: {
		stream << "[";
		const vector<long> & arr = asgn.getArrayVal();
		for (vector<long>::const_iterator it = arr.begin(); it != arr.end();
				++it) {
			stream << *it << ", ";
		}
		return stream << "]";
	}
	case AsgnType::DYN_ARRAY_ASGN: {
		stream << "[";
		const vector<long> & arr = asgn.getArrayVal();
		for (vector<long>::const_iterator it = arr.begin(); it != arr.end();
				++it) {
			stream << *it << ", ";
		}
		stream << "]";
		return stream << "/" << asgn.getArraySize();
	}
	default:
		assert(false);
		return stream;
	}
}

Assignment::Assignment(string var1, mpz_t val1) :
		var(var1), asgnType(AsgnType::INT_ASGN), boolVal(false), arraySize(0) {
	assert(mpz_fits_slong_p(val1));
	this->numVal = mpz_get_si(val1);
}

Assignment::Assignment(string var1, long val1) :
		var(var1), asgnType(AsgnType::INT_ASGN), boolVal(false), numVal(val1), arraySize(
				0) {
}

Assignment::Assignment(string var1, bool val1) :
		var(var1), asgnType(AsgnType::BOOL_ASGN), boolVal(val1), numVal(0), arraySize(
				0) {
}

Assignment::Assignment(string var1, const map<long, long> & arrayVal1) :
		var(var1), asgnType(AsgnType::ARRAY_ASGN), boolVal(false), numVal(0), arraySize(
				0) {
	for (map<long, long>::const_iterator it = arrayVal1.begin();
			it != arrayVal1.end(); ++it) {
		if (it->first >= (long) arrayVal.size())
			arrayVal.resize(it->first + 1);
		if (it->first >= 0)
			arrayVal[(size_t) it->first] = it->second;
	}
}

Assignment::Assignment(string var1, const map<long, long> & arrayVal1,
		string sizeVar, long arraySize) :
		var(var1), asgnType(AsgnType::DYN_ARRAY_ASGN), boolVal(false), numVal(
				0), sizeVar(sizeVar), arraySize(arraySize) {
	for (map<long, long>::const_iterator it = arrayVal1.begin();
			it != arrayVal1.end(); ++it) {
		if (it->first >= (long) arrayVal.size() && it->first < arraySize)
			arrayVal.resize(it->first + 1);
		if (it->first >= 0 && it->first < arraySize)
			arrayVal[(size_t) it->first] = it->second;
	}
}

void classifyPoints(map<set<string>, set<AsgnSet> > & pointClasses,
		const set<AsgnSet> points) {
	for (set<AsgnSet>::const_iterator it = points.begin(); it != points.end();
			++it) {
		set<string> vars;
		for (AsgnSet::const_iterator it2 = it->begin(); it2 != it->end();
				++it2) {
			vars.insert(it2->getVar());
		}
		pointClasses[vars].insert(*it);
	}
}

}
