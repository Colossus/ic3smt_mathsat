/*
 * VarType.cpp
 *
 *  Created on: Feb 28, 2014
 *      Author: Johannes Birgmeier
 */

#include "VarType.h"

namespace CTIGAR {

VarType::E typeFromString(const string & typeString) {
	VarType::E type = VarType::NONE;
	if (typeString == "int") {
		type = VarType::INT;
	} else if (typeString == "bool") {
		type = VarType::BOOL;
	} else if (typeString == "dyn_array") {
		type = VarType::DYN_ARRAY;
	} else if (typeString == "fixed_array") {
		type = VarType::FIXED_ARRAY;
	} else if (typeString == "input_array") {
		type = VarType::INPUT_ARRAY;
	} else {
		assert(false && "only int, bool, fixed_array and dyn_array "
				"types supported for variables");
	}
	return type;
}

}
