/*
 * LiftRefLAF.h
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#ifndef LiftRefLAF_H_
#define LiftRefLAF_H_

#include "LiftRef.h"

using namespace std;

namespace CTIGAR {

class LiftRefLAF: public LiftRef {

public:
	LiftRefLAF(Model & model, AbstractDomain & d, Options & opt);

	virtual ~LiftRefLAF();

protected:
	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls);

};

}

#endif /* LiftRefLAF_H_ */
