#include "MSatSMTEnvironment.h"

#include <iostream>

using namespace std;

namespace CTIGAR {

MSatSMTEnvironment::MSatSMTEnvironment(string name,
		const VarTypeMap & variables, const Options & opt, bool model,
		bool core, bool interpolate) :
		SMTEnvironment(name, opt) {
	msat_config cfg;
	cfg = msat_create_config();
	if (model) msat_set_option(cfg, "model_generation", "true");
	if (core) msat_set_option(cfg, "unsat_core_generation", "1");
	if (interpolate) msat_set_option(cfg, "interpolation", "true");
	env = msat_create_env(cfg);
	msat_destroy_config(cfg);

	for (VarTypeMap::const_iterator it = variables.begin();
			it != variables.end(); ++it) {
		switch (it->second) {
		case VarType::INT:
			declareIntVar(it->first, true);
			break;
		case VarType::BOOL:
			declareBoolVar(it->first, true);
			break;
		case VarType::DYN_ARRAY:
			declareDynArray(it->first, true);
			break;
		case VarType::FIXED_ARRAY:
			declareFixedArray(it->first, true);
			break;
		case VarType::INPUT_ARRAY:
			declareDynArray(it->first, true);
			break;
		default:
			assert(false);
		}
	}
}

MSatSMTEnvironment::~MSatSMTEnvironment() {
	msat_destroy_env(env);
}

msat_term MSatSMTEnvironment::toMSatTerm(TermPtr term) {
	if (toMSat.find(term) != toMSat.end()) {
		return toMSat[term];
	}

	msat_term ret;
	switch (term->type) {
	case TermType::EQ:
		ret = mkComparison(ComparisonType::EQ, toMSatTerm(term->getArg(0)),
				toMSatTerm(term->getArg(1)));
		break;
	case TermType::LEQ:
		ret = mkComparison(ComparisonType::LEQ, toMSatTerm(term->getArg(0)),
				toMSatTerm(term->getArg(1)));
		break;
	case TermType::AND: {
		vector<msat_term> args;
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			args.push_back(toMSatTerm(*it));
		}
		ret = mkAnd(args.size(), &args[0]);
		break;
	}
	case TermType::OR: {
		vector<msat_term> args;
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			args.push_back(toMSatTerm(*it));
		}
		ret = mkOr(args.size(), &args[0]);
		break;
	}
	case TermType::NOT:
		ret = mkNot(toMSatTerm(term->getArg(0)));
		break;
	case TermType::PLUS:
		ret = mkPlus(toMSatTerm(term->getArg(0)), toMSatTerm(term->getArg(1)));
		break;
	case TermType::TIMES:
		ret = mkTimes(toMSatTerm(term->getArg(0)), toMSatTerm(term->getArg(1)));
		break;
	case TermType::SELECT:
		ret = mkArraySelect(toMSatTerm(term->getArg(0)),
				toMSatTerm(term->getArg(1)));
		break;
	case TermType::BOOL_VAR:
		ret = mkBoolVar(term->getName());
		break;
	case TermType::INT_VAR:
		ret = mkIntVar(term->getName());
		break;
	case TermType::ARRAY_VAR:
		ret = mkArrayVar(term->getName());
		break;
	case TermType::INTEGER:
		ret = mkInt(term->toLong());
		break;
	case TermType::STORE:
		ret = mkArrayStore(toMSatTerm(term->getArg(0)),
				toMSatTerm(term->getArg(1)), toMSatTerm(term->getArg(2)));
		break;
	case TermType::FALSE:
		ret = mkFalse();
		break;
	case TermType::TRUE:
		ret = mkTrue();
		break;
	case TermType::UNARY_MINUS:
		ret = mkTimes(mkInt(-1), toMSatTerm(term->getArg(0)));
		break;
	case TermType::MOD_CONGR:
		ret = mkIntModCongr(term->toLong(), toMSatTerm(term->getArg(0)),
				toMSatTerm(term->getArg(1)));
		break;
	case TermType::FORALL:
	case TermType::BOUND_VAR:
		throw UnsupportedTermTypeException(term->type);
	default:
		cerr << *term << endl;
		assert(false);
		ret = mkErrorTerm();
		break;
	}

	toMSat[term] = ret;
	return ret;
}

TermPtr MSatSMTEnvironment::fromMSatTerm(msat_term term) {
	TermPtr ret;

	msat_type type = msat_term_get_type(term);
	if (termIsVar(term)) {
		if (msat_is_bool_type(env, type))
			ret = tf.mkBoolVar(varName(term));
		else if (msat_is_integer_type(env, type))
			ret = tf.mkIntVar(varName(term));
		else if (msat_is_array_type(env, type, NULL, NULL))
			ret = tf.mkArrayVar(varName(term));
		else {
			cerr << termToString(term) << endl;
			assert(false);
			ret = NULL;
		}
	} else if (termIsKind(term, TermType::EQ)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::EQ,
				fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsKind(term, TermType::LEQ)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::LEQ,
				fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsKind(term, TermType::AND)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkAnd(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsKind(term, TermType::OR)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkOr(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsKind(term, TermType::NOT)) {
		assert(termNumArgs(term) == 1);
		ret = tf.mkNot(fromMSatTerm(termGetArg(term, 0)));
	} else if (termIsKind(term, TermType::PLUS)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkPlus(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsKind(term, TermType::TIMES)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkTimes(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsArrayRead(term)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkArraySelect(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else if (termIsArrayWrite(term)) {
		assert(termNumArgs(term) == 3);
		ret = tf.mkArrayStore(fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)),
				fromMSatTerm(termGetArg(term, 2)));
	} else if (termIsNumeral(term)) {
		assert(termNumArgs(term) == 0);
		ret = tf.mkInt(termToLong(term));
	} else if (termIsTrue(term)) {
		ret = tf.mkTrue();
	} else if (termIsFalse(term)) {
		ret = tf.mkFalse();
	} else if (termIsModCongr(term)) {
		assert(termNumArgs(term) == 2);
		mpz_t modz;
		mpz_init(modz);
		msat_term_is_int_modular_congruence(env, term, modz);
		assert(mpz_fits_sint_p(modz));
		long mod = mpz_get_si(modz);
		mpz_clear(modz);
		ret = tf.mkMod(mod, fromMSatTerm(termGetArg(term, 0)),
				fromMSatTerm(termGetArg(term, 1)));
	} else {
		cerr << termToString(term) << endl;
		assert(false);
		ret = NULL;
	}

	toMSat[ret] = term;
	return ret;
}

void MSatSMTEnvironment::assertFormula(TermPtr ast) {
	msat_term msatAst = toMSatTerm(ast);
	int rv = msat_assert_formula(env, msatAst);
	assert(rv == 0);
}

size_t MSatSMTEnvironment::assertAndTrack(TermPtr ast) {
	msat_term msatAst = toMSatTerm(ast);
	int rv = msat_assert_formula(env, msatAst);
	assert(rv == 0);
	return termId(msatAst);
}

void MSatSMTEnvironment::assertFormulas(vector<size_t> & ids,
		const TermVec & ts) {
	for (TermVec::const_iterator it = ts.begin(); it != ts.end(); ++it) {
		size_t id = assertAndTrack(*it);
		ids.push_back(id);
	}
}

void MSatSMTEnvironment::release(TermPtr actLit) {
	int rv = msat_assert_formula(env, mkNot(toMSatTerm(actLit)));
	assert(rv == 0);
}

size_t MSatSMTEnvironment::assertFormula(TermPtr ast, TermPtr actLit) {
	assert(numBT == 0);

	msat_term msatAst = toMSatTerm(ast);
	msat_term msatActLit = toMSatTerm(actLit);
	msat_term toAdd = mkOr(msatAst, mkNot(msatActLit));
	int rv = msat_assert_formula(env, toAdd);
	assert(rv == 0);
	return termId(msatActLit);
}

void MSatSMTEnvironment::push(void) {
	int rv = msat_push_backtrack_point(env);
	assert(rv == 0);
	++numBT;
}

void MSatSMTEnvironment::pop(void) {
	int rv = msat_pop_backtrack_point(env);
	assert(rv == 0);
	--numBT;
}

TermPtr MSatSMTEnvironment::parsePredicate(const string & str) {
	return parseString(str);
}

TermPtr MSatSMTEnvironment::parseString(const string & str) {
	msat_term term = msat_from_string(env, str.c_str());
	if (MSAT_ERROR_TERM(term)) {
		cerr << "Cannot parse term: " << str << endl;
		assert(false && "Cannot parse term");
	}
	return fromMSatTerm(term);
}

TermPtr MSatSMTEnvironment::fromSmtLib2(string str) {
	msat_term term = msat_from_smtlib2(env, str.c_str());
	if (MSAT_ERROR_TERM(term)) {
		cerr << "Cannot parse SMTLIB2 term: " << str << endl;
		assert(false && "Cannot parse SMTLIB2 term");
	}
	return fromMSatTerm(term);
}

long MSatSMTEnvironment::termToLong(msat_term term) const {
	assert(termIsNumeral(term));
	mpz_t number;
	mpz_init(number);
	termToNumber(&number, term);
	long ret = mpz_get_si(number);
	mpz_clear(number);
	return ret;
}

string MSatSMTEnvironment::termToString(msat_term term) const {
	char * s = msat_term_repr(term);
	assert(s);
	string ret(s);
	msat_free(s);
	return ret;
}

void MSatSMTEnvironment::termToNumber(mpz_t * number, msat_term term) const {
	assert(termIsNumeral(term));
	mpq_t q;
	mpz_t d;
	mpq_init(q);
	mpz_init(d);
	int ret = msat_term_to_number(env, term, q);
	assert(ret == 0);
	mpq_get_num(*number, q);
	mpq_get_den(d, q);
	assert(mpz_cmp_si(d, 1) == 0);
	mpz_clear(d);
	mpq_clear(q);
}

msat_term MSatSMTEnvironment::mkTrue(void) const {
	return msat_make_true(env);
}

msat_term MSatSMTEnvironment::mkFalse(void) const {
	return msat_make_false(env);
}

msat_term MSatSMTEnvironment::mkIntVar(string name) const {
	msat_decl var;
	msat_type ift = msat_get_integer_type(env);
	var = msat_find_decl(env, name.c_str());
	assert(!MSAT_ERROR_DECL(var));
	assert(msat_type_equals(msat_decl_get_return_type(var), ift));
	msat_term ret = msat_make_constant(env, var);
	assert(!MSAT_ERROR_TERM(ret));

	return ret;
}

msat_term MSatSMTEnvironment::mkArrayVar(string name) const {
	msat_decl var;
	msat_type ift = msat_get_integer_type(env);
	msat_type aft = msat_get_array_type(env, ift, ift);
	var = msat_find_decl(env, name.c_str());
	assert(!MSAT_ERROR_DECL(var));
	assert(msat_type_equals(msat_decl_get_return_type(var), aft));
	msat_term ret = msat_make_constant(env, var);
	assert(!MSAT_ERROR_TERM(ret));

	return ret;
}

msat_term MSatSMTEnvironment::mkInt(const long l) const {
	msat_term ret = msat_make_number(env, to_string(l).c_str());
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

msat_term MSatSMTEnvironment::mkIntModCongr(long modulus, msat_term t1,
		msat_term t2) {
	mpz_t mod;
	mpz_init_set_si(mod, modulus);
	msat_term ret = msat_make_int_modular_congruence(env, mod, t1, t2);
	assert(!MSAT_ERROR_TERM(ret));
	mpz_clear(mod);
	return ret;
}

msat_term MSatSMTEnvironment::mkArraySelect(msat_term var,
		msat_term idx) const {
	msat_term ret = msat_make_array_read(env, var, idx);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

msat_term MSatSMTEnvironment::mkArrayStore(msat_term var, msat_term idx,
		msat_term val) const {
	msat_term ret = msat_make_array_write(env, var, idx, val);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

TermPtr MSatSMTEnvironment::mkActLit(void) {
	string name = ".al." + to_string(nActLit);
	declareBoolVar(name, false);
	nActLit++;
	return fromMSatTerm(mkBoolVar(name));
}

msat_term MSatSMTEnvironment::mkBoolVar(string name) const {
	msat_decl var;
	msat_type ift = msat_get_bool_type(env);
	var = msat_find_decl(env, name.c_str());
	assert(!MSAT_ERROR_DECL(var));
	assert(msat_type_equals(msat_decl_get_return_type(var), ift));

	msat_term ret = msat_make_constant(env, var);
	assert(!MSAT_ERROR_TERM(ret));

	return ret;
}

msat_term MSatSMTEnvironment::mkErrorTerm(void) const {
	msat_term ret = mkTrue();
	MSAT_MAKE_ERROR_TERM(ret);
	return ret;
}

bool MSatSMTEnvironment::isErrorTerm(msat_term term) const {
	return MSAT_ERROR_TERM(term);
}

bool MSatSMTEnvironment::sat(void) {
	++nQuery;
	startTimer();
	msat_result rv = msat_solve(env);
	endTimer();
	assert(rv == MSAT_SAT || rv == MSAT_UNSAT);
	return rv == MSAT_SAT;
}

bool MSatSMTEnvironment::sat(const TermVec & assumptions) {
	++nQuery;
	vector<msat_term> msatAssumptions;
	for (TermVec::const_iterator it = assumptions.begin();
			it != assumptions.end(); ++it) {
		msat_term t = toMSatTerm(*it);
		msatAssumptions.push_back(t);
	}
	startTimer();
	msat_result rv = msat_solve_with_assumptions(env, &msatAssumptions[0],
			msatAssumptions.size());
	endTimer();
	assert(rv == MSAT_SAT || rv == MSAT_UNSAT);
	return rv == MSAT_SAT;
}

void MSatSMTEnvironment::getUnsatAssumps(vector<size_t> & assumps) const {
	size_t coreSize;
	msat_term * msatCore = msat_get_unsat_assumptions(env, &coreSize);
	assert(msatCore);
	for (size_t i = 0; i < coreSize; ++i) {
		assumps.push_back(termId(msatCore[i]));
	}
	msat_free(msatCore);
}

void MSatSMTEnvironment::getUnsatCore(vector<size_t> & core) const {
	size_t coreSize;
	msat_term * msatCore = msat_get_unsat_core(env, &coreSize);
	assert(msatCore);
	for (size_t i = 0; i < coreSize; ++i) {
		core.push_back(termId(msatCore[i]));
	}
	msat_free(msatCore);
}

size_t MSatSMTEnvironment::termId(msat_term term) const {
	return msat_term_id(term);
}

bool MSatSMTEnvironment::termIsKind(msat_term term, TermType::E kind) const {
	switch (kind) {
	case TermType::AND:
		return msat_term_is_and(env, term) != 0;
	case TermType::OR:
		return msat_term_is_or(env, term) != 0;
	case TermType::NOT:
		return msat_term_is_not(env, term) != 0;
	case TermType::EQ:
		return msat_term_is_equal(env, term) != 0;
	case TermType::LEQ:
		return msat_term_is_leq(env, term) != 0;
	case TermType::PLUS:
		return msat_term_is_plus(env, term) != 0;
	case TermType::TIMES:
		return msat_term_is_times(env, term) != 0;
	default:
		assert(false);
		return false;
	}
}

bool MSatSMTEnvironment::termIsVar(msat_term term) const {
	return msat_term_is_constant(env, term) != 0;
}

bool MSatSMTEnvironment::termIsArrayRead(msat_term term) const {
	return msat_term_is_array_read(env, term);
}

bool MSatSMTEnvironment::termIsArrayWrite(msat_term term) const {
	return msat_term_is_array_write(env, term);
}

bool MSatSMTEnvironment::termIsNumeral(msat_term term) const {
	return msat_term_is_number(env, term) != 0;
}

bool MSatSMTEnvironment::termIsTrue(msat_term term) const {
	return msat_term_is_true(env, term);
}

bool MSatSMTEnvironment::termIsModCongr(msat_term term) const {
	mpz_t m;
	mpz_init(m);
	bool rv = msat_term_is_int_modular_congruence(env, term, m);
	mpz_clear(m);
	return rv;
}

bool MSatSMTEnvironment::termIsFalse(msat_term term) const {
	return msat_term_is_false(env, term);
}

string MSatSMTEnvironment::varName(msat_term term) const {
	msat_decl decl = msat_term_get_decl(term);
	char * str = msat_decl_get_name(decl);
	assert(str);
	string ret(str);
	free(str);
	return ret;
}

string MSatSMTEnvironment::numeralToString(msat_term term) const {
	assert(termIsNumeral(term));
	char * s = msat_term_repr(term);
	string ret(s);
	msat_free(s);
	return ret;
}

msat_term MSatSMTEnvironment::termGetArg(msat_term term, size_t idx) const {
	return msat_term_get_arg(term, idx);
}

msat_term MSatSMTEnvironment::mkNot(msat_term term) const {
	msat_term t = msat_make_not(env, term);
	assert(!MSAT_ERROR_TERM(t));
	return t;
}

msat_term MSatSMTEnvironment::mkOr(size_t size, msat_term * args) const {
	assert(size > 0);
	msat_term ret = args[0];
	for (size_t i = 1; i < size; ++i) {
		ret = msat_make_or(env, ret, args[i]);
	}
	return ret;
}

msat_term MSatSMTEnvironment::mkAnd(size_t size, const msat_term * args) const {
	assert(size > 0);
	msat_term ret = args[0];
	for (size_t i = 1; i < size; ++i) {
		ret = msat_make_and(env, ret, args[i]);
	}
	return ret;
}

msat_term MSatSMTEnvironment::mkOr(msat_term arg1, msat_term arg2) const {
	msat_term t = msat_make_or(env, arg1, arg2);
	assert(!MSAT_ERROR_TERM(t));
	return t;
}

msat_term MSatSMTEnvironment::mkAnd(msat_term arg1, msat_term arg2) const {
	msat_term t = msat_make_and(env, arg1, arg2);
	assert(!MSAT_ERROR_TERM(t));
	return t;
}

msat_term MSatSMTEnvironment::mkComparison(ComparisonType::E kind,
		msat_term arg0, msat_term arg1) const {
	if (kind == ComparisonType::EQ) {
		return msat_make_equal(env, arg0, arg1);
	}
	if (kind == ComparisonType::LEQ) {
		return msat_make_leq(env, arg0, arg1);
	}
	if (kind == ComparisonType::GEQ) {
		return msat_make_leq(env, arg1, arg0);
	}
	if (kind == ComparisonType::LT) {
		return msat_make_not(env, msat_make_leq(env, arg1, arg0));
	}
	if (kind == ComparisonType::GT) {
		return msat_make_not(env, msat_make_leq(env, arg0, arg1));
	}
	if (kind == ComparisonType::NEQ) {
		return msat_make_not(env, msat_make_equal(env, arg0, arg1));
	}
	assert(false);
	return msat_make_false(env);
}

msat_term MSatSMTEnvironment::mkPlus(msat_term arg1, msat_term arg2) const {
	msat_term ret = msat_make_plus(env, arg1, arg2);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

msat_term MSatSMTEnvironment::mkTimes(msat_term arg1, msat_term arg2) const {
	msat_term ret = msat_make_times(env, arg1, arg2);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

msat_term MSatSMTEnvironment::copyFrom(const MSatSMTEnvironment * src,
		msat_term term) const {
	if (this == src) {
		return term;
	}
	msat_term ret = msat_make_copy_from(env, term,
			((MSatSMTEnvironment *) src)->env);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

void MSatSMTEnvironment::printModel(void) {
	SatModel mod;
	getModel(mod, false);
	for (SatModel::const_iterator it = mod.begin(); it != mod.end(); ++it) {
		cout << *it << ", ";
	}
	cout << endl;
}

void MSatSMTEnvironment::getBoolModel(SatModel & mod, set<string> & boolVars) {
	startTimer();
	msat_model_iterator iter = msat_create_model_iterator(env);
	assert(!MSAT_ERROR_MODEL_ITERATOR(iter));
	endTimer();
	while (msat_model_iterator_has_next(iter)) {
		msat_term t, v;
		msat_model_iterator_next(iter, &t, &v);
		if (!termIsVar(t)) continue;
		string name = varName(t);
		if (boolVars.find(name) != boolVars.end()) {
			assert(termIsTrue(v) || termIsFalse(v));
			string name = varName(t);
			if (termIsTrue(v)) {
				mod.insert(Assignment(name, true));
			} else {
				mod.insert(Assignment(name, false));
			}
		}
	}
	msat_destroy_model_iterator(iter);
	assert(mod.size() == boolVars.size());
}

// full model is for bool and int modelVars,
// not for arrays
void MSatSMTEnvironment::getModel(SatModel & mod, bool full) {
	set<string> setVars;
	startTimer();
	msat_model_iterator iter = msat_create_model_iterator(env);
	endTimer();
	assert(!MSAT_ERROR_MODEL_ITERATOR(iter));
	while (msat_model_iterator_has_next(iter)) {
		msat_term t, v;
		msat_model_iterator_next(iter, &t, &v);
		assert(termIsVar(t));
		string name = varName(t);
		setVars.insert(name);
		VarTypeMap::const_iterator it = declaredVars.find(name);
		assert(it != declaredVars.end());
		if (modelVars.find(it->first) != modelVars.end()) {
			if (it->second == VarType::INT) {
				if (opt.verbosity >= 4)
					cout << termToString(t) << "=" << termToString(v) << endl;
				assert(termIsNumeral(v));
				mpq_t number;
				mpz_t numerator;
				mpz_t denominator;

				mpq_init(number);
				mpz_init(numerator);
				mpz_init(denominator);

				int rv = msat_term_to_number(env, v, number);
				assert(rv == 0);

				mpq_get_num(numerator, number);
				mpq_get_den(denominator, number);
				if (mpz_cmp_si(denominator, 1) != 0) {
					char * numStr = mpz_get_str(NULL, 10, numerator);
					char * denStr = mpz_get_str(NULL, 10, denominator);
					cout << "Model denominator != 0: " << numStr << "/"
							<< denStr << endl;
					assert(false && "Model denominator != 0, "
							"even though var is an integer");
				}
				assert(mpz_cmp_si(denominator, 1) == 0);

				mod.insert(Assignment(name, numerator));

				mpz_clear(denominator);
				mpz_clear(numerator);
				mpq_clear(number);
			} else if (it->second == VarType::BOOL) {
				assert(termIsTrue(v) || termIsFalse(v));
				if (termIsTrue(v)) {
					mod.insert(Assignment(name, true));
					if (opt.verbosity >= 4)
						cout << termToString(t) << "=true" << endl;
				} else {
					mod.insert(Assignment(name, false));
					if (opt.verbosity >= 4)
						cout << termToString(t) << "=false" << endl;
				}
			} else {
				assert(false);
			}
		}
	}
	msat_destroy_model_iterator(iter);

	if (full) {
		set<string> unsetVars;
		for (VarTypeMap::const_iterator it = modelVars.begin();
				it != modelVars.end(); ++it) {
			if (setVars.find(it->first) != setVars.end()) continue;
			switch (it->second) {
			case VarType::INT: {
				Assignment asgn(it->first, (long) 0);
				mod.insert(asgn);
				break;
			}
			case VarType::BOOL: {
				Assignment asgn(it->first, false);
				mod.insert(asgn);
				break;
			}
			case VarType::DYN_ARRAY:
			case VarType::FIXED_ARRAY: {
				break;
			}
			default:
				assert(false);
			}
		}
	}
}

void MSatSMTEnvironment::getArgs(vector<msat_term> & args,
		msat_term term) const {
	msat_decl decl = msat_term_get_decl(term);
	size_t arity = msat_decl_get_arity(decl);
	for (size_t i = 0; i < arity; ++i) {
		args.push_back(msat_term_get_arg(term, i));
	}
}

size_t MSatSMTEnvironment::termNumArgs(msat_term term) const {
	msat_decl decl = msat_term_get_decl(term);
	return msat_decl_get_arity(decl);
}

msat_term MSatSMTEnvironment::getArg(msat_term term, size_t idx) const {
	assert(idx < termNumArgs(term));
	msat_term ret = msat_term_get_arg(term, idx);
	assert(!MSAT_ERROR_TERM(ret));
	return ret;
}

msat_term MSatSMTEnvironment::copyApp(msat_term term, msat_term * args) const {
	msat_decl decl = msat_term_get_decl(term);
	return msat_make_term(env, decl, args);
}

void MSatSMTEnvironment::setItpGroup(int group) const {
	int ret = msat_set_itp_group(env, group);
	assert(ret == 0);
}

int MSatSMTEnvironment::createItpGroup(void) const {
	int ret = msat_create_itp_group(env);
	assert(ret != -1);
	return ret;
}

bool MSatSMTEnvironment::interpolate(TermPtr & i, int ante) {
	startTimer();
	msat_term ret = msat_get_interpolant(env, &ante, 1);
	endTimer();
	if (MSAT_ERROR_TERM(ret)) {
		if (opt.verbosity >= 1) cout << "Interpolation failed" << endl;
		return false;
	} else {
		i = fromMSatTerm(ret);
		return true;
	}
}

void MSatSMTEnvironment::assertedTerms(vector<msat_term> & ret) const {
	size_t numAsserted;
	msat_term * asserted = msat_get_asserted_formulas(env, &numAsserted);
	assert(asserted);
	for (size_t i = 0; i < numAsserted; ++i) {
		ret.push_back(asserted[i]);
	}
	msat_free(asserted);
}

void MSatSMTEnvironment::printAssertedTerms(void) const {
	vector<msat_term> terms;
	assertedTerms(terms);
	for (vector<msat_term>::const_iterator it = terms.begin();
			it != terms.end(); ++it) {
		cout << termToString(*it) << endl;
	}
}

void MSatSMTEnvironment::declareIntVar(string name1, bool inModel) {
	if (declaredVars.find(name1) == declaredVars.end()) {
		msat_type ift = msat_get_integer_type(env);
		msat_decl decl = msat_declare_function(env, name1.c_str(), ift);
		assert(!MSAT_ERROR_DECL(decl));
		declaredVars.insert(VarTypeMap::value_type(name1, VarType::INT));
		if (inModel)
			modelVars.insert(VarTypeMap::value_type(name1, VarType::INT));
	}
}

void MSatSMTEnvironment::declareFixedArray(string name1, bool inModel) {
	if (declaredVars.find(name1) == declaredVars.end()) {
		msat_type ift = msat_get_integer_type(env);
		msat_type aft = msat_get_array_type(env, ift, ift);
		msat_decl decl = msat_declare_function(env, name1.c_str(), aft);
		assert(!MSAT_ERROR_DECL(decl));
		declaredVars.insert(
				VarTypeMap::value_type(name1, VarType::FIXED_ARRAY));
		if (inModel)
			modelVars.insert(
					VarTypeMap::value_type(name1, VarType::FIXED_ARRAY));
	}
}

void MSatSMTEnvironment::declareDynArray(string name1, bool inModel) {
	if (declaredVars.find(name1) == declaredVars.end()) {
		msat_type ift = msat_get_integer_type(env);
		msat_type aft = msat_get_array_type(env, ift, ift);
		msat_decl decl = msat_declare_function(env, name1.c_str(), aft);
		assert(!MSAT_ERROR_DECL(decl));
		declaredVars.insert(VarTypeMap::value_type(name1, VarType::DYN_ARRAY));
		if (inModel)
			modelVars.insert(VarTypeMap::value_type(name1, VarType::DYN_ARRAY));
	}
}

void MSatSMTEnvironment::declareInputArray(string name1, bool inModel) {
	if (declaredVars.find(name1) == declaredVars.end()) {
		msat_type ift = msat_get_integer_type(env);
		msat_type aft = msat_get_array_type(env, ift, ift);
		msat_decl decl = msat_declare_function(env, name1.c_str(), aft);
		assert(!MSAT_ERROR_DECL(decl));
		declaredVars.insert(
				VarTypeMap::value_type(name1, VarType::INPUT_ARRAY));
		if (inModel)
			modelVars.insert(
					VarTypeMap::value_type(name1, VarType::INPUT_ARRAY));
	}
}

void MSatSMTEnvironment::declareBoolVar(string name1, bool inModel) {
	if (declaredVars.find(name1) == declaredVars.end()) {
		msat_type ift = msat_get_bool_type(env);
		msat_decl decl = msat_declare_function(env, name1.c_str(), ift);
		assert(!MSAT_ERROR_DECL(decl));
		declaredVars.insert(VarTypeMap::value_type(name1, VarType::BOOL));
		if (inModel)
			modelVars.insert(VarTypeMap::value_type(name1, VarType::BOOL));
	}
}

static int allsatCollector(msat_term * model, int size, void * userData) {
// endTimer() is correct here
	++(((MSatCallbackData *) userData)->nQuery);
	const msat_env * env = ((MSatCallbackData *) userData)->env;
	const AllSatCallback * cb = ((MSatCallbackData *) userData)->cb;
	MSatSMTEnvironment * smtEnv = ((MSatCallbackData *) userData)->smtEnv;
	smtEnv->endTimer();
	SatModel mod;
	for (int i = 0; i < size; ++i) {
		if (msat_term_is_not(*env, model[i])) {
			msat_term v = msat_term_get_arg(model[i], 0);
			char * s = msat_term_repr(v);
			string varName(s);
			Assignment asgn(varName, false);
			mod.insert(asgn);
			free(s);
		} else {
			char * s = msat_term_repr(model[i]);
			string varName(s);
			Assignment asgn(varName, true);
			mod.insert(asgn);
			free(s);
		}
	}
	int ret = cb->callback(mod);
	smtEnv->startTimer();
	return ret;
}

void MSatSMTEnvironment::getAllModels(vector<msat_term> & important,
		const AllSatCallback & callback) {
	MSatCallbackData userData;
	userData.cb = &callback;
	userData.env = &env;
	userData.nQuery = &nQuery;
	userData.smtEnv = this;

	vector<msat_term> msatImportant;
	for (vector<msat_term>::const_iterator it = important.begin();
			it != important.end(); ++it) {
		msatImportant.push_back(*it);
	}

	push();
	startTimer();
	msat_all_sat(env, &msatImportant[0], msatImportant.size(), &allsatCollector,
			&userData);
	endTimer();
	pop();
}

void MSatSMTEnvironment::findVars(set<string> & occuringVars,
		msat_term term) const {
	if (termIsVar(term)) {
		occuringVars.insert(varName(term));
	} else {
		vector<msat_term> args;
		getArgs(args, term);
		for (vector<msat_term>::const_iterator it = args.begin();
				it != args.end(); ++it) {
			findVars(occuringVars, *it);
		}
	}
}

void MSatSMTEnvironment::loadNamedTerms(map<string, msat_term> & namedMap,
		const string & namedStr) const {
	size_t n;
	char ** names;
	msat_term * terms;
	int rv = msat_named_list_from_smtlib2(env, namedStr.c_str(), &n, &names,
			&terms);
	assert(rv == 0);
	for (size_t i = 0; i < n; ++i) {
		namedMap.insert(map<string, msat_term>::value_type(names[i], terms[i]));
	}
	msat_free(terms);
	for (size_t i = 0; i < n; ++i) {
		msat_free(names[i]);
	}
	msat_free(names);
}

}
