/*
 * Term.h
 *
 *  Created on: Apr 7, 2014
 *      Author: Johannes Birgmeier
 */

#ifndef TERM_H_
#define TERM_H_

#include <memory>

#include "Assignment.h"

using namespace std;

namespace CTIGAR {

struct ReturnType {
	enum E {
		BOOL, INT, ARRAY
	};
};

struct TermType {
	enum E {
		NOTERM,
		ERROR,
		BOOL_VAR,
		INT_VAR,
		ARRAY_VAR,
		EQ,
		LEQ,
		AND,
		OR,
		NOT,
		PLUS,
		TIMES,
		SELECT,
		STORE,
		INTEGER,
		TRUE,
		FALSE,
		UNARY_MINUS,
		MOD_CONGR,
		FORALL,
		BOUND_VAR,
		CONST_ARRAY
	};
};

ReturnType::E returnType(TermType::E termType);

struct ComparisonType {
	enum E {
		EQ, NEQ, GEQ, LEQ, LT, GT
	};
};

class Term;

typedef shared_ptr<Term> TermPtr;
typedef vector<TermPtr> TermVec;

class Term {

	friend class TermFactory;

private:
	Term(TermType::E type1) :
			val(0), type(type1) {
	}

	TermVec args;
	vector<string> boundVars;
	string name;
	long val;

	void incUses(void);

	void decUses(void);

public:
	const TermType::E type;
	virtual ~Term();

	void getArgs(TermVec & ret) const {
		ret = args;
	}

	TermPtr getArg(size_t idx) const {
		assert(idx < args.size());
		return args[idx];
	}

	const TermVec & getArgs(void) const {
		return args;
	}

	const string & getName(void) const {
		return name;
	}

	const vector<string> & getBoundVars(void) const {
		assert(type == TermType::FORALL);
		return boundVars;
	}

	const string toString() const;

	void findVars(set<string> & vars) const;

	bool isVar(void) const {
		return type == TermType::BOOL_VAR || type == TermType::INT_VAR
				|| type == TermType::ARRAY_VAR;
	}

	long toLong(void) const {
		assert(type == TermType::INTEGER || type == TermType::MOD_CONGR);
		return val;
	}
}
;

std::ostream & operator<<(std::ostream & stream, const Term & t);

class TermFactory {

private:

	TermFactory(TermFactory const &);
	void operator=(TermFactory const &);

public:

	TermFactory() {
	}

	TermPtr mkTrue(void) const;

	TermPtr mkFalse(void) const;

	TermPtr mkIntVar(string name) const;

	TermPtr mkArrayVar(string name) const;

	TermPtr mkConstArray(TermPtr val) const;

	TermPtr mkBoundVar(string name) const;

	TermPtr mkInt(const long l) const;

	TermPtr mkArraySelect(TermPtr var, TermPtr idx) const;

	TermPtr mkArrayStore(TermPtr var, TermPtr idx, TermPtr val) const;

	TermPtr mkBoolVar(string name) const;

	TermPtr mkNot(TermPtr term) const;

	TermPtr deepNegate(TermPtr term) const;

	TermPtr mkOr(const TermVec & args) const;

	TermPtr mkAnd(const TermVec & args) const;

	TermPtr mkOr(TermPtr arg1, TermPtr arg2) const;

	TermPtr mkAnd(TermPtr arg1, TermPtr arg2) const;

	TermPtr mkComparison(ComparisonType::E kind, TermPtr arg0,
			TermPtr arg1) const;

	TermPtr mkPlus(TermPtr arg1, TermPtr arg2) const;

	TermPtr mkTimes(TermPtr arg1, TermPtr arg2) const;

	TermPtr mkMinus(TermPtr arg0) const;

	TermPtr mkMod(long mod, TermPtr arg0, TermPtr arg1);

	TermPtr negClause(const AsgnVec & latches) const;

	TermPtr negClause(const TermVec & ast) const;

	TermPtr asgnToTerm(const Assignment & asgn) const;

	TermPtr copyApp(TermPtr, TermVec args) const;

	TermPtr mkErrorTerm(void) const;

	TermPtr mkForAll(const vector<string> & quantified, TermPtr formula) const;

	void dissolveOps(TermVec & ret, TermPtr in, TermType::E op) const;

	void toTermVec(TermVec & ret, const AsgnVec & in) const;

};

} /* namespace CTIGAR */

#endif /* TERM_H_ */
