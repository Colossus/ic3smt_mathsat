#! /usr/bin/zsh

function printt () {
  if [ -f ./out/benchmarks/"$fn"_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep "$sstring"`
    if [ "$el" != "" ]
    then
      t=`echo -n $el | sed "s/$sstring*//g"`
      printf "%-10s" "$t"
    else
      printf "%-10s" " "
    fi
  else
    printf "%-10s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

printf "%-10s" "CCE"
printf " , "
printf "%-10s" "CCL"
printf " , "
printf "%-10s" "CAE"
printf " , "
printf "%-10s" "CAL"
printf " , "
printf "%-10s" "LLE"
printf " , "
printf "%-10s" "LLL"
printf " , "
printf "%-10s" "LCE"
printf " , "
printf "%-10s" "LCL"
printf " , "
printf "%-10s" "CPAChecker"
echo

for i in `ls benchmarks/*.c`
do
  fn=`echo $i | sed 's/benchmarks\///g'`
  printt "cons-concr-pred_+Ctg-Geo+RSM"
  echo -n " , "
  printt "cons-concr-pred_+Ctg-Geo+RSM+Lazy"
  echo -n " , "
  printt "cons-abstr-pred_+Ctg-Geo+RSM"
  echo -n " , "
  printt "cons-abstr-pred_+Ctg-Geo+RSM+Lazy"
  echo -n " , "
  printt "lift-laf_+Ctg-Geo+RSM"
  echo -n " , "
  printt "lift-laf_+Ctg-Geo+RSM+Lazy"
  echo -n " , "
  printt "lift-caf_+Ctg-Geo+RSM"
  echo -n " , "
  printt "lift-caf_+Ctg-Geo+RSM+Lazy"
  if [ $sstring = "Elapsed total\\:" ]
  then
    echo -n " , "
    time=`cat out/cpachecker/$fn | egrep "user" | sed 's/\(.*\)\(user.*\)/\1/g'`
    if [ "$time" = "" ]
    then
      time=" "
    fi
    cat out/cpachecker/$fn | egrep -q "Verification result: (SAFE|UNSAFE)"
    if [ $? -eq 0 ]
    then
      printf "%-12s" "$time"
    else
      if [ "$time" = " " ]
      then
        printf "%-12s" "1200"
      else
        printf "%-12s" "$time"
      fi
    fi
  fi
  if [ "$sstring" = "Property holds:" ]
  then
    echo -n " , "
    cat out/cpachecker/$fn | egrep -q SAFE
    if [ $? -eq 0 ]
    then
      printf "%-12s" "1"
    else
      printf "%-12s" " "
    fi
  fi
  echo
done
