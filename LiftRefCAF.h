/*
 * ConsecutionRefinement.h
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#ifndef LiftRefCAF_H_
#define LiftRefCAF_H_

#include "LiftRef.h"

using namespace std;

namespace CTIGAR {

class LiftRefCAF: public LiftRef {

public:
	LiftRefCAF(Model & model, AbstractDomain & d, Options & opt);

	virtual ~LiftRefCAF();

protected:

	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls);

};

}

#endif /* CONSECUTIONREFINEMENT_H_ */
