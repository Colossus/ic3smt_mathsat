/*
 * SMTEnvironment.h
 *
 *  Created on: Apr 15, 2014
 *      Author: birgmei
 */

#ifndef SMTENVIRONMENT_H_
#define SMTENVIRONMENT_H_

#include <stddef.h>
#include <ctime>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "Assignment.h"
#include "Term.h"
#include "VarType.h"
#include "main.h"

using namespace std;

namespace CTIGAR {

typedef map<string, VarType::E> VarTypeMap;
typedef map<VarType::E, string> TypeVarMap;
typedef map<string, long> FixedSizeMap;
typedef map<string, string> DynSizeMap;
typedef map<string, string> ReverseSizeMap;
typedef map<string, TermPtr> InputSizeMap;
typedef map<string, string> VarPrimeMap;

class UnsupportedTermTypeException: public exception {

private:
	TermType::E type;

public:
	UnsupportedTermTypeException(TermType::E type) :
			type(type) {
	}

	virtual const char* what() const throw () {
		return ("Term type " + to_string(type) + " is not supported by the current solver").c_str();
	}

};

class SMTEnvironment {
protected:
	string name;
	const Options & opt;
	clock_t timer;
	clock_t t;
	size_t nQuery;
	int numBT;
	size_t nActLit;
	TermFactory tf;

public:
	SMTEnvironment(const string & name, const Options & opt);

	virtual ~SMTEnvironment() = 0;

	virtual size_t assertFormula(TermPtr term,
			TermPtr actLit) = 0;

	virtual void assertFormula(TermPtr) = 0;

	virtual size_t assertAndTrack(TermPtr) = 0;

	virtual void getModel(SatModel & mod, bool full) = 0;

	virtual TermPtr parsePredicate(const string &) = 0;

	virtual void release(TermPtr actLit) = 0;

	void releaseMultiple(const TermVec & actLits);

	virtual bool sat(void) = 0;

	virtual bool sat(const TermVec & assumptions) = 0;

	virtual void printAssertedTerms(void) const = 0;

	virtual void declareBoolVar(string varName, bool inModel) = 0;

	virtual void declareIntVar(string varName, bool inModel) = 0;

	virtual void push(void) = 0;

	virtual void pop(void) = 0;

	virtual void getBoolModel(SatModel & mod, set<string> & important) = 0;

	clock_t time();

	void startTimer();

	void endTimer(void);

	clock_t satTime();

	void setSatTime(clock_t time);

	void setNQuery(size_t oldQuery);

	size_t getNQuery();

	int getNumBT(void);

	size_t getNumActLits(void) {
		return nActLit;
	}
};

} /* namespace CTIGAR */

#endif /* SMTENVIRONMENT_H_ */
