/*
 * FormulaHolder.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

#include "FormulaHolder.h"

#include <assert.h>

#include "Z3SMTEnvironment.h"

using namespace std;

namespace CTIGAR {

void TRLoad::reset(long maxPC, const VarTypeMap & variables) {
	loadedMap.clear();
	for (int i = 0; i <= maxPC; ++i) {
		for (VarTypeMap::const_iterator it = variables.begin();
				it != variables.end(); ++it) {
			loadedMap[i][it->first] = false;
		}
	}
}

TermPtr PCRestriction::createPcRestriction() {
	TermPtr restriction;
	if (allStates) {
		TermPtr gr0 = tf.mkComparison(ComparisonType::GEQ, tf.mkIntVar(".s"),
				tf.mkInt(0));
		TermPtr leqMax = tf.mkComparison(ComparisonType::LEQ, tf.mkIntVar(".s"),
				tf.mkInt(model.getMaxPC()));
		restriction = tf.mkAnd(gr0, leqMax);
	} else {
		restriction = tf.mkFalse();
		for (set<long>::const_iterator it = pcRestrictions.begin();
				it != pcRestrictions.end(); ++it) {
			restriction = tf.mkOr(restriction,
					tf.mkComparison(ComparisonType::EQ, tf.mkIntVar(".s"),
							tf.mkInt(*it)));
		}
	}
	return restriction;
}

bool TRLoad::loaded(long pc, string var) {
	return loadedMap[pc][var];
}

void TRLoad::load(long pc, string var) {
	loadedMap[pc][var] = true;
}

TransitionHolder::TransitionHolder(Model & model, AbstractDomain * d,
		Options & opt) :
		model(model), nSlices(0), d(d), opt(opt) {
	trEnv = new Z3SMTEnvironment("TR Env", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	CFGMap & cm = model.getCFGMap();
	for (CFGMap::const_iterator pcVar = cm.begin(); pcVar != cm.end();
			++pcVar) {
		long pc = pcVar->first;
		for (ConjunctMap::const_iterator varTerms = pcVar->second.begin();
				varTerms != pcVar->second.end(); ++varTerms) {
			const string & var = varTerms->first;
			for (set<string>::const_iterator terms = varTerms->second.begin();
					terms != varTerms->second.end(); ++terms) {
				cfgTermMap[pc][var].push_back(trEnv->parsePredicate(*terms));
			}
			++nSlices;
		}
	}
	assert(cfgTermMap.size() == (unsigned ) (model.getMaxPC() + 1));
	const set<string> & tr = model.getTRStrings();
	for (set<string>::const_iterator it = tr.begin(); it != tr.end(); ++it) {
		TermPtr t = trEnv->parsePredicate(*it);
		transitionRelation.push_back(t);
	}
}

TransitionHolder::~TransitionHolder() {
	delete trEnv;
}

void TransitionHolder::loadTransitionRelation(SMTEnvironment * env) {
	TermPtr gr0 = tf.mkComparison(ComparisonType::GEQ, tf.mkIntVar(".s"),
			tf.mkInt(0));
	TermPtr leqMax = tf.mkComparison(ComparisonType::LEQ, tf.mkIntVar(".s"),
			tf.mkInt(model.getMaxPC()));
	env->assertFormula(gr0);
	env->assertFormula(leqMax);

	const TermVec & tv = getTransitionRelation();
	for (TermVec::const_iterator it = tv.begin(); it != tv.end(); ++it) {
		env->assertFormula(*it);
	}
}

// returns the number of additionally loaded slices
size_t TransitionHolder::loadSlice(SMTEnvironment * env, PCRestriction & restr,
		TRLoad & load, const AbstractState & ast) {
	assert(d != NULL);
	long min, max;
	if (opt.pcSlicing) {
		possiblePcVals(min, max, restr.origRestrictions, ast);
	} else {
		min = 0;
		max = model.getMaxPC();
		restr.allStates = true;
	}
	set<string> vars;
	d->getOccurringVariables(vars, ast);
	model.getPredecessors(restr.pcRestrictions, min, max);
	size_t ret = loadSlices(env, load, restr.pcRestrictions, vars);
	return ret;
}

// returns the number of additionally loaded slices
size_t TransitionHolder::loadSlice(SMTEnvironment * env, PCRestriction & restr,
		TRLoad & load, const AsgnVec & st) {
	assert(d != NULL);
	long min, max;
	possiblePcVals(min, max, st);
	set<string> vars;
	for (AsgnVec::const_iterator it = st.begin(); it != st.end(); ++it) {
		vars.insert(it->getVar());
		if (it->getType() == AsgnType::DYN_ARRAY_ASGN)
			vars.insert(it->getArraySizeVar());
	}
	model.getPredecessors(restr.pcRestrictions, min, max);
	size_t ret = loadSlices(env, load, restr.pcRestrictions, vars);
	return ret;
}

// returns the number of additionally loaded slices
size_t TransitionHolder::loadErrorSlice(SMTEnvironment * env,
		PCRestriction & restr, TRLoad & load) {
	const set<long> & preds = model.getErrorPredecessors();
	restr.pcRestrictions = preds;
	const set<string> & vars = model.getErrorVars();
	size_t ret = loadSlices(env, load, preds, vars);
	return ret;
}

const TermVec & TransitionHolder::getTransitionRelation(void) {
	return transitionRelation;
}

void TransitionHolder::possiblePcVals(long & minV, long & maxV,
		set<long> & origRestrictions, const AbstractState & ast) const {
	assert(d != NULL);
	minV = 0;
	maxV = model.getMaxPC();
	for (AbstractState::const_iterator it = ast.begin(); it != ast.end();
			++it) {
		assert(*it != 0);
		set<string> variables;
		d->getOccurringVariables(variables, *it);
		if (variables.find(".s") == variables.end() || variables.size() > 1)
			continue;
		origRestrictions.insert(*it);
		TermPtr lit = d->literal(abs(*it));
		// this works only with mathsat! other solvers might not treat all inequalities as LEQ!
		if (lit->type == TermType::LEQ) {
			TermPtr l = lit->getArg(0);
			TermPtr r = lit->getArg(1);
			if (l->isVar()) {
				assert(r->type == TermType::INTEGER);
				long val = r->toLong();
				assert(val >= 0);
				assert(val <= model.getMaxPC());
				if (*it > 0) {
					maxV = min(maxV, val);
				} else {
					minV = max(minV, val + 1);
				}
			} else {
				assert(r->isVar());
				long val = l->toLong();
				assert(val >= 0);
				assert(val <= model.getMaxPC());
				if (*it > 0) {
					minV = max(minV, val);
				} else {
					maxV = min(maxV, val - 1);
				}
			}
		} else if (lit->type == TermType::EQ) {
			if (*it < 0) continue;
			TermPtr l = lit->getArg(0);
			TermPtr r = lit->getArg(1);
			long val;
			if (l->isVar()) {
				val = r->toLong();
			} else {
				assert(r->isVar());
				val = l->toLong();
			}
			assert(val >= 0);
			assert(val <= model.getMaxPC());
			maxV = minV = val;
		} else
			continue;
	}
	assert(minV <= maxV);
}

void TransitionHolder::possiblePcVals(long & minV, long & maxV,
		const AsgnVec & st) const {
	assert(d != NULL);
	minV = 0;
	maxV = model.getMaxPC();
	for (AsgnVec::const_iterator it = st.begin(); it != st.end(); ++it) {
		if (it->getVar() == ".s") {
			minV = maxV = it->getNumVal();
		}
	}
	assert(minV <= maxV);
}

size_t TransitionHolder::loadSlices(SMTEnvironment * env, TRLoad & load,
		const set<long> & preds, const set<string> & vars) {
	size_t ret = 0;
	for (set<long>::const_iterator pc = preds.begin(); pc != preds.end();
			++pc) {
		for (set<string>::const_iterator var = vars.begin(); var != vars.end();
				++var) {
			assert(cfgTermMap.find(*pc) != cfgTermMap.end());
			assert(cfgTermMap[*pc].find(*var) != cfgTermMap[*pc].end());
			if (!load.loaded(*pc, *var)) {
				++ret;
				load.load(*pc, *var);
				TermVec & terms = cfgTermMap[*pc][*var];
				for (TermVec::const_iterator term = terms.begin();
						term != terms.end(); ++term) {
					env->assertFormula(*term);
				}
			}
		}
	}
	return ret;
}

}
