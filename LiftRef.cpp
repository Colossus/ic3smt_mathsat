/*
 * ConsecutionRefinement.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

using namespace std;

#include "LiftRef.h"

namespace CTIGAR {

LiftRef::LiftRef(Model & model, AbstractDomain & d, Options & opt) :
		CTIGAR(model, d, opt) {

}

LiftRef::~LiftRef() {
}

bool LiftRef::checkCEXTrace(void) {
	if (verbosity >= 1) cout << "Checking CEX trace" << endl;
	SMTEnvironment * checker = new Z3SMTEnvironment("checker", model.vars.all,
			opt, model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	model.loadInitialCondition(checker);
	State & st = smgr.state(cexState);
	if (st.hasLiftedAst()) {
		TermVec astVec;
		d.toTermVec(astVec, st.liftedAst);
		for (TermVec::const_iterator it = astVec.begin(); it != astVec.end();
				++it) {
			checker->assertFormula(*it);
		}
	} else {
		for (AsgnVec::const_iterator it = st.lifted.begin();
				it != st.lifted.end(); ++it) {
			checker->assertFormula(tf.asgnToTerm(*it));
		}
	}
	if (!checker->sat()) {
		if (verbosity >= 1)
			cerr
					<< "First state in CEX trace does not satisfy initial condition"
					<< endl;
		delete checker;
		return false;
	}
	delete checker;
	checker = NULL;

	bool rv = true;

	State & st1 = smgr.state(cexState);
	while (st1.successor != 0) {
		checker = new MSatSMTEnvironment("checker", model.vars.all, opt, true,
				false, false);
		transitionHolder->loadTransitionRelation(checker);
		State & st2 = smgr.state(st1.successor);
		if (st1.hasLiftedAst()) {
			if (!st2.hasLiftedAst()) {
				if (verbosity >= 1)
					cerr << "State has lifted AST but successor doesn't"
							<< endl;
				delete checker;
				return false;
			}

			// st1
			TermVec tv1;
			d.toTermVec(tv1, st1.liftedAst);
			for (TermVec::const_iterator it = tv1.begin(); it != tv1.end();
					++it) {
				checker->assertFormula(*it);
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermVec tv2;
			d.toTermVec(tv2, st2.liftedAst);
			TermPtr negTv2 = tf.negClause(tv2);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
			if (checker->sat()) {
				if (verbosity >= 1) {
					cerr << "A->A: State " << st1.liftedAst
							<< " does not lead into " << st2.liftedAst << endl;
					SatModel mod;
					checker->getModel(mod, false);
					cerr << "Model: " << endl;
					for (SatModel::const_iterator it = mod.begin();
							it != mod.end(); ++it) {
						cerr << *it << endl;
					}
				}
				rv = false;
			} else {
				if (verbosity >= 3)
					cout << "Transition " << st1.index << " to " << st2.index
							<< " OK" << endl;
			}
		} else if (st2.hasLiftedAst()) {
			// st1
			for (AsgnVec::const_iterator it = st1.lifted.begin();
					it != st1.lifted.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermVec tv2;
			d.toTermVec(tv2, st2.liftedAst);
			TermPtr negTv2 = tf.negClause(tv2);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
			if (checker->sat()) {
				if (verbosity >= 1)
					cerr << "C->A: State " << st1 << " does not lead into "
							<< st2.liftedAst << endl;
				rv = false;
			} else {
				if (verbosity >= 3)
					cout << "Transition " << st1.index << " to " << st2.index
							<< " OK" << endl;
			}
		} else {
			// st1
			for (AsgnVec::const_iterator it = st1.lifted.begin();
					it != st1.lifted.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermPtr negTv2 = tf.negClause(st2.lifted);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
			if (checker->sat()) {
				if (verbosity >= 1)
					cerr << "C->C: State " << st1 << " does not lead into "
							<< st2 << endl;
				rv = false;
			} else {
				if (verbosity >= 3)
					cout << "Transition " << st1.index << " to " << st2.index
							<< " OK" << endl;
			}
		}
		delete checker;
		checker = NULL;
		st1 = st2;
	}
	if (rv == false) {
		return false;
	}

	checker = new MSatSMTEnvironment("checker", model.vars.all, opt, true,
			false, false);
	model.loadPrimedError(checker);
	transitionHolder->loadTransitionRelation(checker);
	if (st1.hasLiftedAst()) {
		TermVec tv1;
		d.toTermVec(tv1, st1.liftedAst);
		for (TermVec::const_iterator it = tv1.begin(); it != tv1.end(); ++it) {
			checker->assertFormula(*it);
		}
	} else {
		for (AsgnVec::const_iterator it = st1.lifted.begin();
				it != st1.lifted.end(); ++it) {
			checker->assertFormula(tf.asgnToTerm(*it));
		}
	}
	if (!checker->sat()) {
		if (verbosity >= 1)
			cerr << "Last state does not lead into error" << endl;
		delete checker;
		return false;
	}
	delete checker;
	if (verbosity >= 1) cout << "CEX trace OK" << endl;
	return true;
}

// Backtracks to the latest known state with a spurious predecessor and refines
// the domain based on this state, deleting all the obligations up to this state
// on the way
void LiftRef::backtrackRefine(const Obligation & obl, PriorityQueue & obls,
		bool enforceRefinement) {
	assert(enforceRefinement);
	size_t st = obl.state;

	while (!(smgr.state(st).spuriousSucc && smgr.state(st).nSpurious == 1)) {
		st = smgr.state(st).successor;
		assert(st != 0);
	}
	st = smgr.state(st).successor;
	assert(st != 0);
	assert(smgr.state(st).spuriousSucc == false);
	assert(smgr.state(st).nSpurious == 0);

	if (verbosity >= 2)
		cout << "Spurious state refinement " << smgr.state(st) << endl;
	elimSpuriousTrans(st);
	obls.clear();
}

// returns true if lifting succeeded
// returns false if abstract state contains spurious transitions.
// In this case, the lifting ast is not set.
bool LiftRef::updateAbstraction(size_t st) {
	assert(st != 0);
	if (smgr.state(st).hasLiftedAst()) return true;
	if (!smgr.state(st).outdated()) {
		// lifting did not succeed on last try,
		// but since the abstraction domain wasn't updated,
		// it won't succeed now either.
		return false;
	}
	size_t succ = smgr.state(st).successor;
	AbstractState full;
	d.abstract(full, smgr.state(st).lifted);
	AbstractState lifted;
	bool liftingSucceeded = liftAppropriate(lifted, full, st, succ);
	if (!liftingSucceeded) {
		lifted.clear();
	}
	smgr.state(st).setAbstraction(full, lifted, &d);
	return liftingSucceeded;
}

void LiftRef::elimSpuriousTrans(size_t st) {
	++nRefinements;

	size_t succ = smgr.state(st).successor;
	while (smgr.state(st).spuriousSucc
			|| (succ != 0 && !smgr.state(succ).hasLiftedAst())) {
		st = smgr.state(st).successor;
		succ = smgr.state(st).successor;
		assert(st != 0);
	}
	assert(!smgr.state(st).hasLiftedAst());

	TermVec lines;
	rsm->analyzeTrace(lines, st);
	for (TermVec::const_iterator it = lines.begin(); it != lines.end(); ++it) {
		refineDomain(*it, true);
	}
	TermPtr i;
	bool intpOK = interpolate(i, st);
	bool ref = false;
	if (intpOK) {
		ref = refineDomain(i, true);
	} else {
		bool r1 = false;
		for (AsgnVec::const_iterator it = smgr.state(st).lifted.begin();
				it != smgr.state(st).lifted.end(); ++it) {
			r1 = refineDomain(tf.asgnToTerm(*it), true);
			ref = ref || r1;
		}
	}
	assert(ref);
}

bool LiftRef::liftAppropriate(AbstractState & lifted,
		const AbstractState & full, const size_t st, const size_t succ) {
	if (succ == 0) {
		if (lift(lifted, full, smgr.state(st).inputs, NULL)) return true;
	} else if (smgr.state(succ).hasLiftedAst()) {
		if (lift(lifted, full, smgr.state(st).inputs,
				&smgr.state(succ).liftedAst)) return true;
	}
	// else {
	//	if (lift(lifted, full, smgr.state(st).inputs, smgr.state(succ).lifted))
	//		return true;
	// }
	return false;
}

bool LiftRef::interpolate(TermPtr & i, size_t st) {
	MSatSMTEnvironment * env = new MSatSMTEnvironment(
			"interpolation environment", model.vars.all, opt, false, true,
			true);
	int ante = env->createItpGroup();
	int cons = env->createItpGroup();

	env->setItpGroup(ante);
	const AsgnVec & s = smgr.state(st).lifted;
	for (AsgnVec::const_iterator it = s.begin(); it != s.end(); ++it) {
		TermPtr t = tf.asgnToTerm(*it);
		env->assertFormula(t);
	}
	env->setItpGroup(cons);
	const AsgnVec & inputs = smgr.state(st).inputs;
	for (AsgnVec::const_iterator it = inputs.begin(); it != inputs.end();
			++it) {
		TermPtr t = tf.asgnToTerm(*it);
		env->assertFormula(t);
	}
	transitionHolder->loadTransitionRelation(env);
	size_t succ = smgr.state(st).successor;
	if (succ == 0) {
		model.loadPrimedProperty(env);
	} else {
		TermPtr primedClause;
		TermVec tv;
		if (smgr.state(succ).hasLiftedAst()) {
			d.toTermVec(tv, smgr.state(succ).liftedAst);
		} else {
			assert(false);
			tf.toTermVec(tv, smgr.state(succ).lifted);
		}
		TermPtr clause = tf.negClause(tv);
		primedClause = model.primeTerm(clause);
		env->assertFormula(primedClause);
	}

	bool sat = env->sat();
	if (sat) {
		env->printAssertedTerms();
		assert(false && "Result not UNSAT");
	}
	bool intpOK = env->interpolate(i, ante);
	interpolationTime += env->satTime();
	delete env;
	return intpOK;
}

}
