/*
 * VarType.h
 *
 *  Created on: Feb 28, 2014
 *      Author: Johannes Birgmeier
 */

#ifndef VARTYPE_H_
#define VARTYPE_H_

#include <assert.h>
#include <map>
#include <string>
#include <memory>

using namespace std;

namespace CTIGAR {

struct VarType {
	enum E {
		NONE = 0, INT, BOOL, FIXED_ARRAY, DYN_ARRAY, INPUT_ARRAY
	};
};

VarType::E typeFromString(const string & typeString);

}

#endif /* VARTYPE_H_ */
