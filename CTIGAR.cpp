/*
 * CTIGAR.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

using namespace std;

#include "CTIGAR.h"

#include <signal.h>
#include <unistd.h>
#include <time.h>

#include "Model.h"
#include "ConsRefAbstrPred.h"
#include "ConsRefConcrPred.h"
#include "LiftRefCAF.h"
#include "LiftRefLAF.h"

namespace CTIGAR {

CTIGAR::CTIGAR(Model & model1, AbstractDomain & d1, Options & opt1) :
		d(d1), k(1), model(model1), interpolationTime(0), maxJoins(1 << 20), micAttempts(
				opt1.micAttempts), maxDepth(opt1.maxDepth), maxCTGs(
				opt1.maxCTGs), maxSpurious(opt1.maxSpurious), verbosity(
				opt1.verbosity), opt(opt1), transitionHolder(
		NULL), nLoaded(0), rsm(NULL), trivial(false), earliest(0), cexState(0), numLits(
				0), numUpdates(0), nCTI(0), nErased(0), nUnliftedErased(0), nCTG(
				0), nmic(0), nCoreReduced(0), nAbortJoin(0), nAbortMic(0), nRefinements(
				0), maxTraceDepth(0), fixedPointNumClauses(0), numLiteralsPerClause(
				0), nAbstractLiftingTries(0), nAbstractLiftingSuccessful(0), nLoadedSlices(
				0), nConsSolvers(1) {
	transitionHolder = new TransitionHolder(model, &d, opt);
	inits = new Z3SMTEnvironment("inits", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	model.loadInitialCondition(inits);
	lifts = new Z3SMTEnvironment("lifts", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	transitionHolder->loadTransitionRelation(lifts);
	model.loadProperty(lifts);
	d.loadRecommendedAbstractions();
	cons = new Z3SMTEnvironment("consecution", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	trLoad.reset(model.getMaxPC(), model.vars.program);
	model.loadProperty(cons);
	// transitionHolder->loadTransitionRelation(cons);
	// transitionHolder->loadPcRestriction(cons);
	rsm = new RSM(opt, model, smgr);
	// refineDomain(d.predicate(-d.negatedInitial()), true);
	// garg = new GargPredicates(opt, model, *transitionHolder, smgr, 1);
}

CTIGAR::~CTIGAR() {
	delete rsm;
	delete cons;
	delete lifts;
	delete inits;
	delete transitionHolder;
}

void CTIGAR::countFPClauses(size_t level, int & numClauses,
		size_t & numLitsPerClause) {
	numClauses = 0;
	numLitsPerClause = 0;
	for (size_t j = level; j <= k + 1; ++j) {
		for (CubeSet::iterator it = frames[j].borderCubes.begin();
				it != frames[j].borderCubes.end(); ++it) {
			++numClauses;
			numLitsPerClause = numLitsPerClause + it->size();
		}
	}
	numLitsPerClause /= numClauses;
}

bool CTIGAR::checkFP(size_t level) {
	// TODO make syntactic check that bypasses our Term format!
	if (verbosity >= 1) cout << "Checking fixed point" << endl;
	SMTEnvironment * checker = new Z3SMTEnvironment("checker", model.vars.all,
			opt, model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	checker->push();
	model.loadInitialCondition(checker);
	TermPtr f = tf.mkTrue();
	for (size_t j = level; j <= k + 1; ++j) {
		for (CubeSet::iterator it = frames[j].borderCubes.begin();
				it != frames[j].borderCubes.end(); ++it) {
			TermVec tv;
			d.toTermVec(tv, *it);
			TermPtr t = tf.negClause(tv);
			f = tf.mkAnd(f, t);
		}
	}
	TermPtr negF = tf.mkNot(f);
	checker->assertFormula(negF);
	if (checker->sat()) {
		cerr << "Supposed fixed point excludes some initial states!" << endl;
		checker->pop();
		delete checker;
		return false;
	}
	checker->pop();

	checker->push();
	model.loadProperty(checker);
	checker->assertFormula(f);
	transitionHolder->loadTransitionRelation(checker);
	model.loadPrimedError(checker);
	if (checker->sat()) {
		cerr << "Supposed fixed point does not "
				"imply property in one transition!" << endl;
		checker->pop();
		delete checker;
		return false;
	}
	checker->pop();

	checker->push();
	model.loadProperty(checker);
	checker->assertFormula(f);
	transitionHolder->loadTransitionRelation(checker);
	TermPtr primedNegF = model.primeTerm(negF);
	checker->assertFormula(primedNegF);

	if (checker->sat()) {
		cerr << "Supposed fixed point frame is not fixed; "
				"frame does not hold in one transition!" << endl;
		checker->pop();
		checker->push();
		model.loadProperty(checker);
		checker->assertFormula(f);
		transitionHolder->loadTransitionRelation(checker);
		for (size_t j = level; j <= k + 1; ++j) {
			for (CubeSet::iterator it = frames[j].borderCubes.begin();
					it != frames[j].borderCubes.end(); ++it) {
				checker->push();
				TermVec tv;
				d.toTermVec(tv, *it);
				TermPtr ast = tf.mkAnd(tv);
				TermPtr astP = model.primeTerm(ast);
				checker->assertFormula(astP);
				if (checker->sat()) {
					cerr << "Cube " << *it << " reachable in one transition"
							<< endl;
				}
				checker->pop();
			}
		}
		checker->pop();
		delete checker;
		return false;
	}
	checker->pop();

	delete checker;
	if (verbosity >= 1) cout << "Fixed point OK" << endl;
	return true;
}

// The main loop.
bool CTIGAR::check() {
	while (true) {
		cout << "Level " << k << endl;
		clock_t etime = time();
		cout << "Elapsed time: "
				<< ((double) etime / (double) sysconf(_SC_CLK_TCK)) << endl;
		cout << "Num. predicates: " << d.size() << endl;
		extend();                         // push frontier frame
		if (!strengthen()) {  // strengthen to remove bad successors
			cout << "The final domain: " << endl;
			cout << d << endl;
			printWitness();
			assert(checkCEXTrace());
			return false;
		}
		if (verbosity >= 2) {
			cout << "The domain: " << endl;
			cout << d << endl;
		}
		size_t fp = propagate();
		if (fp != 0) {     // propagate clauses; check for proof
			cout << "The final domain: " << endl;
			cout << d << endl;
			countFPClauses(fp, fixedPointNumClauses, numLiteralsPerClause);
			assert(checkFP(fp));
			return true;
		}
		if (verbosity >= 2)
			cout << "Diff level " << k << ": " << frames[k].borderCubes << endl;
		++k;                              // increment frontier
	}
	assert(false);
	return false;
}

// Follows and prints chain of states from cexState forward.
void CTIGAR::printWitness() {
	if (cexState != 0) {
		size_t curr = cexState;
		while (curr) {
			cout << smgr.state(curr).index << ":\t" << smgr.state(curr).lifted
					<< endl;
			curr = smgr.state(curr).successor;
		}
	}
}

// Push a new Frame.
void CTIGAR::extend() {
	while (frames.size() < k + 2) {
		frames.resize(frames.size() + 1);
		Frame & fr = frames.back();
		fr.k = frames.size() - 1;
		fr.consecution = cons;
		fr.initialized = true;
		TermPtr actLit = cons->mkActLit();
		fr.actLit = actLit;
		for (size_t i = 0; i < frames.size(); ++i) {
			frames[i].actLits.push_back(actLit);
		}

		if (fr.k == 0) {
			const TermVec & i = model.getInitialCondition();
			for (TermVec::const_iterator it = i.begin(); it != i.end(); ++it) {
				cons->assertFormula(*it, actLit);
			}
		}
	}
}

// Strengthens frontier to remove error successors.
bool CTIGAR::strengthen() {
	Frame & frontier = frames[k];
	trivial = true;  // whether any cubes are generated
	earliest = k + 1;  // earliest frame with enlarged borderCubes
	while (true) {
		PCRestriction restr(model, opt);
		nLoadedSlices += transitionHolder->loadErrorSlice(cons, restr, trLoad);
		TermPtr restrTerm = restr.createPcRestriction();
		TermPtr actLit = cons->mkActLit();
		TermPtr primedError = model.getPrimedError();
		cons->assertFormula(primedError, actLit);
		cons->assertFormula(restrTerm, actLit);
		if (verbosity >= 2)
			cout << "Trying to get from frontier, which is level " << k
					<< ", to error" << endl;
		bool rv = frontier.solve(actLit);
		if (!rv) {
			if (verbosity >= 2) cout << "Can't get to error" << endl;
			cons->release(actLit);
			return true;
		}
		// handle CTI with error successor
		trivial = false;
		PriorityQueue pq;
		// enqueue main obligation and handle
		size_t sti = stateOf(0, true);
		State & st = smgr.state(sti);
		cons->release(actLit);
		if (verbosity >= 2)
			cout << "Enqueuing obligation " << st << " at level " << k - 1
					<< " and depth 1 " << endl;
		++nCTI;  // stats
		pq.insert(Obligation(sti, k - 1, 1));
		if (!handleObligations(pq)) {
			cout << "Could not handle obligations, returning false" << endl;
			return false;
		}
		// finished with States for this iteration, so clean up
		smgr.resetStates();
	}
	assert(false);
	return false;
}

// Propagates clauses forward using induction.  If any frame has
// all of its clauses propagated forward, then two frames' clause
// sets agree; hence those clause sets are inductive
// strengthenings of the property.  See the four invariants of IC3
// in the original paper.
size_t CTIGAR::propagate() {
	// 1. clean up: remove c in frame i if c appears in frame j when i < j
	CubeSet all;
	for (size_t i = k + 1; i >= earliest; --i) {
		Frame & fr = frames[i];
		CubeSet rem, nall;
		set_difference(fr.borderCubes.begin(), fr.borderCubes.end(),
				all.begin(), all.end(), inserter(rem, rem.end()),
				AbstractStateComp());
		if (verbosity >= 1)
			cout << i << " " << fr.borderCubes.size() << " " << rem.size()
					<< " ";
		fr.borderCubes.swap(rem);
		set_union(rem.begin(), rem.end(), all.begin(), all.end(),
				inserter(nall, nall.end()), AbstractStateComp());
		all.swap(nall);
		for (CubeSet::const_iterator it = fr.borderCubes.begin();
				it != fr.borderCubes.end(); ++it)
			assert(all.find(*it) != all.end());
		if (verbosity >= 1) cout << all.size() << endl;
		if (verbosity >= 3) {
			cout << "Frame " << i << " diff cubes: " << fr.borderCubes << endl;
		}
	}

	// 2. check if each c in frame i can be pushed to frame j
	for (size_t i = trivial ? k : 1; i <= k; ++i) {
		int ckeep = 0, cprop = 0;
		Frame & fr = frames[i];
		for (CubeSet::iterator j = fr.borderCubes.begin();
				j != fr.borderCubes.end();) {
			AbstractState core;

			if (abstractConsecution(i, *j, 0, &core)) {
				++cprop;
				// only add to frame i+1 unless the core is reduced
				addAbstractCube(i + 1, core);
				CubeSet::iterator tmp = j;
				++j;
				fr.borderCubes.erase(tmp);
			} else {
				++ckeep;
				++j;
			}
		}
		if (verbosity >= 1) cout << i << " " << ckeep << " " << cprop << endl;
		if (fr.borderCubes.empty()) {
			cout << "The fixed point:" << endl;
			for (size_t j = i; j <= k + 1; ++j) {
				for (CubeSet::iterator it = frames[j].borderCubes.begin();
						it != frames[j].borderCubes.end(); ++it) {
					TermVec tv;
					d.toTermVec(tv, *it);
					TermPtr t = tf.negClause(tv);
					cout << *t << endl;
				}
			}
			return i;
		}
	}
	//  simplify frames if possible and useful
	return 0;
}

// Based on
//
//   Zyad Hassan, Aaron R. Bradley, and Fabio Somenzi, "Better
//   Generalization in IC3," (submitted May 2013)
//
// Improves upon "down" from the original paper (and the FMCAD'07
// paper) by handling CTGs.
bool CTIGAR::ctgDown(size_t level, AbstractState & ast, size_t keepTo,
		size_t recDepth, size_t efMaxCTGs) {
	size_t ctgs = 0, joins = 0;
	while (true) {
		// induction check
		if (!initiation(ast)) return false;
		if (recDepth > maxDepth) {
			// quick check if recursion depth is exceeded
			AbstractState core;

			bool rv = abstractConsecution(level, ast, 0, &core);
			if (rv && core != ast) {
				++nCoreReduced;  // stats
				ast = core;
			}
			return rv;
		}
		// prepare to obtain CTG
		size_t ctg;
		AbstractState core;
		if (abstractConsecution(level, ast, 0, &core, &ctg)) {
			if (core != ast) {
				++nCoreReduced;  // stats
				ast = core;
			}
			// inductive, so clean up
			return true;
		}
		// not inductive, address interfering CTG
		bool ret = false;

		AbstractState abstractCtg;
		d.abstract(abstractCtg, smgr.state(ctg).lifted);

		AbstractState ctgCore;
		if (ctgs < efMaxCTGs && level > 1 && initiation(abstractCtg)
				&& abstractConsecution(level - 1, abstractCtg, 0, &ctgCore)) {
			// CTG is inductive relative to level-1; push forward and generalize
			++(nCTG);  // stats
			++ctgs;
			size_t j = level;
			// QUERY: generalize then push or vice versa? -- Johannes: This version works best, as it seems

			while (j <= k && abstractConsecution(j, ctgCore))
				++j;
			mic(j - 1, ctgCore, recDepth + 1);

			addAbstractCube(j, ctgCore);
		} else if (joins < maxJoins) {
			// ran out of CTG attempts, so join instead
			ctgs = 0;
			++joins;
			AbstractState tmp;
			for (size_t i = 0; i < ast.size(); ++i)
				if (find(abstractCtg.begin(), abstractCtg.end(), ast[i])
						!= abstractCtg.end())
					tmp.push_back(ast[i]);
				else if (i < keepTo) {
					// previously failed when this literal was dropped
					++(nAbortJoin);  // stats
					ret = true;
				}
			ast = tmp;  // enlarged cube
		} else {
			ret = true;
		}

		// clean up
		smgr.delState(ctg);
		if (ret) return false;
	}
	assert(false);
	return false;
}

// Extracts minimal inductive (relative to level) subclause from
// ~cube --- at least that's where the name comes from.  With
// ctgDown, it's not quite a MIC anymore, but what's returned is
// inductive relative to the possibly modifed level.
void CTIGAR::mic(size_t level, AbstractState & ast, size_t recDepth) {
	++nmic;  // stats
	// try dropping each literal in turn
	size_t attempts = micAttempts;
	orderCube(ast);
	size_t i;
	for (i = 0; i < ast.size();) {
		AbstractState cp(ast.begin(), ast.begin() + i);
		cp.insert(cp.end(), ast.begin() + i + 1, ast.end());
		if (ctgDown(level, cp, i, recDepth, maxCTGs)) {
			// maintain original order
			set<long> lits(cp.begin(), cp.end());
			AbstractState tmp;
			for (AbstractState::const_iterator j = ast.begin(); j != ast.end();
					++j)
				if (lits.find(*j) != lits.end()) {
					lits.erase(*j);
					tmp.push_back(*j);
				}
			tmp.insert(tmp.end(), lits.begin(), lits.end());
			ast.swap(tmp);
			attempts = micAttempts;
		} else {
			if (!--attempts) {
				// Limit number of attempts: if micAttempts literals in a
				// row cannot be dropped, conclude that the cube is just
				// about minimal.  Definitely improves mics/second to use
				// a low micAttempts, but does it improve overall
				// performance?
				++nAbortMic;						// stats
				return;
			}
			++i;
		}
	}
}

// wrapper to start inductive generalization
void CTIGAR::mic(size_t level, AbstractState & cube) {
	mic(level, cube, 1);
}

// Adds cube to frames at and below level, unless !toAll, in which
// case only to level.
void CTIGAR::addAbstractCube(size_t level, AbstractState & state) {
	sort(state.begin(), state.end());
	pair<CubeSet::iterator, bool> rv = frames[level].borderCubes.insert(state);
	if (!rv.second) return;

	// assert(initiation(state));
	earliest = min(earliest, level);
	TermVec tv;
	d.toTermVec(tv, state);
	TermPtr cls = tf.negClause(tv);
	if (verbosity >= 3) {
		cout << "Adding at level " << level << " " << *cls << endl;
	}
	frames[level].addClause(cls);
}

// ~cube was found to be inductive relative to level; now see if
// we can do better.
size_t CTIGAR::abstractGeneralize(size_t level, AbstractState & cube) {
	static unsigned ctr = 0;
	if (verbosity >= 3)
		cout << "Abstract Generalization Counter: " << ctr++ << endl;
	// generalize by dropping "literals" (predicates of the conjunction) and geometric generalization
	mic(level, cube);
	// push
	do {
		++level;
	} while (level <= k && abstractConsecution(level, cube));
	addAbstractCube(level, cube);
	return level;
}

bool CTIGAR::refineDomain(TermPtr toRefineWith, bool ineqsOnly) {
	if (verbosity >= 3) cout << "To refine with: " << *toRefineWith << endl;
	TermVec dissolvedAnds;
	bool refinedOne = false;

	set<string> termStrings;
	tf.dissolveOps(dissolvedAnds, toRefineWith, TermType::AND);
	for (TermVec::iterator it = dissolvedAnds.begin();
			it != dissolvedAnds.end(); ++it) {
		string termString = (*it)->toString();
		if (termStrings.find(termString) != termStrings.end()) continue;
		termStrings.insert(termString);

		if (ineqsOnly) {
			if ((*it)->type == TermType::EQ) {
				TermPtr cmp = tf.mkComparison(ComparisonType::LEQ,
						(*it)->getArg(0), (*it)->getArg(1));
				bool ref = d.refine(cmp);
				refinedOne = refinedOne || ref;
				cmp = tf.mkComparison(ComparisonType::GEQ, (*it)->getArg(0),
						(*it)->getArg(1));
				ref = d.refine(cmp);
				refinedOne = refinedOne || ref;
			} else {
				bool ref = d.refine(*it);
				refinedOne = refinedOne || ref;
			}
		} else {
			if ((*it)->type == TermType::EQ) {
				TermPtr cmp = tf.mkComparison(ComparisonType::LEQ,
						(*it)->getArg(0), (*it)->getArg(1));
				bool ref = d.refine(cmp);
				refinedOne = refinedOne || ref;
			}
			bool ref = d.refine(*it);
			refinedOne = refinedOne || ref;
		}
	}
	return refinedOne;
}

// Returns true if the cube does not intersect with the initial condition.
bool CTIGAR::initiation(const AsgnVec & latches) {
	inits->push();
	for (AsgnVec::const_iterator i = latches.begin(); i != latches.end(); ++i) {
		TermPtr newTerm = tf.asgnToTerm(*i);
		inits->assertFormula(newTerm);
	}
	bool rv = inits->sat();
	inits->pop();
	return !rv;
}

// Returns true if the abstract state does not intersect with the initial condition
// first check if -3 is in ast
bool CTIGAR::initiation(const AbstractState & ast, bool optimize, bool print) {
	if (optimize && is_sorted(ast.begin(), ast.end())) {
		if (find(ast.begin(), ast.end(), d.negatedInitial()) != ast.end()) {
			return true;
		}
	}

	assert(inits->getNumBT() == 0);
	inits->push();
	TermVec tv;
	d.toTermVec(tv, ast);
	for (TermVec::const_iterator i = tv.begin(); i != tv.end(); ++i) {
		inits->assertFormula(*i);
	}
	bool rv = inits->sat();
	if (print) {
		inits->printAssertedTerms();
	}
	inits->pop();
	return !rv;
}

void CTIGAR::rebuildConsecution(void) {
	if (cons->getNumActLits() - k
			< opt.rebuildIntercept
					+ model.vars.toPrime.size() * opt.rebuildVarSlope) return;
	if (verbosity >= 2) cout << "Rebuilding consecution" << endl;

	clock_t t = cons->satTime();
	size_t oldQuery = cons->getNQuery();
	delete cons;
	cons = new Z3SMTEnvironment("consecution", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	++nConsSolvers;
	cons->setSatTime(t);
	cons->setNQuery(oldQuery);
	model.loadProperty(cons);
	// transitionHolder->loadTransitionRelation(fr.consecution);
	// transitionHolder->loadPcRestriction(cons);

	trLoad.reset(model.getMaxPC(), model.vars.program);

	TermPtr initActLit = cons->mkActLit();
	frames[0].actLit = initActLit;
	const TermVec & init = model.getInitialCondition();
	for (TermVec::const_iterator it = init.begin(); it != init.end(); ++it) {
		cons->assertFormula(*it, initActLit);
	}
	frames[0].consecution = cons;
	frames[0].actLits.clear();
	frames[0].actLits.push_back(initActLit);

	for (size_t i = 1; i < frames.size(); ++i) {
		frames[i].consecution = cons;
		frames[i].actLits.clear();
		TermPtr actLit = cons->mkActLit();
		frames[i].actLit = actLit;
		for (size_t j = 1; j <= i; ++j) {
			frames[j].actLits.push_back(actLit);
		}
		for (CubeSet::const_iterator it = frames[i].borderCubes.begin();
				it != frames[i].borderCubes.end(); ++it) {
			TermVec tv;
			d.toTermVec(tv, *it);
			TermPtr clause = tf.negClause(tv);
			cons->assertFormula(clause, actLit);
		}
	}
}

// checks if F_fi & ~abstract(s) & T => ~abstract(s)'
// succ: pred.successor = succ
bool CTIGAR::abstractConsecution(size_t fi, const AbstractState & ast,
		size_t succ, AbstractState * core, size_t * pred) {
	rebuildConsecution();

	bool rv;
	if (verbosity >= 3) {
		cout << "Original abstract state: " << ast << endl;
	}
	Frame & fr = frames[fi];
	PCRestriction restr(model, opt);
	nLoadedSlices += transitionHolder->loadSlice(cons, restr, trLoad, ast);
	TermVec actLits;
	TermPtr restrTerm = restr.createPcRestriction();
	TermPtr restrAL = cons->mkActLit();
	size_t restrALId = cons->assertFormula(restrTerm, restrAL);
	TermPtr actLit = cons->mkActLit();
	actLits.push_back(restrAL);
	actLits.push_back(actLit);
	TermVec tv;
	PredicateMap pm;
	d.toTermVec(tv, ast, &pm);
	TermPtr negS = tf.negClause(tv);
	cons->assertFormula(negS, actLit);

	map<size_t, TermPtr> actToUnprimed;
	for (TermVec::const_iterator i = tv.begin(); i != tv.end(); ++i) {
		TermPtr t = model.primeTerm(*i);
		TermPtr actLitP = cons->mkActLit();
		actLits.push_back(actLitP);
		size_t actLitId = cons->assertFormula(t, actLitP);
		actToUnprimed.insert(map<size_t, TermPtr>::value_type(actLitId, *i));
	}
	bool sat = fr.solve(actLits);
	if (sat) {
		if (pred) {
			*pred = spuriousStateOf(cons, succ);
		}
		rv = false;
	} else {
		// succeeds
		if (core) {
			vector<size_t> unsatAssumps;
			cons->getUnsatAssumps(unsatAssumps);
			bool pushBackRestrs = false;
			for (size_t i = 0; i < unsatAssumps.size(); ++i) {
				size_t unsatId = unsatAssumps[i];
				// cout << cons->termToString(t) << endl;
				if (unsatId == restrALId) {
					pushBackRestrs = true;
				}
				if (actToUnprimed.find(unsatId) == actToUnprimed.end())
					continue;
				try {
					long domainId = pm.at(actToUnprimed[unsatId]);
					core->push_back(domainId);
				} catch (out_of_range &) {
					assert(false);
				}
			}

			if (opt.pcSlicing && pushBackRestrs) {
				if (verbosity >= 3) cout << "PC restriction is in core, "
						"pushing back original restrictions: ";
				for (set<long>::const_iterator it =
						restr.getOrigRestrs().begin();
						it != restr.getOrigRestrs().end(); ++it) {
					if (find(core->begin(), core->end(), *it) == core->end()) {
						if (verbosity >= 3) cout << d.literal(*it) << ", ";
						core->push_back(*it);
					}
				}
				if (verbosity >= 3) cout << endl;
			}

			if (!initiation(*core)) {
				if (verbosity >= 2) cout << "Pushing back negated initial "
						" in order "
						"to make core satisfy initiation" << endl;
				core->push_back(d.negatedInitial());
				//!don't uncomment these lines without double checking that addAbstractCube third param is correct!!
				//*core = ast;
			}
			if (verbosity >= 3) {
				cout << "Core of abstract state: " << *core << endl;
			}
			if (!initiation(*core)) {
				cout << "Initiation failed on core" << endl;
				initiation(*core, true, true);
				assert(false && "initiation failed on core");
			}
		}
		rv = true;
	}
	cons->releaseMultiple(actLits);
	return rv;
}

// checks if F_fi & ~s & T => ~s'
// won't return spurious results
bool CTIGAR::consecution(size_t fi, const AsgnVec & latches, size_t succ,
		size_t * pred) {
	rebuildConsecution();

	bool rv;
	Frame & fr = frames[fi];
	PCRestriction restr(model, opt);
	nLoadedSlices += transitionHolder->loadSlice(cons, restr, trLoad, latches);
	TermPtr restrTerm = restr.createPcRestriction();
	TermPtr actLit = cons->mkActLit();
	cons->assertFormula(restrTerm, actLit);
	for (AsgnVec::const_iterator i = latches.begin(); i != latches.end(); ++i) {
		cons->assertFormula(model.primeTerm(tf.asgnToTerm(*i)), actLit);
	}
	TermPtr negS = tf.negClause(latches);
	cons->assertFormula(negS, actLit);
	bool sat = fr.solve(actLit);
	if (sat) {
		// fails: extract predecessor(s)
		if (pred) *pred = stateOf(succ, true);
		rv = false;
	} else {
		rv = true;
	}
	cons->release(actLit);
	return rv;
}

void CTIGAR::updateLitOrder(const AbstractState & cube) {
	litOrder.decay();
	numUpdates += 1;
	numLits += (float) cube.size();
	litOrder.count(cube);
}

// order according to preference
void CTIGAR::orderCube(AbstractState & cube) {
	stable_sort(cube.begin(), cube.end(), litOrder);
}

void CTIGAR::extractLiftingCore(AbstractState & ret, bool sat,
		map<size_t, long> & termIdToLit, const AbstractState & toLift) {
	if (sat) {
		if (verbosity >= 2) {
			cout << "Abstract lifting unsuccessful" << endl;
		}
		ret.clear();
	} else {
		++nAbstractLiftingSuccessful;
		if (verbosity >= 2) {
			cout << "Abstract lifting successful: " << endl;
			cout << "Abstract lifting orig: " << toLift << endl;
		}
		ret.clear();
		vector<size_t> core;
		lifts->getUnsatAssumps(core);
		for (vector<size_t>::const_iterator it = core.begin(); it != core.end();
				++it) {
			if (termIdToLit.find(*it) == termIdToLit.end()) continue;
			ret.push_back(termIdToLit[*it]);
		}
		if (!initiation(ret)) {
			ret.push_back(d.negatedInitial());
			// assert(initiation(ret, false));
		}
		if (verbosity >= 2) cout << "Abstract lifting lifted: " << ret << endl;
	}
}

void CTIGAR::loadLiftingPrelims(const AbstractState & toLift,
		const AsgnVec & inputs, map<size_t, long> & termIdToLit) {
	for (AbstractState::const_iterator it = toLift.begin(); it != toLift.end();
			++it) {
		size_t litId = lifts->assertAndTrack(d.literal(*it));
		termIdToLit.insert(map<size_t, long>::value_type(litId, *it));
	}
	for (AsgnVec::const_iterator it = inputs.begin(); it != inputs.end();
			++it) {
		lifts->assertFormula(tf.asgnToTerm(*it));
	}
}

// returned ast always satisfies initiation
bool CTIGAR::lift(AbstractState & ret, const AbstractState & toLift,
		const AsgnVec & inputs, const AbstractState * successor) {
	// State t, transition relation T, successor s:
	// t & T & ~s' is unsat
	// Core assumptions reveal a lifting of t.
	bool sat = false;

	// assert(initiation(toLift));
	map<size_t, long> termIdToLit;
	lifts->push();
	loadLiftingPrelims(toLift, inputs, termIdToLit);
	if (successor != NULL) {
		TermVec succTerms;
		const AbstractState & bestAbstraction = *successor;
		d.toTermVec(succTerms, bestAbstraction);
		TermPtr negSuccTerms = tf.negClause(succTerms);
		TermPtr primedNegSuccTerms = model.primeTerm(negSuccTerms);
		lifts->assertFormula(primedNegSuccTerms);
	} else {
		model.loadPrimedProperty(lifts);
	}
	++nAbstractLiftingTries;
	sat = lifts->sat();
	extractLiftingCore(ret, sat, termIdToLit, toLift);
	lifts->pop();
	return !sat;
}

// returned ast always satisfies initiation
bool CTIGAR::lift(AbstractState & ret, const AbstractState & toLift,
		const AsgnVec & inputs, const AsgnVec & successor) {
	// State t, transition relation T, successor s:
	// t & T & ~s' is unsat
	// Core assumptions reveal a lifting of t.
	bool sat = false;

	// assert(initiation(toLift));
	map<size_t, long> termIdToLit;
	lifts->push();
	loadLiftingPrelims(toLift, inputs, termIdToLit);
	TermPtr negSuccTerms = tf.negClause(successor);
	TermPtr primedNegSuccTerms = model.primeTerm(negSuccTerms);
	lifts->assertFormula(primedNegSuccTerms);
	++nAbstractLiftingTries;
	sat = lifts->sat();
	extractLiftingCore(ret, sat, termIdToLit, toLift);
	lifts->pop();
	return !sat;
}

size_t CTIGAR::spuriousStateOf(SMTEnvironment * env, size_t succIdx) {
	// create state
	size_t st = smgr.newState();
	smgr.state(st).successor = succIdx;

	// extract and assert latches
	SatModel m;
	env->getModel(m, true);
	for (SatModel::const_iterator it = m.begin(); it != m.end(); ++it) {
		if (model.isInputVariable(it->getVar())) {
			smgr.state(st).inputs.push_back(*it);
		} else if (model.isUnprimedVariable(it->getVar())) {
			smgr.state(st).full.push_back(*it);
			smgr.state(st).lifted.push_back(*it);
		}
	}
	return st;
}

// Assumes that last call to cons->solve() was
// satisfiable.  Extracts smgr.state(s) cube from satisfying
// assignment.
size_t CTIGAR::stateOf(size_t succ, bool doLift) {
	// State t, transition relation T, successor s:
	//   t & T & ~s' is unsat
	// Core assumptions reveal a lifting of t.

	// create state
	size_t st = smgr.newState();
	smgr.state(st).successor = succ;

	if (doLift) {
		lifts->push();
		// extract and assert latches
		SatModel m;
		cons->getModel(m, true);
		for (SatModel::const_iterator it = m.begin(); it != m.end(); ++it) {
			const Assignment & asgn = *it;
			if (model.isInputVariable(asgn.getVar()))
				smgr.state(st).inputs.push_back(*it);
			else if (model.isUnprimedVariable(asgn.getVar()))
				smgr.state(st).full.push_back(*it);
		}

		AsgnMap am;
		if (verbosity >= 2) cout << "Lifting from " << endl;
		for (AsgnVec::const_iterator asgn = smgr.state(st).full.begin();
				asgn != smgr.state(st).full.end(); ++asgn) {
			TermPtr asgnTerm = tf.asgnToTerm(*asgn);
			size_t asgnId = lifts->assertAndTrack(asgnTerm);
			am.insert(AsgnMap::value_type(asgnId, *asgn));
			if (verbosity >= 2) cout << *asgn << ", ";
		}
		for (AsgnVec::const_iterator asgn = smgr.state(st).inputs.begin();
				asgn != smgr.state(st).inputs.end(); ++asgn) {
			TermPtr asgnTerm = tf.asgnToTerm(*asgn);
			lifts->assertFormula(asgnTerm);
			if (verbosity >= 2) cout << *asgn << ", ";
		}
		if (verbosity >= 2) cout << endl;

		AsgnVec latches;
		// assert the negation of the successor
		if (succ == 0)
			model.loadPrimedProperty(lifts);
		else {
			for (AsgnVec::const_iterator it = smgr.state(succ).lifted.begin();
					it != smgr.state(succ).lifted.end(); ++it) {
				latches.push_back(*it);
			}
			TermPtr negS = tf.negClause(latches);
			TermPtr negSPrime = model.primeTerm(negS);
			lifts->assertFormula(negSPrime);
		}

		if (verbosity >= 2) {
			cout << "to ";
			if (succ > 0)
				cout << latches << endl;
			else
				cout << "error" << endl;
		}

		bool rv = lifts->sat();
		if (rv) {
			lifts->printAssertedTerms();
			SatModel mod;
			lifts->getModel(mod, false);
			cout << "Model: " << mod << endl;
			assert(false);
		}
		// obtain lifted latch set from unsat core
		if (verbosity >= 2) cout << "Core: ";
		vector<size_t> unsatCore;
		lifts->getUnsatAssumps(unsatCore);
		for (size_t i = 0; i < unsatCore.size(); ++i) {
			size_t tId = unsatCore[i];
			if (am.find(tId) != am.end()) {
				Assignment & asgn = am.at(tId);
				if (verbosity >= 2) cout << asgn << ", ";
				smgr.state(st).lifted.push_back(asgn);
			}
		}
		if (verbosity >= 2) cout << endl;
		lifts->pop();
		return st;
	} else {
		// extract and assert latches
		SatModel m;
		cons->getModel(m, true);
		for (SatModel::const_iterator it = m.begin(); it != m.end(); ++it) {
			if (model.isInputVariable(it->getVar())) {
				smgr.state(st).inputs.push_back(*it);
			} else if (model.isUnprimedVariable(it->getVar())) {
				smgr.state(st).full.push_back(*it);
				smgr.state(st).lifted.push_back(*it);
			}
		}
		return st;
	}
}

clock_t CTIGAR::time() {
	struct tms t;
	times(&t);
	return t.tms_utime;
}

clock_t CTIGAR::satTime() {
	clock_t ret = 0;
	ret += d.env->satTime();
	ret += cons->satTime();
	ret += rsm->satTime();
	ret += lifts->satTime();
	ret += inits->satTime();
	ret += interpolationTime;
	return ret;
}

size_t CTIGAR::nQuery() {
	size_t ret = 0;
	ret += d.env->getNQuery();
	ret += cons->getNQuery();
	ret += rsm->getNQuery();
	ret += lifts->getNQuery();
	ret += inits->getNQuery();
	return ret;
}

void CTIGAR::printStats() {
	clock_t etime = time();
	cout << "Elapsed total:  "
			<< ((double) etime / (double) sysconf(_SC_CLK_TCK)) << endl;
	cout << "K:              " << k << endl;
	cout << "% SAT:          "
			<< (int) (100 * (((double) satTime()) / ((double) etime))) << endl;
	cout << "% Interpol:     "
			<< (int) (100 * (((double) interpolationTime) / ((double) etime)))
			<< endl;
	cout << "# Queries:      " << nQuery() << endl;
	cout << "# CTIs:         " << nCTI << endl;
	cout << "# CTGs:         " << nCTG << endl;
	cout << "# mic calls:    " << nmic << endl;
	cout << "Queries/sec:    "
			<< (int) (((double) nQuery()) / ((double) etime)
					* (double) sysconf(_SC_CLK_TCK)) << endl;
	cout << "Mics/sec:       "
			<< (int) (((double) nmic) / ((double) etime)
					* (double) sysconf(_SC_CLK_TCK)) << endl;
	cout << "# Red. cores:   " << nCoreReduced << endl;
	cout << "# Int. joins:   " << nAbortJoin << endl;
	cout << "# Int. mics:    " << nAbortMic << endl;
	cout << "# Domain Pr.:   " << d.size() << endl;
	cout << "# Spur. Ref.:   " << nRefinements << endl;
	cout << "# RSM:          " << rsm->getNSuccessfulAttempts() << endl;
	cout << "# Abstrctns:    " << d.nAbstractions << endl;
	cout << "# MaxTrDepth:   " << maxTraceDepth << endl;
	cout << "# FPClauses:    " << fixedPointNumClauses << endl;
	cout << "# AvgLit/Cl:    " << numLiteralsPerClause << endl;
	cout << "# AbsLiftTry:   " << nAbstractLiftingTries << endl;
	cout << "% AbsLiftSucc:  "
			<< (int) (100
					* (((double) nAbstractLiftingSuccessful)
							/ ((double) nAbstractLiftingTries))) << endl;
	cout << "# ConsRebuild:  " << nConsSolvers << endl;
	cout << "# Slices:       " << transitionHolder->numSlices() << endl;
	cout << "% COILoad:      "
			<< (int) (100 * (double) nLoadedSlices
					/ (double) (transitionHolder->numSlices() * nConsSolvers))
			<< endl;
	cout << "# Erased:       " << nErased << endl;
	cout << "% Unlifted:     "
			<< (int) ((100 * (double) nUnliftedErased) / ((double) nErased))
			<< endl;
}

// returns false if a CEX initial state is found
bool CTIGAR::predecessor(const Obligation & obl, PriorityQueue & obls,
		size_t pred) {
	if (obl.level == 0) {
		if (smgr.state(obl.state).nSpurious == 0) {
			// No, in fact an initial state is a predecessor.
			++nCTI;			// stats
			cexState = pred;
			return false;
		}
		backtrackRefine(obl, obls);
	} else {
		// No, so focus on predecessor.
		smgr.state(pred).spuriousSucc = false;
		smgr.state(pred).spuriousLevel = 0;
		smgr.state(pred).nSpurious = smgr.state(obl.state).nSpurious;
		++nCTI;
		obls.insert(Obligation(pred, obl.level - 1, obl.depth + 1));
		if (obl.depth + 1 > maxTraceDepth) {
			maxTraceDepth = obl.depth + 1;
		}
	}
	return true;
}

void CTIGAR::spuriousPredecessor(const Obligation & obl, PriorityQueue & obls,
		size_t pred) {
	if (verbosity >= 1)
		cout << "Ignoring " << smgr.state(obl.state).nSpurious + 1
				<< " spurious transitions, continuing with "
						"spurious predecessor. Affected state: "
				<< smgr.state(obl.state) << endl;
// level 0 is appropriate, see Aaron's proposal
	smgr.state(pred).spuriousSucc = true;
	smgr.state(pred).spuriousLevel = obl.level;
	smgr.state(pred).nSpurious = smgr.state(obl.state).nSpurious + 1;
	++nCTI;
	obls.insert(Obligation(pred, 0, obl.depth + 1));
	if (obl.depth + 1 > maxTraceDepth) {
		maxTraceDepth = obl.depth + 1;
	}
}

void CTIGAR::generalizeErase(const Obligation & obl,
		const PriorityQueue::const_iterator & obli, PriorityQueue & obls,
		AbstractState & core) {
// Yes, so generalize and possibly produce a new obligation
// at a higher level.
	size_t n = abstractGeneralize(obl.level, core);
	obls.erase(obli);
	if (n <= k) {
		obls.insert(Obligation(obl.state, n, obl.depth));
	} else {
		++nErased;
		if (!smgr.state(obl.state).hasLiftedAst()) {
			++nUnliftedErased;
		}
	}
}

// IC3 does not check for 0-step and 1-step reachability, so do it
// separately.
bool baseCases(Model & model, Options & opt) {
	SMTEnvironment * base0 = new Z3SMTEnvironment("base0", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	model.loadInitialCondition(base0);
	model.loadError(base0);
	bool rv = base0->sat();
	delete base0;
	if (rv) return false;

	SMTEnvironment * base1 = new Z3SMTEnvironment("base1", model.vars.all, opt,
			model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	// TODO eliminate this bad design (NULL)
	TransitionHolder * trHolder = new TransitionHolder(model, NULL, opt);
	model.loadInitialCondition(base1);
	trHolder->loadTransitionRelation(base1);
	model.loadPrimedError(base1);
	rv = base1->sat();
	delete trHolder;
	if (rv) {
		SatModel mod;
		base1->getModel(mod, false);
		delete base1;
		cout << mod << endl;
		return false;
	}
	delete base1;

	return true;
}

CTIGAR * ic3_glob = NULL;

void sigHandler(int signo) {
	if (signo == SIGINT) {
		ic3_glob->printStats();
		exit(2);
	} else if (signo == SIGPWR) {
		ic3_glob->printStats();
	} else {
		assert(false);
	}
}

// External function to make the magic happen.
bool check(Model & model, Options & opt) {
	if (!baseCases(model, opt)) return false;
	bool ret = true;
	SMTEnvironment * domainEnv;
	if (opt.abstractor == "mathsat") {
		domainEnv = new MSatSMTEnvironment("domain environment", model.vars.all,
				opt, true);
	} else if (opt.abstractor == "z3") {
		domainEnv = new Z3SMTEnvironment("domain environment", model.vars.all,
				opt, model.vars.dynArraySize, model.vars.fixedArraySize,
				model.vars.inputArraySize);
	} else {
		assert(false);
	}
	KarrIneqDomain d(model, domainEnv, opt);
	CTIGAR * ic3 = NULL;
	if (opt.refinementStrategy == "lift-caf") {
		ic3 = new LiftRefCAF(model, d, opt);
	} else if (opt.refinementStrategy == "lift-laf") {
		ic3 = new LiftRefLAF(model, d, opt);
	} else if (opt.refinementStrategy == "cons-abstr-pred") {
		ic3 = new ConsRefAbstrPred(model, d, opt);
	} else if (opt.refinementStrategy == "cons-concr-pred") {
		ic3 = new ConsRefConcrPred(model, d, opt);
	} else {
		assert(false && "Refinement strategy can "
				"be (lift-caf|lift-laf|cons-abstr-pred|cons-concr-pred)");
	}
	ic3_glob = ic3;
	signal(SIGINT, &sigHandler);
	signal(SIGPWR, &sigHandler);

	bool rv = ic3->check();
	if (!rv) {
		ic3->printWitness();
		ret = false;
	}
	ic3->printStats();
	return ret;
}

}
