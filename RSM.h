/*
 * RSM.h
 *
 *  Created on: Jan 8, 2014
 *      Author: Johannes Birgmeier
 */

#ifndef RSM_H_
#define RSM_H_

#include <map>
#include <iostream>

#include "Model.h"
#include "State.h"
#include "TraceBase.h"

using namespace std;

namespace CTIGAR {

class RSM {

protected:
	Model & model;
	TraceBase traceBase;
	unsigned verbosity;
	StateManager & smgr;
	size_t halfPlaneMinPoints;
	size_t halfPlaneMaxPoints;
	Options & opt;
	VarTypeMap intVars;

	TermFactory tf;
	MSatSMTEnvironment * traceMiner;

	size_t nRSM;
	clock_t t;
	size_t nQuery;

protected:

	void collectOccurringVars(set<string> & occuringVars,
			const set<AsgnSet> & asgns) const;

	string createLines(TermVec & ret,
			map<string, string> & coeffMap, map<string, string> & coeffRevMap,
			map<TermPtr, AsgnSet> & coreMap, const set<AsgnSet> & inputStates,
			string & cst) const;

	TermPtr extractLine(map<string, string> & coeffMap, string & cst);

	TermPtr mkNotZero(map<string, string> & coeffMap,
			const set<string> & notZeroCoeffs);

	TermPtr mkNotLine(AsgnSet & line);

	TermPtr halfPlane(set<AsgnSet> & unsatCore,
			const set<AsgnSet> & inputStates, const set<string> & notZero);

	TermPtr mineStates(set<AsgnSet> & statesToRemove,
			const set<AsgnSet> & inputStates, const set<string> & notZero);

	void analyzeTraceStates(TermVec & rv);

public:

	RSM(Options & opt, Model & model, StateManager & smgr);

	~RSM(void);

	void analyzeTrace(TermVec & rv, size_t st);

	size_t getNSuccessfulAttempts(void) {
		return nRSM;
	}

	clock_t satTime(void) {
		return t;
	}

	size_t getNQuery(void) {
		return nQuery;
	}

};

}

#endif /* RSM_H_ */
