/*
 * RSM.cpp
 *
 *  Created on: Jan 8, 2014
 *      Author: Johannes Birgmeier
 */

#include "RSM.h"

#include <algorithm>

using namespace std;

namespace CTIGAR {

RSM::RSM(Options & opt, Model & model, StateManager & smgr) :
		model(model), verbosity(opt.verbosity), smgr(smgr), halfPlaneMinPoints(
				opt.halfPlaneMinPoints), halfPlaneMaxPoints(
				opt.halfPlaneMaxPoints), opt(opt), traceMiner(NULL), nRSM(0), t(
				0), nQuery(0) {
	for (VarTypeMap::const_iterator it = model.vars.program.begin();
			it != model.vars.program.end(); ++it) {
		if (it->second != VarType::INT || it->first == ".s") continue;
		intVars.insert(VarTypeMap::value_type(it->first, it->second));
	}
}

RSM::~RSM(void) {
}

void RSM::collectOccurringVars(set<string> & occuringVars,
		const set<AsgnSet> & asgns) const {
	for (set<AsgnSet>::const_iterator it = asgns.begin(); it != asgns.end();
			++it) {
		for (AsgnSet::const_iterator it2 = it->begin(); it2 != it->end();
				++it2) {
			assert(it2->getVar() != ".s");
			occuringVars.insert(it2->getVar());
		}
	}
}

string RSM::createLines(TermVec & ret, map<string, string> & coeffMap,
		map<string, string> & coeffRevMap, map<TermPtr, AsgnSet> & coreMap,
		const set<AsgnSet> & inputStates, string & cst) const {
	string rv;
	set<string> occurringVars;
	collectOccurringVars(occurringVars, inputStates);
	unsigned i = 0;
	for (set<string>::const_iterator it = occurringVars.begin();
			it != occurringVars.end(); ++it) {
		string coeff = ".traceMiner.coeff." + to_string(i);
		traceMiner->declareIntVar(coeff, true);
		if (verbosity >= 2)
			cout << "Mining map element: " << coeff << " " << *it << endl;
		coeffMap.insert(map<string, string>::value_type(coeff, *it));
		coeffRevMap.insert(map<string, string>::value_type(*it, coeff));
		++i;
	}

	for (set<AsgnSet>::iterator it = inputStates.begin();
			it != inputStates.end(); ++it) {
		assert(it->size() == occurringVars.size());
		TermPtr inner = tf.mkInt(0);
		for (AsgnSet::const_iterator it2 = it->begin(); it2 != it->end();
				++it2) {
			TermPtr var = tf.mkInt(it2->getNumVal());
			TermPtr coeff = tf.mkIntVar(coeffRevMap[it2->getVar()]);
			TermPtr prod = tf.mkTimes(coeff, var);
			inner = tf.mkPlus(inner, prod);
		}
		TermPtr cstT = tf.mkIntVar(cst);
		inner = tf.mkComparison(ComparisonType::EQ, inner, cstT);
		coreMap.insert(map<TermPtr, AsgnSet>::value_type(inner, *it));
		if (verbosity >= 2) cout << "Mining line: " << *inner << endl;
		ret.push_back(inner);
	}

	return rv;
}

TermPtr RSM::extractLine(map<string, string> & coeffMap, string & cst) {
	SatModel mod;
	traceMiner->getModel(mod, false);
	Assignment * cstAsgn = NULL;
	map<string, Assignment> varAsgns;
	for (SatModel::iterator it = mod.begin(); it != mod.end(); ++it) {
		if (it->getVar() == cst) {
			cstAsgn = new Assignment(*it);
			continue;
		}
		map<string, string>::iterator mapIt = coeffMap.find(it->getVar());
		if (mapIt == coeffMap.end()) {
			continue;
		}
		varAsgns.insert(
				map<string, Assignment>::value_type(mapIt->second, *it));
	}
	assert(cstAsgn != NULL);
	assert(varAsgns.size() == coeffMap.size());
	TermPtr ret = tf.mkInt(0);
	for (map<string, Assignment>::const_iterator it = varAsgns.begin();
			it != varAsgns.end(); ++it) {
		TermPtr coeff = tf.mkInt(it->second.getNumVal());
		TermPtr var = tf.mkIntVar(it->first);
		TermPtr prod = tf.mkTimes(coeff, var);
		ret = tf.mkPlus(ret, prod);
	}
	TermPtr cstT = tf.mkInt(cstAsgn->getNumVal());
	ret = tf.mkComparison(ComparisonType::EQ, ret, cstT);
	if (verbosity >= 2) cout << ret << endl;
	return ret;
}

TermPtr RSM::mkNotZero(map<string, string> & coeffMap,
		const set<string> & notZeroCoeff) {
	assert(coeffMap.size() > 0);
	TermPtr ret = tf.mkFalse();
	TermPtr zero = tf.mkInt(0);
	for (map<string, string>::const_iterator it = coeffMap.begin();
			it != coeffMap.end(); ++it) {
		if (notZeroCoeff.find(it->first) != notZeroCoeff.end()) {
			continue;
		}
		TermPtr var = tf.mkIntVar(it->first);
		TermPtr inner = tf.mkComparison(ComparisonType::NEQ, var, zero);
		ret = tf.mkOr(ret, inner);
	}
	for (set<string>::const_iterator it = notZeroCoeff.begin();
			it != notZeroCoeff.end(); ++it) {
		TermPtr var = tf.mkIntVar(*it);
		TermPtr inner = tf.mkComparison(ComparisonType::NEQ, var, zero);
		ret = tf.mkAnd(ret, inner);
	}
	return ret;
}

TermPtr RSM::mkNotLine(AsgnSet & line) {
	TermPtr clause = tf.mkFalse();
	for (AsgnSet::const_iterator it = line.begin(); it != line.end(); ++it) {
		TermPtr asgn = tf.asgnToTerm(*it);
		TermPtr nAsgn = tf.mkNot(asgn);
		clause = tf.mkOr(clause, nAsgn);
	}
	return clause;
}

TermPtr RSM::halfPlane(set<AsgnSet> & unsatCore,
		const set<AsgnSet> & inputStates, const set<string> & notZero) {
	TermPtr rv = tf.mkErrorTerm();
	assert(inputStates.size() >= 2);
	if (verbosity >= 2)
		cout << "Mining over " << inputStates.size() << " points" << endl;
	map<string, string> coeffMap;
	map<string, string> coeffRevMap;
	string cst = ".traceMiner.cst";
	traceMiner->declareIntVar(cst, true);
	map<TermPtr, AsgnSet> coreMap;
	TermVec lines;
	createLines(lines, coeffMap, coeffRevMap, coreMap, inputStates, cst);
	set<string> notZeroCoeffs;
	for (set<string>::const_iterator it = notZero.begin(); it != notZero.end();
			++it) {
		assert(coeffRevMap.find(*it) != coeffRevMap.end());
		notZeroCoeffs.insert(coeffRevMap[*it]);
	}
	traceMiner->push();
	traceMiner->assertFormula(mkNotZero(coeffMap, notZeroCoeffs));
	map<size_t, TermPtr> termIdMap;
	for (TermVec::const_iterator it = lines.begin(); it != lines.end(); ++it) {
		size_t lineId = traceMiner->assertAndTrack(*it);
		termIdMap.insert(map<size_t, TermPtr>::value_type(lineId, *it));
	}
	if (traceMiner->sat()) {
		TermPtr substLine = extractLine(coeffMap, cst);
		if (verbosity >= 1) cout << "Mining found line: " << *substLine << endl;
		rv = substLine;
		++nRSM;
	} else {
		vector<size_t> core;
		traceMiner->getUnsatCore(core);
		if (verbosity >= 2) cout << "Mining core size: " << core.size() << endl;
		for (vector<size_t>::const_iterator it = core.begin(); it != core.end();
				++it) {
			map<size_t, TermPtr>::iterator fromId = termIdMap.find(*it);
			if (fromId == termIdMap.end()) continue;
			map<TermPtr, AsgnSet>::iterator mapIt = coreMap.find(
					fromId->second);
			if (mapIt == coreMap.end()) continue;
			unsatCore.insert(mapIt->second);
		}
	}
	traceMiner->pop();
	return rv;
}

TermPtr RSM::mineStates(set<AsgnSet> & statesToRemove,
		const set<AsgnSet> & inputStates, const set<string> & notZero) {
	if (inputStates.size() <= 2) return tf.mkErrorTerm();
	set<AsgnSet> unsatCore;
	TermPtr rv = halfPlane(unsatCore, inputStates, notZero);
	if (rv->type != TermType::ERROR) {
		statesToRemove.insert(inputStates.begin(), inputStates.end());
		return rv;
	} else {
		assert(unsatCore.size() >= 2);
		set<AsgnSet> noCore;
		set_difference(inputStates.begin(), inputStates.end(),
				unsatCore.begin(), unsatCore.end(),
				inserter(noCore, noCore.end()));
		set<AsgnSet> input2;
		input2.insert(*unsatCore.begin());
		input2.insert(noCore.begin(), noCore.end());
		TermPtr rv = mineStates(statesToRemove, input2, notZero);
		if (rv->type != TermType::ERROR) return rv;
		set<AsgnSet>::iterator it = unsatCore.begin();
		++it;
		input2.clear();
		input2.insert(it, unsatCore.end());
		input2.insert(noCore.begin(), noCore.end());
		rv = mineStates(statesToRemove, input2, notZero);
		if (rv->type != TermType::ERROR) return rv;
		if (inputStates.size() > halfPlaneMaxPoints) {
			statesToRemove.insert(inputStates.begin(), inputStates.end());
		}
		return tf.mkErrorTerm();
	}
}

void RSM::analyzeTraceStates(TermVec & ret) {
	if (halfPlaneMinPoints == 0) return;
	if (verbosity >= 1) cout << "Starting mining" << endl;
	for (TraceBaseMap::const_iterator pcPointsSet = traceBase.getMap().begin();
			pcPointsSet != traceBase.getMap().end(); ++pcPointsSet) {
		// TODO classification of points can be done at insertion of points
		// doing this at every mining is redundant
		map<set<string>, set<AsgnSet> > pointClasses;
		classifyPoints(pointClasses, pcPointsSet->second);
		for (map<set<string>, set<AsgnSet> >::const_iterator equalVarsPoints =
				pointClasses.begin(); equalVarsPoints != pointClasses.end();
				++equalVarsPoints) {
			set<AsgnSet> statesToRemove;

			bool rv = false;

			if (!rv) {
				TermPtr line = mineStates(statesToRemove,
						equalVarsPoints->second, set<string>());
				if (line->type != TermType::ERROR) {
					ret.push_back(line);
				}
			}

			if (!statesToRemove.empty()) {
				if (verbosity >= 2)
					cout << "Mining states to remove: " << statesToRemove.size()
							<< endl;
				set<AsgnSet> setDiff;
				set_difference(pcPointsSet->second.begin(),
						pcPointsSet->second.end(), statesToRemove.begin(),
						statesToRemove.end(), inserter(setDiff, setDiff.end()));
				if (verbosity >= 3)
					cout << "Mining set diff size: " << setDiff.size() << endl;
				traceBase.set(pcPointsSet->first, setDiff);
			}
		}
	}
	if (verbosity >= 1) cout << "Stopping mining" << endl;
}

void RSM::analyzeTrace(TermVec & ret, size_t st) {
	size_t pc = smgr.state(st).getPcVal();
	AsgnVec intsOnly;
	const AsgnVec & lifted = smgr.state(st).lifted;
	for (AsgnVec::const_iterator it = lifted.begin(); it != lifted.end();
			++it) {
		if (it->getVar() == ".s") continue;
		switch (it->getType()) {
		case AsgnType::INT_ASGN:
			intsOnly.push_back(*it);
			break;
		case AsgnType::DYN_ARRAY_ASGN: {
			Assignment asgn(it->getArraySizeVar(), it->getArraySize());
			intsOnly.push_back(asgn);
			break;
		}
		case AsgnType::BOOL_ASGN:
		case AsgnType::ARRAY_ASGN:
			break;
		default:
			assert(false);
		}
	}
	traceBase.fillInState(pc, intsOnly, 2);
	if (verbosity >= 2) cout << "Trace Analysis: " << endl << traceBase << endl;
	traceMiner = new MSatSMTEnvironment("traceMiner", intVars, opt, true, true,
			false);
	TermVec rv;
	analyzeTraceStates(rv);
	t += traceMiner->satTime();
	nQuery += traceMiner->getNQuery();
	for (TermVec::const_iterator it = rv.begin(); it != rv.end(); ++it) {
		ret.push_back(*it);
	}
	delete traceMiner;
	traceMiner = NULL;
}

}
