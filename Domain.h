/*
 * Domain.h
 *
 *  Created on: May 14, 2014
 *      Author: birgmei
 */

#ifndef DOMAIN_H_
#define DOMAIN_H_

#include <stddef.h>
#include <cassert>
#include <iterator>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include "Model.h"
#include "Term.h"

using namespace std;

namespace CTIGAR {

class Domain {

	map<string, size_t> termReprs;
	vector<Predicate *> preds;
	map<string, size_t> boolVarsToIdx;
	SMTEnvironment * env;
	const Options & opt;
	TermFactory tf;

public:

	Domain(SMTEnvironment * env1, const Options & opt1) :
			env(env1), opt(opt1) {
	}

	~Domain() {
		for (vector<Predicate *>::iterator it = preds.begin();
				it != preds.end(); ++it) {
			delete *it;
		}
	}

	size_t add(TermPtr toAdd, Model & model, size_t * redundant = NULL);

	TermPtr term(size_t idx);

	const Predicate & node(size_t idx);

	size_t size(void) const;

	size_t boolVarIdx(string var) {
		try {
			size_t ret = boolVarsToIdx.at(var);
			return ret;
		} catch (out_of_range &) {
			assert(false);
			return 0;
		}
	}

	const vector<Predicate *> & getPreds(void) {
		return preds;
	}

};

}

#endif /* DOMAIN_H_ */
