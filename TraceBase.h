/*
 * TraceBase.h
 *
 *  Created on: May 19, 2014
 *      Author: birgmei
 */

#ifndef TRACEBASE_H_
#define TRACEBASE_H_

#include <map>
#include <set>
#include <iostream>

#include "Assignment.h"
#include "State.h"

using namespace std;

namespace CTIGAR {

typedef map<size_t, set<AsgnSet>> TraceBaseMap;

class TraceBase {

	TraceBaseMap tbm;

public:
	void fillInState(size_t pc, const AsgnVec & st, size_t minSize);

	friend std::ostream & operator<<(std::ostream & stream,
			const TraceBase & trB);

	const TraceBaseMap & getMap(void) const {
		return tbm;
	}

	void set(size_t i, const set<AsgnSet> & asgns) {
		tbm[i] = asgns;
	}

	std::set<AsgnSet> & operator[](size_t const& i) {
		return tbm[i];
	}

};

}

#endif /* TRACEBASE_H_ */
