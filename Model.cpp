/*
 * Model.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

#include "Model.h"

#include <algorithm>
#include <sstream>
#include <fstream>
#include <stdexcept>

#include "Z3SMTEnvironment.h"
#include "myutil.h"

using namespace std;

namespace CTIGAR {

std::ostream & operator<<(std::ostream & stream, const AbstractState & st) {
	for (AbstractState::const_iterator it = st.begin(); it != st.end(); ++it) {
		stream << *it << " ";
	}
	return stream;
}

std::ostream & operator<<(std::ostream & stream, const AsgnVec & st) {
	set<Assignment> asgnSet(st.begin(), st.end());
	for (set<Assignment>::const_iterator it = asgnSet.begin();
			it != asgnSet.end(); ++it) {
		stream << *it << ", ";
	}
	return stream;
}

std::ostream & operator<<(std::ostream & stream, const AsgnSet & asgnSet) {
	for (set<Assignment>::const_iterator it = asgnSet.begin();
			it != asgnSet.end(); ++it) {
		stream << *it << ", ";
	}
	return stream;
}

std::ostream & operator<<(std::ostream & stream, const CubeSet & st) {
	for (CubeSet::const_iterator it = st.begin(); it != st.end(); ++it) {
		stream << *it << " | ";
	}
	return stream;
}

void Model::getPredecessors(set<long> & ret, long fromPC, long toPC) {
	assert(ret.empty());
	for (long i = fromPC; i <= toPC; ++i) {
		ret.insert(preds[i].begin(), preds[i].end());
	}
}

void Model::loadAsrtsFile(ifstream & asrtsFile, SMTEnvironment * parser) {
	string line;
	bool allPred = false;
	while (getline(asrtsFile, line)) {
		TermPtr prop = parser->parsePredicate(line);
		properties.push_back(prop);
		// TODO fix this for implication properties
		if (!allPred && prop->type == TermType::NOT
				&& prop->getArg(0)->type == TermType::EQ) {
			TermPtr eq = prop->getArg(0);
			TermPtr l = eq->getArg(0);
			TermPtr r = eq->getArg(1);
			if (l->isVar() && l->getName() == ".s"
					&& r->type == TermType::INTEGER) {
				long pc = r->toLong();
				set<long> & pr = preds[pc];
				errorPreds.insert(pr.begin(), pr.end());
			} else if (r->isVar() && r->getName() == ".s"
					&& l->type == TermType::INTEGER) {
				long pc = l->toLong();
				set<long> & pr = preds[pc];
				errorPreds.insert(pr.begin(), pr.end());
			} else {
				allPred = true;
			}
		} else {
			allPred = true;
		}
	}
	if (allPred) {
		for (int i = 0; i <= maxPC; ++i) {
			errorPreds.insert(i);
		}
		for (VarTypeMap::const_iterator it = vars.program.begin();
				it != vars.program.end(); ++it) {
			errorVars.insert(it->first);
		}
	} else {
		errorVars.insert(".s");
	}
}

void Model::loadTrFile(ifstream & trFile) {
	string line;
	set<int> predsForAll;
	while (getline(trFile, line)) {
		istringstream iss(line);
		string pcval;
		getline(iss, pcval, ';');
		assert(pcval.size() > 0);
		errno = 0;
		assert(isNumber(pcval));
		long pc = strtol(pcval.c_str(), NULL, 10);
		assert(pc >= 0);
		if (maxPC < pc) maxPC = pc;
		string condition;
		getline(iss, condition, ';');
		string var;
		getline(iss, var, ';');
		assert(var.size() > 0);
		assert(vars.inputs.find(var) == vars.inputs.end());
		bool isTransition = false;
		if (var == ".s") {
			isTransition = true;
		}
		string update;
		getline(iss, update, ';');
		assert(update.size() > 0);
		string function;
		getline(iss, function, ';');
		assert(function != ".s");
		long succ = -1;
		if (isTransition) {
			assert(function.size() > 0);
			if (isNumber(function)) {
				errno = 0;
				succ = strtol(function.c_str(), NULL, 10);
				assert(errno == 0);
				preds[succ].insert(pc);
			} else {
				predsForAll.insert(pc);
			}
		}
		string conjunct;
		if (condition.size() > 0)
			conjunct = "(or (not (= .s " + pcval + ")) (or (not " + condition
					+ ")  " + update + "))";
		else
			conjunct = "(or (not (= .s " + pcval + ")) " + update + ")";
		assert(
				!condition.empty() || cfgMap[pc][var].empty()
						|| vars.all[var] == VarType::DYN_ARRAY);
		cfgMap[pc][var].insert(conjunct);
		trStrings.insert(conjunct);
		if (opt.karr) {
			if (succ != -1) {
				karrFactory->karrSucc(pc, succ, condition);
			} else {
				if (function.size() > 0) {
					karrFactory->karrLoad(pc, condition, var, function);
				}
			}
		}
	}
	for (int i = 0; i <= maxPC; ++i) {
		preds[i].insert(predsForAll.begin(), predsForAll.end());
	}
	if (opt.verbosity >= 3) {
		cout << "Predecessor map:" << endl;
		for (PredecessorMap::const_iterator it = preds.begin();
				it != preds.end(); ++it) {
			cout << it->first << " <- ";
			for (set<long>::const_iterator it2 = it->second.begin();
					it2 != it->second.end(); ++it2) {
				cout << *it2 << ", ";
			}
			cout << endl;
		}
	}
}

void Model::loadVarsFile(ifstream & varsFile) {
	string line;
	while (getline(varsFile, line)) {
		istringstream iss(line);
		string typeString;
		getline(iss, typeString, ' ');
		string var;
		getline(iss, var, ' ');
		VarType::E type = typeFromString(typeString);
		if (type == VarType::DYN_ARRAY) {
			string sizeFunction;
			getline(iss, sizeFunction, ' ');
			assert(vars.all.find(sizeFunction) != vars.all.end());
			assert(vars.all[sizeFunction] == VarType::INT);
			vars.dynArraySize.insert(DynSizeMap::value_type(var, sizeFunction));
			vars.dynArraySize.insert(
					DynSizeMap::value_type(var + "_p", sizeFunction + "_p"));
			if (opt.karr)
				cout << "Have to disable Karr analysis; program contains arrays"
						<< endl;
			opt.karr = false;
			hasArrays = true;
		} else if (type == VarType::FIXED_ARRAY) {
			string sizeInt;
			getline(iss, sizeInt, ' ');
			assert(isNumber(sizeInt));
			long l = strtol(sizeInt.c_str(), NULL, 10);
			assert(l >= 0);
			vars.fixedArraySize.insert(FixedSizeMap::value_type(var, l));
			vars.fixedArraySize.insert(FixedSizeMap::value_type(var + "_p", l));
			if (opt.karr)
				cout << "Have to disable Karr analysis; program contains arrays"
						<< endl;
			opt.karr = false;
			hasArrays = true;
		}
		assert(typeString != "input_array");
		vars.toPrime.insert(VarPrimeMap::value_type(var, var + "_p"));
		vars.fromPrime.insert(VarPrimeMap::value_type(var + "_p", var));
		vars.all.insert(VarTypeMap::value_type(var, type));
		vars.all.insert(VarTypeMap::value_type(var + "_p", type));
		vars.program.insert(VarTypeMap::value_type(var, type));
	}
	if (vars.program.empty()) {
		cerr
				<< "No program variables found. Did you prepare the program with TRGen2?"
				<< endl;
		exit(-1);
	}
}

void Model::loadNondetFile(ifstream & nondetFile) {
	MSatSMTEnvironment * parser = new MSatSMTEnvironment("parser", vars.all,
			opt);
	string line;
	while (getline(nondetFile, line)) {
		istringstream iss(line);
		string typeString;
		getline(iss, typeString, ' ');
		string inputVar;
		getline(iss, inputVar, ' ');
		VarType::E type = typeFromString(typeString);
		if (type == VarType::INPUT_ARRAY) {
			string sizeFunction;
			getline(iss, sizeFunction, ' ');
			TermPtr sizeFunctionTerm = parser->parseString(sizeFunction);
			vars.inputArraySize.insert(
					InputSizeMap::value_type(inputVar, sizeFunctionTerm));
			hasArrays = true;
		}
		assert(type != VarType::DYN_ARRAY && type != VarType::FIXED_ARRAY);
		if (opt.verbosity >= 1) cout << "Input var: " << inputVar << endl;
		vars.inputs.insert(VarTypeMap::value_type(inputVar, type));
		vars.all.insert(VarTypeMap::value_type(inputVar, type));
	}
	delete parser;
}

Model::Model(string progPrefix, Options & opt1) :
		maxPC(-1), hasArrays(false), opt(opt1) {
	ifstream varsFile(progPrefix + ".vars");
	loadVarsFile(varsFile);
	varsFile.close();

	ifstream nondetFile(progPrefix + ".nondet");
	loadNondetFile(nondetFile);
	nondetFile.close();

	Z3SMTEnvironment * parser = new Z3SMTEnvironment("parser", vars.all, opt,
			vars.dynArraySize, vars.fixedArraySize, vars.inputArraySize);

	if (opt.karr) {
		karrFactory = new KarrCFGFactory(vars.program, vars.inputs, opt);
	}

	ifstream initFile(progPrefix + ".init");
	string initString;
	while (getline(initFile, initString)) {
		if (opt.karr) {
			karrFactory->karrInit(initString);
		}
		TermPtr i = parser->parsePredicate(initString);
		initial.push_back(i);
	}
	if (initial.empty()) {
		cerr << "Initial condition is empty" << endl;
		exit(5);
	}
	initFile.close();

	ifstream trFile(progPrefix + ".tr");
	loadTrFile(trFile);
	trFile.close();

	ifstream asrtsFile(progPrefix + ".asrts");
	loadAsrtsFile(asrtsFile, parser);
	asrtsFile.close();
	if (properties.size() == 0) {
		cerr << "Must find at least one property to prove" << endl;
		exit(4);
	}

	ifstream trxFile(progPrefix + ".trx");
	string trx;
	while (getline(trxFile, trx)) {
		if (opt.verbosity >= 1) cout << "TRX: " << trx << endl;
		trxs.push_back(parser->parsePredicate(trx));
	}
	trxFile.close();

	if (opt.karr) {
		if (opt.verbosity >= 3) karrFactory->printCFG();
		karr = new Karr(karrFactory, opt);
		karr->perform();
		karr->getLines(lines);
	}
	delete parser;
}

Model::~Model() {
	if (opt.karr) {
		delete karr;
		delete karrFactory;
	}
}

TermPtr Model::getError(void) const {
	TermPtr property = getProperty();
	TermPtr ret = tf.mkNot(property);
	return ret;
}

const TermVec & Model::getInitialCondition(void) const {
	return initial;
}

void Model::loadInitialCondition(SMTEnvironment * env) {
	TermPtr andInitial = tf.mkAnd(initial);
	env->assertFormula(andInitial);
}

TermPtr Model::getProperty(void) const {
	assert(properties.size() > 0);
	TermPtr property = tf.mkAnd(properties);
	return property;
}

void Model::loadProperty(SMTEnvironment * env) {
	TermPtr property = getProperty();
	env->assertFormula(property);
}

void Model::loadError(SMTEnvironment * env) {
	TermPtr errorTerm = getError();
	env->assertFormula(errorTerm);
}

TermPtr Model::getPrimedError(void) const {
	TermPtr property = getProperty();
	TermPtr primedProperty = primeTerm(property);
	return tf.mkNot(primedProperty);
}

void Model::loadPrimedError(SMTEnvironment * env) {
	TermPtr property = getProperty();
	TermPtr primedProperty = primeTerm(property);
	TermPtr negatedPrimedProperty = tf.mkNot(primedProperty);
	env->assertFormula(negatedPrimedProperty);
}

void Model::loadPrimedProperty(SMTEnvironment * env) {
	TermPtr property = getProperty();
	TermPtr primedProperty = primeTerm(property);
	env->assertFormula(primedProperty);
}

TermPtr Model::primeTerm(TermPtr term) const {
	if (term->isVar()) {
		string varName = term->getName();
		try {
			string primedVarName = vars.toPrime.at(varName);
			VarType::E type = vars.all.at(varName);
			switch (type) {
			case VarType::INT:
				return tf.mkIntVar(primedVarName);
			case VarType::BOOL:
				return tf.mkBoolVar(primedVarName);
			case VarType::DYN_ARRAY:
			case VarType::FIXED_ARRAY:
				return tf.mkArrayVar(primedVarName);
			default:
				assert(false);
			}
		} catch (out_of_range &) {
			return term;
		}
	}
	if (term->type == TermType::INTEGER || term->type == TermType::TRUE
			|| term->type == TermType::FALSE) {
		return term;
	} else {
		TermVec args;
		term->getArgs(args);
		TermVec primedArgs;
		for (size_t i = 0; i < args.size(); ++i) {
			primedArgs.push_back(primeTerm(args[i]));
		}
		return tf.copyApp(term, primedArgs);
	}
}

TermPtr Model::unprimeTerm(TermPtr term) const {
	if (term->isVar()) {
		string varName = term->getName();
		try {
			string unprimedVarName = vars.fromPrime.at(varName);
			VarType::E type = vars.all.at(varName);
			switch (type) {
			case VarType::INT:
				return tf.mkIntVar(unprimedVarName);
			case VarType::BOOL:
				return tf.mkBoolVar(unprimedVarName);
			case VarType::DYN_ARRAY:
			case VarType::FIXED_ARRAY:
				return tf.mkArrayVar(unprimedVarName);
			default:
				assert(false);
			}
		} catch (out_of_range &) {
			return term;
		}
	}
	if (term->type == TermType::INTEGER || term->type == TermType::TRUE
			|| term->type == TermType::FALSE) {
		return term;
	} else {
		TermVec args;
		term->getArgs(args);
		TermVec unprimedArgs;
		for (size_t i = 0; i < args.size(); ++i) {
			unprimedArgs.push_back(unprimeTerm(args[i]));
		}
		return tf.copyApp(term, unprimedArgs);
	}
}

bool Model::isUnprimedVariable(string var) const {
	return vars.toPrime.find(var) != vars.toPrime.end();
}

bool Model::isProgramCounter(string var) const {
	return var == ".s";
}

bool Model::hasBoundVars(const TermPtr idx) const {
	if (idx->type == TermType::BOUND_VAR) return true;
	for (TermVec::const_iterator it = idx->getArgs().begin();
			it != idx->getArgs().end(); ++it) {
		if (hasBoundVars(*it)) return true;
	}
	return false;
}

void Model::safeArrayAccess(TermVec & safetyConditions,
		const TermPtr predicate) {
	if (predicate->type == TermType::SELECT) {
		TermPtr var = predicate->getArg(0);
		TermPtr idx = predicate->getArg(1);
		if (!hasBoundVars(idx)) {
			assert(var->type == TermType::ARRAY_VAR);
			TermPtr geq = tf.mkComparison(ComparisonType::GEQ, idx,
					tf.mkInt(0));
			VarType::E varType = vars.all.at(var->getName());
			TermPtr lt;
			if (varType == VarType::DYN_ARRAY) {
				lt = tf.mkComparison(ComparisonType::LT, idx,
						tf.mkIntVar(vars.dynArraySize.at(var->getName())));
			} else if (varType == VarType::FIXED_ARRAY) {
				lt = tf.mkComparison(ComparisonType::LT, idx,
						tf.mkInt(vars.fixedArraySize.at(var->getName())));
			} else {
				assert(false);
			}
			safetyConditions.push_back(geq);
			safetyConditions.push_back(lt);
		}
	}
	for (vector<TermPtr>::const_iterator it = predicate->getArgs().begin();
			it != predicate->getArgs().end(); ++it) {
		safeArrayAccess(safetyConditions, *it);
	}
}

}

