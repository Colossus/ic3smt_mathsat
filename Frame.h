/*
 * Frame.h
 *
 *  Created on: Dec 19, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef FRAME_H_
#define FRAME_H_

#include <memory>
#include <stddef.h>

#include "Term.h"

using namespace std;

namespace CTIGAR {

// For IC3's overall frame structure.
struct Frame {
	bool initialized;
	size_t k;             // steps from initial state
	CubeSet borderCubes;  // additional cubes in this and previous frames
	Z3SMTEnvironment * consecution;
	TermPtr actLit;
	TermVec actLits;

	Frame() :
			initialized(false), k(-1), consecution(NULL) {
	}

	void addClause(TermPtr clause) {
		consecution->assertFormula(clause, actLit);
	}

	bool solve(TermVec & addALs) {
		actLits.insert(actLits.end(), addALs.begin(), addALs.end());
		bool rv = consecution->sat(actLits);
		actLits.erase(actLits.end() - addALs.size(), actLits.end());
		return rv;
	}

	bool solve(TermPtr al) {
		actLits.push_back(al);
		bool rv = consecution->sat(actLits);
		actLits.pop_back();
		return rv;
	}
};

}

#endif /* FRAME_H_ */
