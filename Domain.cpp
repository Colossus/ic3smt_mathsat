/*
 * Domain.cpp
 *
 *  Created on: May 14, 2014
 *      Author: birgmei
 */

#include "Domain.h"

namespace CTIGAR {

size_t Domain::add(TermPtr toAdd, Model & model, size_t * redundant) {
	string repr = toAdd->toString();
	map<string, size_t>::const_iterator it = termReprs.find(repr);
	if (it != termReprs.end()) {
		if (opt.verbosity >= 3)
			cout << "Redundant predicate (syntactic) (" << it->second + 1 << ") "
					<< repr << endl;
		if (redundant != NULL) {
			*redundant = termReprs[repr];
		}
		return (size_t) -1;
	}

	set<size_t> supporters;
	if (model.getHasArrays()) {
		TermVec safetyConditions;
		model.safeArrayAccess(safetyConditions, toAdd);
		for (TermVec::const_iterator it = safetyConditions.begin();
				it != safetyConditions.end(); ++it) {
			size_t redundant = (size_t) -1;
			size_t supporter = add(*it, model, &redundant);
			if (supporter == ((size_t) -1)) {
				supporter = redundant;
			}
			assert(supporter != ((size_t ) -1));
			supporters.insert(supporter);
		}
	}

	size_t idx = preds.size();
	termReprs.insert(map<string, size_t>::value_type(repr, idx));
	Predicate * pred = new Predicate(env, toAdd, idx, supporters);
	boolVarsToIdx.insert(
			map<string, size_t>::value_type(pred->getBoolVarName(),
					pred->getIdx()));
	preds.push_back(pred);

	if (opt.verbosity >= 1) {
		cout << "Refining domain with " << pred->getIdx() + 1 << ": " << repr
				<< " (supporters: ";
		for (set<size_t>::const_iterator it = supporters.begin();
				it != supporters.end(); ++it) {
			cout << (*it) + 1 << " ";
		}
		cout << ")" << endl;
	}

	return pred->getIdx();
}

TermPtr Domain::term(size_t idx) {
	return preds[idx]->getTerm();
}

const Predicate & Domain::node(size_t idx) {
	return *preds[idx];
}

size_t Domain::size(void) const {
	return preds.size();
}

}
