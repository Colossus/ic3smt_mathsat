/*
 * KarrCFG.h
 *
 *  Created on: Jan 23, 2014
 *      Author: johannes
 */

#ifndef CFG_H_
#define CFG_H_

#include <vector>
#include <map>
#include <string>

#include "MSatSMTEnvironment.h"
#include "main.h"

using namespace std;

namespace CTIGAR {

typedef vector<long> Affine;

void printAffine(const Affine & aff, const vector<string> & vars);

void subtract(Affine & ret, const Affine & a, const Affine & b);

void subtract(Affine & inOut, const Affine & b);

void add(Affine & inOut, const Affine & b);

void multiply(Affine & ret, const Affine & a, long factor);

void multiply(Affine & a, long factor);

long gcd(long a, long b);

long lcm(long a, long b);

class InputVariableException: public exception {
};

struct CFGTrans;

struct CFGNode {

	long pc;
	vector<CFGTrans *> succs;

	CFGNode(long pc1) :
			pc(pc1) {
	}

};

struct CFGTrans {

	vector<string> & vars;
	vector<Affine> stmt;
	CFGNode * next;

	CFGTrans(vector<string> & vars1) :
			vars(vars1), next(NULL) {
		stmt.resize(vars.size());
	}

};

class KarrCFGFactory {
	friend class Karr;

	vector<CFGNode *> allNodes;
	vector<CFGTrans *> allTrans;
	map<long, CFGNode *> pcToNode;
	MSatSMTEnvironment * env;
	map<pair<long, string>, CFGTrans *> pcConditionToTrans;
	map<string, size_t> varIndexMap;
	long initPc;

	TermFactory tf;

	vector<string> programVariables;
	VarTypeMap inputVariables;

	const Options & opt;

public:

	KarrCFGFactory(const VarTypeMap & programVariables,
			const VarTypeMap & inputVariables, const Options & opt);

	~KarrCFGFactory();

	void karrSucc(long pc, long succPc, string condition);

	void karrLoad(long pc, string condition, string var, string function);

	void karrInit(string initString);

	void printCFG(void);

protected:

	void printNode(CFGNode * node, set<CFGNode *> & visited);

	void dissolveTerm(Affine & ret, TermPtr term);

	void dissolveTimes(Affine & ret, TermPtr term)
			throw (InputVariableException);

	void dissolveNumeral(Affine & ret, TermPtr term)
			throw (InputVariableException);

	void dissolveVar(Affine & ret, TermPtr term)
			throw (InputVariableException);

	void add(Affine & ret, const string & varName, long factor);

	void createNode(long pc);

	void createTrans(long pc, string condition);
};

// Implementation following www2.in.tum.de/bib/files/Muller-Olm04Note.pdf
// : A Note on Karr's Algorithm; Mueller-Olm & Seidl
class Karr {

	map<CFGNode *, vector<Affine>> g;
	set<pair<CFGNode *, Affine>> w;
	vector<string> & vars;
	CFGNode * init;
	const VarTypeMap & inputVars;
	map<CFGNode *, vector<Affine>> gb_p;

	Affine empty;

	Options & opt;

public:

	Karr(KarrCFGFactory * factory, Options & opt);

	~Karr();

	void getLines(map<long, vector<Affine>> & lines);

	void perform(void);

	void printTerms(const map<long, vector<Affine>> & lines);

protected:

	void collectDeps(set<size_t> & deps, const vector<Affine> & vecs);

	void extractLines(map<long, vector<Affine>> & lines, CFGNode * v);

	void initialize(void);

	void apply(vector<Affine> & ret, const vector<Affine> & ss,
			const Affine & x);

	bool inHull(Affine & x_p, const Affine & t, CFGNode * v);

	bool isZero(const Affine & x);

	size_t firstNonZeroIndex(const Affine & x);

	void subtractToZero(Affine & inOut, const size_t index,
			const Affine & toSubtract);

	bool addToHull(const Affine & t, CFGNode * v);

	void addToBase(Affine & x_p, CFGNode * v);

};

}

#endif /* CFG_H_ */
