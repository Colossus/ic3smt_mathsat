CFLAGS=-g -std=c++0x -Wall -Wextra -Wconversion -pedantic -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS -O0
CXX=g++
LDFLAGS=-lgmp -lmathsat -lz3

OFILES = AbstractDomain.o \
        Domain.o \
        Assignment.o \
        ConsRefAbstrPred.o \
        ConsRefConcrPred.o \
        ConsRef.o \
        CTIGAR.o \
        FormulaHolder.o \
        Frame.o \
        KarrCFG.o \
        LiftRefCAF.o \
        LiftRef.o \
        LiftRefLAF.o \
        main.o \
        Model.o \
        MSatSMTEnvironment.o \
        myutil.o \
        RSM.o \
        SMTEnvironment.o \
        State.o \
        Term.o \
		TraceBase.o \
        VarType.o \
        Z3SMTEnvironment.o

all: CTIGAR


CTIGAR: $(OFILES)
	$(CXX) $(CFLAGS) $(OFILES) -o CTIGAR  $(LDFLAGS)

depend: .depend

.depend: $(wildcard *.cpp)
	rm -f ./.depend
	$(CXX) $(CFLAGS) -MM $^ > ./.depend

include .depend

%.o: %.cpp
	$(CXX) $(CFLAGS) $< -c

clean:
	rm -f *.o CTIGAR .depend *.h.gch

.PHONY: all depend clean
