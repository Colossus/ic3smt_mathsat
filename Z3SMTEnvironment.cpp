#include "Z3SMTEnvironment.h"

#include <assert.h>
#include <gmp.h>
#include <stddef.h>
#include <z3_api.h>
#include <iostream>
#include <iterator>
#include <set>
#include <utility>
#include <vector>

#include "Assignment.h"
#include "Term.h"
#include "VarType.h"

namespace CTIGAR {

void Z3SMTEnvironment::checkError() const {
	Z3_error_code e = Z3_get_error_code(ctx);
	if (e != Z3_OK) {
		cerr << Z3_get_error_msg_ex(ctx, e) << endl;
		assert(false);
	}
}

Z3SMTEnvironment::Z3SMTEnvironment(string name, const VarTypeMap & variables,
		const Options & opt, DynSizeMap & dynBoundsMap,
		FixedSizeMap & fixedBoundsMap, InputSizeMap & inputBoundsMap) :
		SMTEnvironment(name, opt) {
	Z3_config cfg;
	cfg = Z3_mk_config();
	ctx = Z3_mk_context(cfg);
	Z3_del_config(cfg);
	slv = Z3_mk_solver(ctx);
	Z3_solver_inc_ref(ctx, slv);
	Z3_params params = Z3_mk_params(ctx);
	Z3_params_set_uint(ctx, params, Z3_mk_string_symbol(ctx, ":random-seed"),
			opt.z3Random);
	Z3_solver_set_params(ctx, slv, params);
	// checkError();

	for (VarTypeMap::const_iterator it = variables.begin();
			it != variables.end(); ++it) {
		switch (it->second) {
		case VarType::INT:
			declareIntVar(it->first, true);
			break;
		case VarType::BOOL:
			declareBoolVar(it->first, true);
			break;
		case VarType::DYN_ARRAY:
			assert(dynBoundsMap.find(it->first) != dynBoundsMap.end());
			declareDynArray(it->first, true, dynBoundsMap[it->first]);
			break;
		case VarType::FIXED_ARRAY:
			assert(fixedBoundsMap.find(it->first) != fixedBoundsMap.end());
			declareFixedArray(it->first, true, fixedBoundsMap[it->first]);
			break;
		case VarType::INPUT_ARRAY:
			assert(inputBoundsMap.find(it->first) != inputBoundsMap.end());
			declareInputArray(it->first, true, inputBoundsMap[it->first]);
			break;
		default:
			assert(false);
		}
	}
}

Z3SMTEnvironment::~Z3SMTEnvironment() {
	Z3_solver_dec_ref(ctx, slv);
	Z3_del_context(ctx);
}

Z3_ast Z3SMTEnvironment::toZ3Term(TermPtr term, vector<string> * boundVars) {
	if (toZ3.find(term) != toZ3.end()) {
		return toZ3[term];
	}

	Z3_ast ret;
	switch (term->type) {
	case TermType::EQ:
		ret = mkComparison(ComparisonType::EQ,
				toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::LEQ:
		ret = mkComparison(ComparisonType::LEQ,
				toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::AND: {
		vector<Z3_ast> args;
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			args.push_back(toZ3Term(*it, boundVars));
		}
		ret = mkAnd(args.size(), &args[0]);
		break;
	}
	case TermType::OR: {
		vector<Z3_ast> args;
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			args.push_back(toZ3Term(*it, boundVars));
		}
		ret = mkOr(args.size(), &args[0]);
		break;
	}
	case TermType::NOT:
		ret = mkNot(toZ3Term(term->getArg(0), boundVars));
		break;
	case TermType::PLUS:
		ret = mkPlus(toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::TIMES:
		ret = mkTimes(toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::SELECT:
		ret = mkArraySelect(toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::BOOL_VAR:
		ret = mkBoolVar(term->getName());
		break;
	case TermType::INT_VAR:
		ret = mkIntVar(term->getName());
		break;
	case TermType::ARRAY_VAR:
		ret = mkArrayVar(term->getName());
		break;
	case TermType::INTEGER:
		ret = mkInt(term->toLong());
		break;
	case TermType::STORE:
		ret = mkArrayStore(toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars),
				toZ3Term(term->getArg(2), boundVars));
		break;
	case TermType::FALSE:
		ret = mkFalse();
		break;
	case TermType::TRUE:
		ret = mkTrue();
		break;
	case TermType::MOD_CONGR:
		ret = mkModCongr(term->toLong(), toZ3Term(term->getArg(0), boundVars),
				toZ3Term(term->getArg(1), boundVars));
		break;
	case TermType::UNARY_MINUS:
		ret = mkMinus(toZ3Term(term->getArg(0), boundVars));
		break;
	case TermType::FORALL: {
		vector<string> bv;
		if (boundVars != NULL) {
			bv.insert(bv.end(), boundVars->begin(), boundVars->end());
		}
		bv.insert(bv.end(), term->getBoundVars().begin(),
				term->getBoundVars().end());
		assert(!term->getBoundVars().empty());
		ret = mkForAll(term->getBoundVars(), toZ3Term(term->getArg(0), &bv));
		break;
	}
	case TermType::BOUND_VAR:
		assert(boundVars != NULL);
		ret = mkBoundVar(term->getName(), *boundVars);
		break;
	default:
		cerr << *term << endl;
		assert(false);
		ret = mkErrorTerm();
		break;
	}

	toZ3[term] = ret;
	return ret;
}

TermPtr Z3SMTEnvironment::fromZ3Term(Z3_ast term,
		vector<string> * boundVariables) {
	TermPtr ret;

	if (termIsKind(term, TermType::FORALL)) {
		unsigned numBound = Z3_get_quantifier_num_bound(ctx, term);
		vector<string> bv;
		if (boundVariables != NULL) {
			bv.insert(bv.end(), boundVariables->begin(), boundVariables->end());
		}
		vector<string> localBoundVars;
		for (unsigned i = 0; i < numBound; ++i) {
			Z3_symbol name = Z3_get_quantifier_bound_name(ctx, term, i);
			Z3_sort sort = Z3_get_quantifier_bound_sort(ctx, term, i);
			string nameStr = Z3_get_symbol_string(ctx, name);
			Z3_sort_kind sortKind = Z3_get_sort_kind(ctx, sort);
			assert(sortKind == Z3_INT_SORT);
			bv.push_back(nameStr);
			localBoundVars.push_back(nameStr);
		}
		Z3_ast body = Z3_get_quantifier_body(ctx, term);
		TermPtr bodyTerm = fromZ3Term(body, &bv);
		ret = tf.mkForAll(localBoundVars, bodyTerm);
	} else if (termIsVar(term)) {
		Z3_sort s = Z3_get_sort(ctx, term);
		Z3_sort_kind sk = Z3_get_sort_kind(ctx, s);
		if (sk == Z3_BOOL_SORT)
			ret = tf.mkBoolVar(varName(term));
		else if (sk == Z3_INT_SORT)
			ret = tf.mkIntVar(varName(term));
		else if (sk == Z3_ARRAY_SORT)
			ret = tf.mkArrayVar(varName(term));
		else {
			cerr << termToString(term) << endl;
			assert(false);
			ret = NULL;
		}
	} else if (termIsConstArray(term)) {
		Z3_ast arg = termGetArg(term, 0);
		ret = tf.mkConstArray(fromZ3Term(arg, boundVariables));
	} else if (termIsBoundVar(term)) {
		assert(boundVariables != NULL);
		unsigned index = Z3_get_index_value(ctx, term);
		assert(index < boundVariables->size());
		string bound = (*boundVariables)[boundVariables->size() - index - 1];
		ret = tf.mkBoundVar(bound);
	} else if (termIsNumeral(term)) {
		ret = tf.mkInt(termToLong(term));
	} else if (termIsTrue(term)) {
		ret = tf.mkTrue();
	} else if (termIsFalse(term)) {
		ret = tf.mkFalse();
	} else if (termIsKind(term, TermType::UNARY_MINUS)) {
		assert(termNumArgs(term) == 1);
		ret = tf.mkMinus(fromZ3Term(termGetArg(term, 0), boundVariables));
	} else if (termIsKind(term, TermType::EQ)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::EQ,
				fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsComparison(term, ComparisonType::LEQ)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::LEQ,
				fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsComparison(term, ComparisonType::GEQ)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::GEQ,
				fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsComparison(term, ComparisonType::LT)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::LT,
				fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsComparison(term, ComparisonType::GT)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkComparison(ComparisonType::GT,
				fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsKind(term, TermType::AND)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkAnd(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsKind(term, TermType::OR)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkOr(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsKind(term, TermType::NOT)) {
		assert(termNumArgs(term) == 1);
		ret = tf.mkNot(fromZ3Term(termGetArg(term, 0), boundVariables));
	} else if (termIsKind(term, TermType::PLUS)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkPlus(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsKind(term, TermType::TIMES)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkTimes(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsArrayRead(term)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkArraySelect(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else if (termIsArrayWrite(term)) {
		assert(termNumArgs(term) == 3);
		ret = tf.mkArrayStore(fromZ3Term(termGetArg(term, 0), boundVariables),
				fromZ3Term(termGetArg(term, 1), boundVariables),
				fromZ3Term(termGetArg(term, 2), boundVariables));
	} else if (termIsImplication(term)) {
		assert(termNumArgs(term) == 2);
		ret = tf.mkOr(tf.mkNot(fromZ3Term(termGetArg(term, 0), boundVariables)),
				fromZ3Term(termGetArg(term, 1), boundVariables));
	} else {
		cerr << termToString(term) << endl;
		assert(false);
		ret = NULL;
	}

	toZ3[ret] = term;
	return ret;
}

long Z3SMTEnvironment::termToLong(Z3_ast term) const {
	int l;
	Z3_bool ok = Z3_get_numeral_int(ctx, term, &l);
	// checkError();
	assert(ok == Z3_TRUE);
	return l;
}

Z3_ast Z3SMTEnvironment::mkActLitAst(void) {
	string name = ".al." + to_string(nActLit);
	declareBoolVar(name, false);
	nActLit++;
	return mkBoolVar(name);
}

TermPtr Z3SMTEnvironment::mkActLit(void) {
	return fromZ3Term(mkActLitAst(), NULL);
}

void Z3SMTEnvironment::assertFormula(TermPtr term) {
	Z3_ast ast = toZ3Term(term, NULL);
	Z3_solver_assert(ctx, slv, ast);
	// checkError();
}

size_t Z3SMTEnvironment::assertFormula(TermPtr ast, TermPtr actLit) {
	assert(numBT == 0);

	Z3_ast z3Ast = toZ3Term(ast, NULL);
	Z3_ast z3ActLit = toZ3Term(actLit, NULL);
	Z3_ast toAdd = mkOr(z3Ast, mkNot(z3ActLit));
	Z3_solver_assert(ctx, slv, toAdd);
	// checkError();
	return termId(z3ActLit);
}

size_t Z3SMTEnvironment::assertAndTrack(TermPtr term) {
	Z3_ast ast = toZ3Term(term, NULL);
	Z3_ast p = mkActLitAst();
	Z3_solver_assert_and_track(ctx, slv, ast, p);
	// checkError();
	return termId(p);
}

void Z3SMTEnvironment::release(TermPtr actLit) {
	Z3_solver_assert(ctx, slv, mkNot(toZ3Term(actLit, NULL)));
	// checkError();
}

void Z3SMTEnvironment::push(void) {
	Z3_solver_push(ctx, slv);
	++numBT;
}

void Z3SMTEnvironment::pop(void) {
	assert(Z3_solver_get_num_scopes(ctx, slv) >= 1);
	Z3_solver_pop(ctx, slv, 1);
	// checkError();
	--numBT;
}

TermPtr Z3SMTEnvironment::parsePredicate(const string & str) {
	string toParse = "(assert " + str + ")";
	Z3_ast ret = Z3_parse_smtlib2_string(ctx, toParse.c_str(), 0, 0, 0,
			declaredVars.size(), &varSymbols[0], &varDecls[0]);
	// checkError();
	return fromZ3Term(ret, NULL);
}

string Z3SMTEnvironment::termToString(Z3_ast term) const {
	const char * s = Z3_ast_to_string(ctx, term);
	return string(s);
}

void Z3SMTEnvironment::termToNumber(mpz_t * number, Z3_ast term) const {
	assert(termIsNumeral(term));
	Z3_string str = Z3_get_numeral_string(ctx, term);
	// checkError();
	mpz_set_str(*number, str, 10);
}

Z3_ast Z3SMTEnvironment::mkModCongr(long mod, Z3_ast arg0, Z3_ast arg1) const {
	return mkComparison(ComparisonType::EQ,
			Z3_mk_mod(ctx, mkPlus(arg0, mkMinus(arg1)), mkInt(mod)), mkInt(0));
}

Z3_ast Z3SMTEnvironment::mkTrue(void) const {
	return Z3_mk_true(ctx);
}

Z3_ast Z3SMTEnvironment::mkFalse(void) const {
	return Z3_mk_false(ctx);
}

Z3_ast Z3SMTEnvironment::mkIntVar(string name) const {
	Z3_sort ift = Z3_mk_int_sort(ctx);
	Z3_symbol sy = Z3_mk_string_symbol(ctx, name.c_str());
	Z3_ast ast = Z3_mk_const(ctx, sy, ift);
	return ast;
}

Z3_ast Z3SMTEnvironment::mkBoolVar(string name) const {
	Z3_sort ift = Z3_mk_bool_sort(ctx);
	Z3_symbol sy = Z3_mk_string_symbol(ctx, name.c_str());
	Z3_ast ast = Z3_mk_const(ctx, sy, ift);
	return ast;
}

Z3_ast Z3SMTEnvironment::mkArrayVar(string name) const {
	Z3_sort ift = Z3_mk_int_sort(ctx);
	Z3_sort aft = Z3_mk_array_sort(ctx, ift, ift);
	Z3_symbol sy = Z3_mk_string_symbol(ctx, name.c_str());
	Z3_ast ast = Z3_mk_const(ctx, sy, aft);
	return ast;
}

Z3_ast Z3SMTEnvironment::mkInt(long l) const {
	Z3_sort ift = Z3_mk_int_sort(ctx);
	return Z3_mk_numeral(ctx, to_string(l).c_str(), ift);
}

bool Z3SMTEnvironment::sat(void) {
	++nQuery;
	startTimer();
	Z3_lbool rv = Z3_solver_check(ctx, slv);
	endTimer();
	// checkError();
	assert(rv == Z3_L_TRUE || rv == Z3_L_FALSE);
	return rv == Z3_L_TRUE;
}

bool Z3SMTEnvironment::sat(const TermVec & assumptions) {
	++nQuery;
	vector<Z3_ast> z3Assumptions;
	for (TermVec::const_iterator it = assumptions.begin();
			it != assumptions.end(); ++it) {
		Z3_ast t = toZ3Term(*it, NULL);
		z3Assumptions.push_back(t);
	}
	startTimer();
	Z3_lbool rv = Z3_solver_check_assumptions(ctx, slv, z3Assumptions.size(),
			&z3Assumptions[0]);
	endTimer();
	assert(rv == Z3_L_TRUE || rv == Z3_L_FALSE);
	return rv == Z3_L_TRUE;
}

void Z3SMTEnvironment::getUnsatAssumps(vector<size_t> & core) const {
	size_t coreSize;
	Z3_ast_vector unsatCore = Z3_solver_get_unsat_core(ctx, slv);
	// checkError();
	coreSize = Z3_ast_vector_size(ctx, unsatCore);
	for (size_t i = 0; i < coreSize; ++i) {
		Z3_ast t = Z3_ast_vector_get(ctx, unsatCore, i);
		core.push_back(termId(t));
	}
}

size_t Z3SMTEnvironment::termId(Z3_ast term) const {
	return Z3_get_ast_id(ctx, term);
}

bool Z3SMTEnvironment::termIsBoundVar(Z3_ast term) const {
	return Z3_get_ast_kind(ctx, term) == Z3_VAR_AST;
}

bool Z3SMTEnvironment::termIsKind(Z3_ast term, TermType::E kind) const {
	if (kind == TermType::FORALL) {
		if (Z3_get_ast_kind(ctx, term) == Z3_QUANTIFIER_AST
				&& Z3_is_quantifier_forall(ctx, term)) {
			return true;
		} else {
			return false;
		}
	}
	assert(Z3_get_ast_kind(ctx, term) == Z3_APP_AST);
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	// EQ, GEQ, LEQ, AND, OR, NOT
	switch (kind) {
	case TermType::EQ:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_EQ;
	case TermType::LEQ:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_LE;
	case TermType::AND:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_AND;
	case TermType::OR:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_OR;
	case TermType::NOT:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_NOT;
	case TermType::PLUS:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_ADD;
	case TermType::TIMES:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_MUL;
	case TermType::UNARY_MINUS:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_UMINUS;
	default:
		assert(false);
		return false;
	}
	assert(false);
	return false;
}

bool Z3SMTEnvironment::termIsComparison(Z3_ast term,
		ComparisonType::E kind) const {
	if (Z3_get_ast_kind(ctx, term) != Z3_APP_AST) return false;
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	// EQ, GEQ, LEQ, LT, GT
	switch (kind) {
	case ComparisonType::EQ:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_EQ;
	case ComparisonType::LEQ:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_LE;
	case ComparisonType::GEQ:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_GE;
	case ComparisonType::LT:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_LT;
	case ComparisonType::GT:
		return Z3_get_decl_kind(ctx, decl) == Z3_OP_GT;
	default:
		assert(false);
		return false;
	}
}

bool Z3SMTEnvironment::termIsConstArray(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	Z3_decl_kind declKind = Z3_get_decl_kind(ctx, decl);
	return declKind == Z3_OP_CONST_ARRAY;
}

bool Z3SMTEnvironment::termIsVar(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	Z3_decl_kind declKind = Z3_get_decl_kind(ctx, decl);
	unsigned arity = Z3_get_arity(ctx, decl);
	return declKind == Z3_OP_UNINTERPRETED && arity == 0;
}

bool Z3SMTEnvironment::termIsArrayRead(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	return Z3_get_decl_kind(ctx, Z3_get_app_decl(ctx, Z3_to_app(ctx, term)))
			== Z3_OP_SELECT;
}

bool Z3SMTEnvironment::termIsImplication(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	return Z3_get_decl_kind(ctx, Z3_get_app_decl(ctx, Z3_to_app(ctx, term)))
			== Z3_OP_IMPLIES;
}

bool Z3SMTEnvironment::termIsArrayWrite(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	return Z3_get_decl_kind(ctx, Z3_get_app_decl(ctx, Z3_to_app(ctx, term)))
			== Z3_OP_STORE;
}

bool Z3SMTEnvironment::termIsNumeral(Z3_ast term) const {
	return Z3_is_numeral_ast(ctx, term);
}

bool Z3SMTEnvironment::termIsTrue(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	Z3_decl_kind declKind = Z3_get_decl_kind(ctx, decl);
	return declKind == Z3_OP_TRUE;
}

bool Z3SMTEnvironment::termIsFalse(Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	if (kind != Z3_APP_AST) return false;
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	Z3_decl_kind declKind = Z3_get_decl_kind(ctx, decl);
	return declKind == Z3_OP_FALSE;
}

string Z3SMTEnvironment::varName(Z3_ast term) const {
	assert(Z3_get_ast_kind(ctx, term) == Z3_APP_AST);
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	Z3_symbol sy = Z3_get_decl_name(ctx, decl);
	Z3_string str = Z3_get_symbol_string(ctx, sy);
	return string(str);
}

string Z3SMTEnvironment::numeralToString(Z3_ast term) const {
	assert(termIsNumeral(term));
	return string(Z3_get_numeral_string(ctx, term));
}

Z3_ast Z3SMTEnvironment::termGetArg(Z3_ast term, size_t idx) const {
	assert(Z3_get_ast_kind(ctx, term) == Z3_APP_AST);
	Z3_app app = Z3_to_app(ctx, term);
	assert(Z3_get_app_num_args(ctx, app) > idx);
	return Z3_get_app_arg(ctx, app, idx);
}

Z3_ast Z3SMTEnvironment::mkNot(Z3_ast term) const {
	return Z3_mk_not(ctx, term);
}

Z3_ast Z3SMTEnvironment::mkOr(size_t size, Z3_ast * args) const {
	return Z3_mk_or(ctx, size, args);
}

Z3_ast Z3SMTEnvironment::mkAnd(size_t size, Z3_ast * args) const {
	return Z3_mk_and(ctx, size, args);
}

Z3_ast Z3SMTEnvironment::mkOr(Z3_ast arg1, Z3_ast arg2) const {
	size_t size = 2;
	Z3_ast args[2] = { arg1, arg2 };
	return Z3_mk_or(ctx, size, args);
}

Z3_ast Z3SMTEnvironment::mkAnd(Z3_ast arg1, Z3_ast arg2) const {
	size_t size = 2;
	Z3_ast args[2] = { arg1, arg2 };
	return Z3_mk_and(ctx, size, args);
}

Z3_ast Z3SMTEnvironment::mkComparison(ComparisonType::E kind, Z3_ast arg0,
		Z3_ast arg1) const {
	assert(
			kind == ComparisonType::EQ || kind == ComparisonType::LEQ
					|| kind == ComparisonType::GEQ || kind == ComparisonType::LT
					|| kind == ComparisonType::GT);
	if (kind == ComparisonType::EQ) {
		return Z3_mk_eq(ctx, arg0, arg1);
	}
	if (kind == ComparisonType::LEQ) {
		return Z3_mk_le(ctx, arg0, arg1);
	}
	if (kind == ComparisonType::GEQ) {
		return Z3_mk_ge(ctx, arg0, arg1);
	}
	if (kind == ComparisonType::LT) {
		return Z3_mk_lt(ctx, arg0, arg1);
	}
	if (kind == ComparisonType::GT) {
		return Z3_mk_gt(ctx, arg0, arg1);
	}
	assert(false);
	return Z3_mk_false(ctx);
}

Z3_ast Z3SMTEnvironment::mkArraySelect(Z3_ast var, Z3_ast idx) const {
	return Z3_mk_select(ctx, var, idx);
}

Z3_ast Z3SMTEnvironment::mkArrayStore(Z3_ast var, Z3_ast idx,
		Z3_ast val) const {
	return Z3_mk_store(ctx, var, idx, val);
}

Z3_ast Z3SMTEnvironment::mkBoundVar(const string & name,
		const vector<string> & boundVars) const {
	unsigned idx = 0;
	for (vector<string>::const_reverse_iterator it = boundVars.rbegin();
			it != boundVars.rend(); ++it) {
		if (*it == name) return Z3_mk_bound(ctx, idx, Z3_mk_int_sort(ctx));
		++idx;
	}
	assert(false);
	return Z3_mk_bound(ctx, 0, Z3_mk_int_sort(ctx));
}

Z3_ast Z3SMTEnvironment::mkForAll(const vector<string> & boundVars,
		Z3_ast body) const {
	vector<Z3_sort> sorts;
	vector<Z3_symbol> names;
	for (vector<string>::const_iterator it = boundVars.begin();
			it != boundVars.end(); ++it) {
		sorts.push_back(Z3_mk_int_sort(ctx));
		names.push_back(Z3_mk_string_symbol(ctx, (*it).c_str()));
	}
	return Z3_mk_forall(ctx, 0, 0, NULL, boundVars.size(), &sorts[0], &names[0],
			body);
}

Z3_ast Z3SMTEnvironment::mkMinus(Z3_ast arg) const {
	return Z3_mk_unary_minus(ctx, arg);
}

Z3_ast Z3SMTEnvironment::mkPlus(Z3_ast arg1, Z3_ast arg2) const {
	Z3_ast args[2] = { arg1, arg2 };
	return Z3_mk_add(ctx, 2, args);
}

Z3_ast Z3SMTEnvironment::mkTimes(Z3_ast arg1, Z3_ast arg2) const {
	Z3_ast args[2] = { arg1, arg2 };
	return Z3_mk_mul(ctx, 2, args);
}

bool Z3SMTEnvironment::extractInteger(long & ret, const Z3_model & m,
		TermPtr & sharedTerm, bool full) {
	Z3_ast val;
	Z3_ast term = toZ3Term(sharedTerm, NULL);
	startTimer();
	Z3_bool ok = Z3_model_eval(ctx, m, term, full ? Z3_TRUE : Z3_FALSE, &val);
	endTimer();
	// checkError();
	assert(ok == Z3_TRUE);
	if (termIsNumeral(val)) {
		ret = termToLong(val);
		return true;
	} else {
		assert(!full);
		return false;
	}
}

bool Z3SMTEnvironment::extractBool(bool & ret, const Z3_model & m,
		Z3_func_decl & varDecl, bool full) {
	Z3_ast val;
	Z3_ast term = Z3_mk_app(ctx, varDecl, 0, NULL);
	startTimer();
	Z3_bool ok = Z3_model_eval(ctx, m, term, full ? Z3_TRUE : Z3_FALSE, &val);
	endTimer();
	// checkError();
	assert(ok == Z3_TRUE);
	if (termIsTrue(val)) {
		ret = true;
		return true;
	} else if (termIsFalse(val)) {
		ret = false;
		return true;
	} else {
		assert(!full);
		return false;
	}
}

bool Z3SMTEnvironment::extractArrayElem(long & ret, const Z3_model & m,
		Z3_func_decl & varDecl, long idx, bool full) {
	Z3_ast val;
	Z3_ast term = Z3_mk_select(ctx, Z3_mk_app(ctx, varDecl, 0, NULL),
			Z3_mk_int(ctx, idx, Z3_mk_int_sort(ctx)));
	startTimer();
	Z3_bool ok = Z3_model_eval(ctx, m, term, full ? Z3_TRUE : Z3_FALSE, &val);
	endTimer();
	// checkError();
	assert(ok == Z3_TRUE);
	if (termIsNumeral(val)) {
		ret = termToLong(val);
		return true;
	} else {
		assert(!full);
		return false;
	}
}

void Z3SMTEnvironment::printAssertedTerms(void) const {
	Z3_ast_vector av = Z3_solver_get_assertions(ctx, slv);
	Z3_ast_vector_inc_ref(ctx, av);
	// checkError();
	unsigned s = Z3_ast_vector_size(ctx, av);
	for (unsigned i = 0; i < s; ++i) {
		cout << termToString(Z3_ast_vector_get(ctx, av, i)) << endl;
	}
	Z3_ast_vector_dec_ref(ctx, av);
	// checkError();
}

void Z3SMTEnvironment::getBoolModel(SatModel & mod, set<string> & important) {
	startTimer();
	Z3_model m = Z3_solver_get_model(ctx, slv);
	endTimer();
	Z3_model_inc_ref(ctx, m);
	for (set<string>::iterator it = important.begin(); it != important.end();
			++it) {
		Z3_ast boolVar = mkBoolVar(*it);
		Z3_func_decl decl = Z3_get_app_decl(ctx, Z3_to_app(ctx, boolVar));
		bool b;
		bool ok = extractBool(b, m, decl, true);
		assert(ok);
		Assignment asgn(*it, b);
		mod.insert(asgn);
	}
	assert(mod.size() == important.size());
	Z3_model_dec_ref(ctx, m);
}

void Z3SMTEnvironment::getModel(SatModel & mod, bool full) {
	startTimer();
	Z3_model m = Z3_solver_get_model(ctx, slv);
	endTimer();
	// checkError();
	Z3_model_inc_ref(ctx, m);
	for (map<string, Z3_func_decl>::iterator it = modelVars.begin();
			it != modelVars.end(); ++it) {
		assert(declaredVars.find(it->first) != declaredVars.end());
		switch (declaredVars[it->first]) {
		case VarType::INT: {
			TermPtr intVar = tf.mkIntVar(it->first);
			long i;
			if (boundsToArray.find(it->first) != boundsToArray.end()) continue;
			bool assigned = extractInteger(i, m, intVar, full);
			assert(!full || assigned);
			if (assigned) {
				Assignment asgn(it->first, i);
				mod.insert(asgn);
				if (opt.verbosity >= 4) cout << it->first << "=" << i << endl;
			}
			break;
		}
		case VarType::BOOL: {
			bool b;
			bool assigned = extractBool(b, m, it->second, full);
			assert(!full || assigned);
			if (assigned) {
				Assignment asgn(it->first, b);
				mod.insert(asgn);
				if (opt.verbosity >= 4) cout << it->first << "=" << b << endl;
			}
			break;
		}
		case VarType::DYN_ARRAY: {
			assert(dynArrayBounds.find(it->first) != dynArrayBounds.end());
			TermPtr dynArrayBound = tf.mkIntVar(dynArrayBounds[it->first]);
			long sb;
			bool assigned = extractInteger(sb, m, dynArrayBound, full);
			if (!assigned) continue;
			if (opt.verbosity >= 4)
				cout << dynArrayBounds[it->first] << "=" << sb << endl;
			assert(assigned);
			map<long, long> arrayVals;
			bool skipArray = false;
			for (long i = 0; i < sb; ++i) {
				long arrayElem;
				assigned = extractArrayElem(arrayElem, m, it->second, i, full);
				assert(assigned);
				arrayVals.insert(map<long, long>::value_type(i, arrayElem));
				if (opt.verbosity >= 4)
					cout << it->first << "[" << i << "]=" << arrayElem << endl;
			}

			if (skipArray || sb <= 0) {
				Assignment asgn(dynArrayBounds[it->first], sb);
				mod.insert(asgn);
			} else {
				Assignment asgn(it->first, arrayVals, dynArrayBounds[it->first],
						sb);
				mod.insert(asgn);
			}
			break;
		}
		case VarType::FIXED_ARRAY: {
			assert(fixedArrayBounds.find(it->first) != fixedArrayBounds.end());
			long b = fixedArrayBounds[it->first];
			assert(b > 0);
			map<long, long> arrayVals;
			bool skipArray = false;
			for (long i = 0; i < b; ++i) {
				long arrayElem;
				bool assigned = extractArrayElem(arrayElem, m, it->second, i,
						full);
				if (!assigned) {
					skipArray = true;
					break;
				}
				assert(assigned);
				arrayVals.insert(map<long, long>::value_type(i, arrayElem));
				if (opt.verbosity >= 4)
					cout << it->first << "[" << i << "]=" << arrayElem << endl;
			}
			if (!skipArray) {
				Assignment asgn(it->first, arrayVals);
				mod.insert(asgn);
			}
			break;
		}
		case VarType::INPUT_ARRAY: {
			assert(inputArrayBounds.find(it->first) != inputArrayBounds.end());
			TermPtr inputArrayBound = inputArrayBounds[it->first];
			long sb;
			bool assigned = extractInteger(sb, m, inputArrayBound, true);
			assert(assigned);
			if (opt.verbosity >= 4)
				cout << *inputArrayBound << "=" << sb << endl;
			if (sb <= 0) continue;
			map<long, long> arrayVals;
			for (long i = 0; i < sb; ++i) {
				long arrayElem;
				assigned = extractArrayElem(arrayElem, m, it->second, i, true);
				assert(assigned);
				arrayVals.insert(map<long, long>::value_type(i, arrayElem));
				if (opt.verbosity >= 4)
					cout << it->first << "[" << i << "]=" << arrayElem << endl;
			}
			Assignment asgn(it->first, arrayVals);
			mod.insert(asgn);
			break;
		}
		default:
			assert(false);
		}
	}
	Z3_model_dec_ref(ctx, m);
}

size_t Z3SMTEnvironment::termNumArgs(Z3_ast term) const {
	return Z3_get_app_num_args(ctx, Z3_to_app(ctx, term));
}

void Z3SMTEnvironment::getArgs(vector<Z3_ast> & args, Z3_ast term) const {
	Z3_ast_kind kind = Z3_get_ast_kind(ctx, term);
	assert(kind == Z3_APP_AST);
	Z3_app app = Z3_to_app(ctx, term);
	for (size_t i = 0; i < Z3_get_app_num_args(ctx, app); ++i) {
		args.push_back(Z3_get_app_arg(ctx, app, i));
	}
}

// TODO ast reference counting and/or garbage collection

Z3_ast Z3SMTEnvironment::copyApp(Z3_ast term, size_t numArgs,
		Z3_ast * args) const {
	assert(Z3_get_ast_kind(ctx, term) == Z3_APP_AST);
	Z3_app app = Z3_to_app(ctx, term);
	Z3_func_decl decl = Z3_get_app_decl(ctx, app);
	return Z3_mk_app(ctx, decl, numArgs, args);
}

void Z3SMTEnvironment::declareIntVar(string varName, bool inModel) {
	if (declaredVars.find(varName) == declaredVars.end()) {
		Z3_sort ift = Z3_mk_int_sort(ctx);
		Z3_symbol sy = Z3_mk_string_symbol(ctx, varName.c_str());
		Z3_func_decl de = Z3_mk_func_decl(ctx, sy, 0, 0, ift);
		declaredVars.insert(VarTypeMap::value_type(varName, VarType::INT));
		varSymbols.push_back(sy);
		varDecls.push_back(de);
		if (inModel) {
			modelVars.insert(Z3TypeMap::value_type(varName, de));
		}
	}
}

void Z3SMTEnvironment::declareBoolVar(string varName, bool inModel) {
	if (declaredVars.find(varName) == declaredVars.end()) {
		Z3_sort ift = Z3_mk_bool_sort(ctx);
		Z3_symbol sy = Z3_mk_string_symbol(ctx, varName.c_str());
		Z3_func_decl de = Z3_mk_func_decl(ctx, sy, 0, 0, ift);
		declaredVars.insert(VarTypeMap::value_type(varName, VarType::BOOL));
		varSymbols.push_back(sy);
		varDecls.push_back(de);
		if (inModel) {
			modelVars.insert(Z3TypeMap::value_type(varName, de));
		}
	}
}

void Z3SMTEnvironment::declareInputArray(string varName, bool inModel,
		TermPtr boundFunction) {
	if (declaredVars.find(varName) == declaredVars.end()) {
		Z3_sort ift = Z3_mk_int_sort(ctx);
		Z3_sort aft = Z3_mk_array_sort(ctx, ift, ift);
		Z3_symbol sy = Z3_mk_string_symbol(ctx, varName.c_str());
		Z3_func_decl de = Z3_mk_func_decl(ctx, sy, 0, 0, aft);
		declaredVars.insert(
				VarTypeMap::value_type(varName, VarType::INPUT_ARRAY));
		inputArrayBounds.insert(
				InputSizeMap::value_type(varName, boundFunction));
		varSymbols.push_back(sy);
		varDecls.push_back(de);
		if (inModel) {
			modelVars.insert(Z3TypeMap::value_type(varName, de));
		}
	}
}

void Z3SMTEnvironment::declareDynArray(string varName, bool inModel,
		string boundVar) {
	if (declaredVars.find(varName) == declaredVars.end()) {
		Z3_sort ift = Z3_mk_int_sort(ctx);
		Z3_sort aft = Z3_mk_array_sort(ctx, ift, ift);
		Z3_symbol sy = Z3_mk_string_symbol(ctx, varName.c_str());
		Z3_func_decl de = Z3_mk_func_decl(ctx, sy, 0, 0, aft);
		declaredVars.insert(
				VarTypeMap::value_type(varName, VarType::DYN_ARRAY));
		dynArrayBounds.insert(DynSizeMap::value_type(varName, boundVar));
		boundsToArray.insert(ReverseSizeMap::value_type(boundVar, varName));
		varSymbols.push_back(sy);
		varDecls.push_back(de);
		if (inModel) {
			modelVars.insert(Z3TypeMap::value_type(varName, de));
		}
	}
}

void Z3SMTEnvironment::declareFixedArray(string varName, bool inModel,
		size_t bound) {
	if (declaredVars.find(varName) == declaredVars.end()) {
		Z3_sort ift = Z3_mk_int_sort(ctx);
		Z3_sort aft = Z3_mk_array_sort(ctx, ift, ift);
		Z3_symbol sy = Z3_mk_string_symbol(ctx, varName.c_str());
		Z3_func_decl de = Z3_mk_func_decl(ctx, sy, 0, 0, aft);
		declaredVars.insert(
				VarTypeMap::value_type(varName, VarType::FIXED_ARRAY));
		fixedArrayBounds.insert(
				map<string, size_t>::value_type(varName, bound));
		varSymbols.push_back(sy);
		varDecls.push_back(de);
		if (inModel) {
			modelVars.insert(Z3TypeMap::value_type(varName, de));
		}
	}
}

}
