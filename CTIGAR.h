/*
 * CTIGAR.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef CTIGAR_H_
#define CTIGAR_H_

#include <stddef.h>
#include <set>
#include <sys/times.h>
#include <algorithm>
#include <stack>

#include "Model.h"
#include "State.h"
#include "Z3SMTEnvironment.h"
#include "RSM.h"
#include "FormulaHolder.h"
#include "Frame.h"

using namespace std;

namespace CTIGAR {

// A proof obligation.
struct Obligation {
	Obligation(size_t st, size_t l, size_t d) :
			state(st), level(l), depth(d) {
	}
	size_t state;  // Generalize this state...
	size_t level;  // ... relative to this level.
	size_t depth;  // Length of CTI suffix to error.
};
class ObligationComp {
public:
	bool operator()(const Obligation & o1, const Obligation & o2) {
		if (o1.level < o2.level) return true;  // prefer lower levels (required)
		if (o1.level > o2.level) return false;
		if (o1.depth < o2.depth) return true;  // prefer shallower (heuristic)
		if (o1.depth > o2.depth) return false;
		if (o1.state < o2.state) return true;  // canonical final decider
		return false;
	}
};

typedef set<Obligation, ObligationComp> PriorityQueue;

class CTIGAR {
protected:
	TermFactory tf;
	AbstractDomain & d;
	unsigned k;
	vector<Frame> frames;
	Model & model;
	Z3SMTEnvironment * cons;
	Z3SMTEnvironment * inits;
	Z3SMTEnvironment * lifts;
	clock_t interpolationTime;
	size_t maxJoins, micAttempts, maxDepth, maxCTGs, maxSpurious;
	unsigned verbosity;
	Options & opt;

	TransitionHolder * transitionHolder;
	TRLoad trLoad;
	size_t nLoaded;
	RSM * rsm;
	// GargPredicates * garg;
	StateManager smgr;

	bool trivial;
	size_t earliest;  // track earliest modified level in a major iteration
	size_t cexState;  // beginning of counterexample trace

public:
	CTIGAR(Model & model, AbstractDomain & d, Options & opt);

	virtual ~CTIGAR();

	void countFPClauses(size_t level, int & numClauses,
			size_t & numLitsPerClause);

	virtual bool checkCEXTrace(void) = 0;

	bool checkFP(size_t level);

	// bool checkCEXTrace(void);

	// The main loop.
	bool check();

	// Follows and prints chain of states from cexState forward.
	void printWitness();

protected:

	// Push a new Frame.
	void extend();

	// Strengthens frontier to remove error successors.
	bool strengthen();

	// Propagates clauses forward using induction.  If any frame has
	// all of its clauses propagated forward, then two frames' clause
	// sets agree; hence those clause sets are inductive
	// strengthenings of the property.  See the four invariants of IC3
	// in the original paper.
	size_t propagate();

	// Based on
	//
	//   Zyad Hassan, Aaron R. Bradley, and Fabio Somenzi, "Better
	//   Generalization in IC3," (submitted May 2013)
	//
	// Improves upon "down" from the original paper (and the FMCAD'07
	// paper) by handling CTGs.
	bool ctgDown(size_t level, AbstractState & ast, size_t keepTo,
			size_t recDepth, size_t maxCTGs);

	// Extracts minimal inductive (relative to level) subclause from
	// ~cube --- at least that's where the name comes from.  With
	// ctgDown, it's not quite a MIC anymore, but what's returned is
	// inductive relative to the possibly modifed level.
	void mic(size_t level, AbstractState & ast, size_t recDepth);

	// wrapper to start inductive generalization
	void mic(size_t level, AbstractState & cube);

	// Adds cube to frames at and below level, unless !toAll, in which
	// case only to level.
	void addAbstractCube(size_t level, AbstractState & state);

	// ~cube was found to be inductive relative to level; now see if
	// we can do better.
	size_t abstractGeneralize(size_t level, AbstractState & cube);

	bool refineDomain(TermPtr toRefineWith, bool ineqsOnly);

	typedef pair<size_t, size_t> StatePair;
	typedef stack<StatePair> GuidedTrace;

	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls) = 0;

	// Returns true if the cube does not intersect with the initial condition.
	bool initiation(const AsgnVec & latches);

	// Returns true if the abstract state does not intersect with the initial condition
	// first check if -3 is in ast
	bool initiation(const AbstractState & ast, bool optimize = true,
			bool print = false);

	typedef map<size_t, size_t> TermIdMap;

	void rebuildConsecution(void);

	// checks if F_fi & abstract(~s) & t => abstract(s)'
	// succ: pred.successor = succ
	bool abstractConsecution(size_t fi, const AbstractState & ast, size_t succ =
			0, AbstractState * core = NULL, size_t * pred = NULL);

	// checks if F_fi & ~s & T => s'
	// won't return spurious results
	bool consecution(size_t fi, const AsgnVec & latches, size_t succ = 0,
			size_t * pred = NULL);

	// Structure and methods for imposing priorities on literals
	// through ordering the dropping of literals in mic (drop leftmost
	// literal first) and assumptions to Minisat. The implemented
	// ordering prefers to keep literals that appear frequently in
	// addCube() calls.
	struct HeuristicLitOrder {
		HeuristicLitOrder() :
				_mini(1 << 20) {
		}
		vector<float> counts;
		size_t _mini;
		void count(const AbstractState & cube) {
			assert(!cube.empty());
			AbstractState posCube;
			for (AbstractState::const_iterator it = cube.begin();
					it != cube.end(); ++it) {
				posCube.push_back(abs(*it) - 1);
			}
			sort(posCube.begin(), posCube.end());
			// assumes cube is ordered
			size_t sz = (size_t) posCube.back();
			if (sz >= counts.size()) counts.resize(sz + 1);
			_mini = (size_t) posCube[0];
			for (AbstractState::const_iterator i = posCube.begin();
					i != posCube.end(); ++i)
				counts[(size_t) *i] += 1;
		}
		void decay() {
			for (size_t i = _mini; i < counts.size(); ++i)
				counts[i] *= 0.99f;
		}
		bool operator()(const long & l1, const long & l2) const {
			//if (l1 < 0 && l2 > 0)
			//	return true;
			//if (l2 < 0 && l1 > 0)
			//	return false;
			// l1, l2 must be unprimed
			size_t i2 = (size_t) (abs(l2) - 1);
			if (i2 >= counts.size()) return false;
			size_t i1 = (size_t) (abs(l1) - 1);
			if (i1 >= counts.size()) return true;
			return (counts[i1] < counts[i2]);
		}
	} litOrder;

	float numLits, numUpdates;

	void updateLitOrder(const AbstractState & cube);

	// order according to preference
	void orderCube(AbstractState & cube);

	void extractLiftingCore(AbstractState & ret, bool sat,
			map<size_t, long> & termIdToLit, const AbstractState & toLift);

	void loadLiftingPrelims(const AbstractState & toLift,
			const AsgnVec & inputs, map<size_t, long> & termIdToLit);

	// returned ast always satisfies initiation
	bool lift(AbstractState & ret, const AbstractState & toLift,
			const AsgnVec & inputs, const AbstractState * successor);

	// returned ast always satisfies initiation
	bool lift(AbstractState & ret, const AbstractState & toLift,
			const AsgnVec & inputs, const AsgnVec & successor);

	size_t spuriousStateOf(SMTEnvironment * env, size_t succIdx);

	// Assumes that last call to fr.consecution->solve() was
	// satisfiable.  Extracts smgr.state(s) cube from satisfying
	// assignment.
	size_t stateOf(size_t succ, bool lift);

	virtual bool predecessor(const Obligation & obl, PriorityQueue & obls,
			size_t pred);

	void spuriousPredecessor(const Obligation & obl, PriorityQueue & obls,
			size_t pred);

	void generalizeErase(const Obligation & obl,
			const PriorityQueue::const_iterator & obli, PriorityQueue & obls,
			AbstractState & core);

	// Backtracks to the latest known state with a spurious predecessor and refines
	// the domain based on this state, deleting all the obligations up to this state
	// on the way
	virtual void backtrackRefine(const Obligation & obl, PriorityQueue & obls,
			bool enforceRefinement = true) = 0;

	int nCTI;
	int nErased;
	int nUnliftedErased;
	int nCTG;
	int nmic;
	int nCoreReduced;
	int nAbortJoin;
	int nAbortMic;
	int nRefinements;
	size_t maxTraceDepth;
	int fixedPointNumClauses;
	size_t numLiteralsPerClause;
	int nAbstractLiftingTries;
	int nAbstractLiftingSuccessful;
	size_t nLoadedSlices;
	int nConsSolvers;

	clock_t time();

	clock_t satTime();

	size_t nQuery();

	void printStats();

	friend bool check(Model & model, Options & opt);

	friend void sigHandler(int signo);

}
;

bool check(Model & model, Options & opt);

}

#endif /* CTIGAR_H_ */
