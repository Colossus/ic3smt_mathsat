/*
 * ConsecutionRefinement.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#include "ConsRef.h"

using namespace std;

namespace CTIGAR {

ConsRef::ConsRef(Model & model1, AbstractDomain & d1, Options & opt1) :
		CTIGAR(model1, d1, opt1) {
}

ConsRef::~ConsRef() {
}

void ConsRef::elimSpuriousTrans(size_t st, size_t level,
		bool enforceRefinement) {
	++nRefinements;
	TermVec lines;
	rsm->analyzeTrace(lines, st);
	TermVec blah;
	for (TermVec::const_iterator it = lines.begin(); it != lines.end(); ++it) {
		refineDomain(*it, true);
	}
	TermPtr i;
	bool intpOK = interpolate(i, level, smgr.state(st).lifted);
	bool ref = false;
	if (intpOK) {
		ref = refineDomain(i, true);
	} else {
		bool r1 = false;
		for (AsgnVec::const_iterator it = smgr.state(st).lifted.begin();
				it != smgr.state(st).lifted.end(); ++it) {
			r1 = refineDomain(tf.asgnToTerm(*it), true);
			ref = ref || r1;
		}
	}
	assert(~enforceRefinement || ref);
}

void ConsRef::removeObligations(size_t st, size_t depth, PriorityQueue & obls) {
	for (size_t level = 0; level <= k; ++level) {
		Obligation o(st, level, depth);
		PriorityQueue::iterator obli = obls.find(o);
		if (obli != obls.end()) {
			obls.erase(obli);
			if (verbosity >= 2)
				cout << "Deleting obligation for state at level " << level
						<< ", depth " << depth << ": " << smgr.state(st)
						<< endl;
		}
	}
}

// Backtracks to the latest known state with a spurious predecessor and refines
// the domain based on this state, deleting all the obligations up to this state
// on the way
void ConsRef::backtrackRefine(const Obligation & obl, PriorityQueue & obls,
		bool enforceRefinement) {
	size_t st = obl.state;
	size_t depth = obl.depth;

	while (!(smgr.state(st).spuriousSucc && smgr.state(st).nSpurious == 1)) {
		removeObligations(st, depth, obls);
		--depth;
		st = smgr.state(st).successor;
		assert(st != 0);
	}
	removeObligations(st, depth, obls);
	size_t level = smgr.state(st).spuriousLevel;
	st = smgr.state(st).successor;
	assert(st != 0);

	if (verbosity >= 2)
		cout << "Spurious state refinement " << smgr.state(st) << " at level "
				<< level << ", depth " << depth << endl;

	// assert(consecution(level, smgr.state(st).lifted));
	if (!enforceRefinement) {
		updateAbstraction(st);
		if (!abstractConsecution(level, smgr.state(st).bestAbstraction())) {
			elimSpuriousTrans(st, level);
		}
	} else {
		elimSpuriousTrans(st, level);
	}
	// obls.clear();
}

bool ConsRef::interpolate(TermPtr & ret, size_t j, const AsgnVec & s) {
	MSatSMTEnvironment * env = new MSatSMTEnvironment(
			"interpolation environment", model.vars.all, opt, false, true,
			true);
	int anteGroup = env->createItpGroup();
	int consGroup = env->createItpGroup();

	env->setItpGroup(anteGroup);
	try {
		if (j == 0) {
			model.loadInitialCondition(env);
		}
		model.loadProperty(env);
		transitionHolder->loadTransitionRelation(env);
	} catch (UnsupportedTermTypeException &) {
		delete env;
		return false;
	}

	if (j > 0) {
		for (size_t i = j; i <= (k + 1); ++i) {
			for (CubeSet::iterator it = frames[i].borderCubes.begin();
					it != frames[i].borderCubes.end(); ++it) {
				TermVec terms;
				d.toTermVec(terms, *it);
				TermPtr t = tf.negClause(terms);
				env->assertFormula(t);
			}
		}
	}

	TermPtr negS = tf.negClause(s);
	env->assertFormula(negS);
	env->setItpGroup(consGroup);
	for (AsgnVec::const_iterator i = s.begin(); i != s.end(); ++i) {
		env->assertFormula(model.primeTerm(tf.asgnToTerm(*i)));
	}

	bool sat = env->sat();
	assert(!sat);
	TermPtr i;
	bool intpOK = env->interpolate(i, anteGroup);
	interpolationTime += env->satTime();
	delete env;
	if (intpOK) {
		TermPtr unprimed = model.unprimeTerm(i);
		ret = tf.deepNegate(unprimed);
		return true;
	} else {
		return false;
	}
}

}
