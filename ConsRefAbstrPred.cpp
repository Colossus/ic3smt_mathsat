/*
 * ConsecutionRefinement.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

using namespace std;

#include "ConsRefAbstrPred.h"

#include "SMTEnvironment.h"
#include "Z3SMTEnvironment.h"
#include "main.h"
#include "Assignment.h"
#include "Model.h"

namespace CTIGAR {

ConsRefAbstrPred::ConsRefAbstrPred(Model & model1, AbstractDomain & d1,
		Options & opt1) :
		ConsRef(model1, d1, opt1) {
}

ConsRefAbstrPred::~ConsRefAbstrPred() {
}

bool ConsRefAbstrPred::checkCEXTrace(void) {
	if (verbosity >= 1) cout << "Checking CEX trace" << endl;
	SMTEnvironment * checker = new Z3SMTEnvironment("checker", model.vars.all,
			opt, model.vars.dynArraySize, model.vars.fixedArraySize,
			model.vars.inputArraySize);
	checker->push();
	model.loadInitialCondition(checker);
	State & st = smgr.state(cexState);
	if (st.hasLiftedAst()) {
		TermVec astVec;
		d.toTermVec(astVec, st.liftedAst);
		for (TermVec::const_iterator it = astVec.begin(); it != astVec.end();
				++it) {
			checker->assertFormula(*it);
		}
	} else {
		for (AsgnVec::const_iterator it = st.lifted.begin();
				it != st.lifted.end(); ++it) {
			checker->assertFormula(tf.asgnToTerm(*it));
		}
	}
	if (!checker->sat()) {
		if (verbosity >= 1)
			cerr
					<< "First state in CEX trace does not satisfy initial condition"
					<< endl;
		delete checker;
		return false;
	}
	checker->pop();

	bool rv = true;
	checker->push();
	transitionHolder->loadTransitionRelation(checker);
	State & st1 = smgr.state(cexState);
	while (st1.successor != 0) {
		checker->push();
		State & st2 = smgr.state(st1.successor);
		if (st1.hasLiftedAst()) {
			if (!st2.hasLiftedAst()) {
				if (verbosity >= 1)
					cerr << "State has lifted AST but successor doesn't"
							<< endl;
				rv = false;
			}

			// st1
			TermVec tv1;
			d.toTermVec(tv1, st1.liftedAst);
			for (TermVec::const_iterator it = tv1.begin(); it != tv1.end();
					++it) {
				checker->assertFormula(*it);
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermVec tv2;
			d.toTermVec(tv2, st2.liftedAst);
			TermPtr negTv2 = tf.negClause(tv2);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
		} else if (st2.hasLiftedAst()) {
			// st1
			for (AsgnVec::const_iterator it = st1.lifted.begin();
					it != st1.lifted.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermVec tv2;
			d.toTermVec(tv2, st2.liftedAst);
			TermPtr negTv2 = tf.negClause(tv2);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
		} else {
			// st1
			for (AsgnVec::const_iterator it = st1.lifted.begin();
					it != st1.lifted.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// inputs
			for (AsgnVec::const_iterator it = st1.inputs.begin();
					it != st1.inputs.end(); ++it) {
				checker->assertFormula(tf.asgnToTerm(*it));
			}

			// st2
			TermPtr negTv2 = tf.negClause(st2.lifted);
			TermPtr negTv2_p = model.primeTerm(negTv2);
			checker->assertFormula(negTv2_p);
		}
		if (checker->sat()) {
			if (verbosity >= 1)
				cerr << "State " << st1 << " does not lead into " << st2
						<< endl;
			rv = false;
		}
		checker->pop();
		st1 = st2;
	}
	checker->pop();
	if (rv == false) {
		delete checker;
		return false;
	}

	checker->push();
	model.loadPrimedError(checker);
	transitionHolder->loadTransitionRelation(checker);
	if (st1.hasLiftedAst()) {
		TermVec tv1;
		d.toTermVec(tv1, st1.liftedAst);
		for (TermVec::const_iterator it = tv1.begin(); it != tv1.end(); ++it) {
			checker->assertFormula(*it);
		}
	} else {
		for (AsgnVec::const_iterator it = st1.lifted.begin();
				it != st1.lifted.end(); ++it) {
			checker->assertFormula(tf.asgnToTerm(*it));
		}
	}
	if (!checker->sat()) {
		if (verbosity >= 1)
			cerr << "Last state does not lead into error" << endl;
		delete checker;
		return false;
	}
	checker->pop();

	delete checker;
	if (verbosity >= 1) cout << "CEX trace OK" << endl;
	return true;
}

// Process obligations according to priority.
bool ConsRefAbstrPred::handleObligations(PriorityQueue obls) {
	while (!obls.empty()) {
		PriorityQueue::iterator obli = obls.begin();
		Obligation obl = *obli;
		assert(obl.level <= k);

		if (obl.level == 0 && !initiation(smgr.state(obl.state).lifted)) {
			if (smgr.state(obl.state).nSpurious == 0) {
				cexState = obl.state;
				return false;
			}
			backtrackRefine(obl, obls);
			continue;
		}

		// Is the obligation fulfilled?
		updateAbstraction(obl.state);
		if (!initiation(smgr.state(obl.state).bestAbstraction())) {
			cout << "Initiation failed on ast" << endl;
			initiation(smgr.state(obl.state).bestAbstraction(), true, true);
			assert(false && "initiation failed on ast");
		}

		if (verbosity >= 1)
			cout << "Trying to prove abstract consecution at level "
					<< obl.level << " with state " << smgr.state(obl.state)
					<< endl;

		size_t abstractPred;
		size_t concretePred;
		AbstractState core;
		assert(obl.state != 0);
		// first, try consecution with the lifted abstraction
		// that may admit more spurious transitions than the most precise abstraction
		if (abstractConsecution(obl.level,
				smgr.state(obl.state).bestAbstraction(), obl.state, &core,
				&abstractPred)) {
			generalizeErase(obl, obli, obls, core);
		} else if (smgr.state(obl.state).hasLiftedAst()) {
			if (!predecessor(obl, obls, abstractPred)) return false;
		} else if (consecution(obl.level, smgr.state(obl.state).lifted,
				obl.state, &concretePred)) {
			// If the concrete state satisfies consecution, try abstract consecution
			// with the most precise abstraction
			if (smgr.state(obl.state).nSpurious >= maxSpurious || obl.level == 0
					|| !initiation(smgr.state(abstractPred).lifted)) {
				// If the most precise abstraction admits spurious transitions,
				// refine the domain to eliminate those
				elimSpuriousTrans(obl.state, obl.level);
			} else {
				spuriousPredecessor(obl, obls, abstractPred);
			}
		} else {
			if (!predecessor(obl, obls, concretePred)) return false;
		}
	}
	return true;
}

// returns true if state or some successor was updated, and false otherwise
bool ConsRefAbstrPred::updateAbstraction(size_t st) {
	if (st == 0) return false;
	size_t succ = smgr.state(st).successor;
	bool succUpdated = updateAbstraction(succ);
	if (!succUpdated && !smgr.state(st).outdated()) return false;
	AbstractState full;
	d.abstract(full, smgr.state(st).lifted);
	AbstractState lifted;
	if (succ == 0) {
		lift(lifted, full, smgr.state(st).inputs, NULL);
	} else if (smgr.state(succ).hasLiftedAst()) {
		lift(lifted, full, smgr.state(st).inputs, &smgr.state(succ).liftedAst);
	} // else don't lift at all
	smgr.state(st).setAbstraction(full, lifted, &d);
	return true;
}

}
