#include <getopt.h>

#include "CTIGAR.h"

using namespace std;

void usage(string progname) {
	cerr
			<< "usage: " + progname
					+ "\n\t[-d:|--max-depth:] (0..)"
							"\n\t[-c:|--max-ctgs:] (0..)"
							"\n\t[-g:|--max-geo:] (0..)"
							"\n\t[-r:|--rsm-points:] (0..)"
							"\n\t[-R:|--rsm-max-points:] (0..)"
							"\n\t[-l:|--max-spurious:] (0..)"
							"\n\t[-f:|--refinement-strategy:] (cons-concr-pred|lift-caf|lift-laf|cons-abstr-pred)"
							"\n\t[-v:|--verbosity:] (0,1,2,3,4)"
							"\n\t[--rebuild-intercept] (1..)"
							"\n\t[--rebuild-var-slope] (0..)"
							"\n\t[--mic-attempts] (0..)"
							"\n\t[--pc-slicing] (no argument)"
							"\n\t[--karr] (on|off)"
							"\n\t[--z3-random] (0..) ... Z3 random seed. If none is given, this will be initialized automatically"
							"\n\t[--abstractor] (mathsat|z3) ... MathSAT is faster but freezes sometimes. Z3 is required for array programs."
							"\n\tpath/to/prog.c"
							"\n\tPrepare program with TRGen2." << endl;
}

int main(int argc, char ** argv) {
	string progname = argv[0];
	if (argc < 2) {
		usage(progname);
		return -1;
	}

	srand(time(0));

	CTIGAR::Options opt;

	bool haveSeed = false;

	int c;
	long arg;
	while (1) {
		int option_index = 0;
		static struct option long_options[] = { { "max-depth",
		required_argument, 0, 'd' }, { "max-ctgs", required_argument, 0, 'c' },
				{ "rsm-points",
				required_argument, 0, 'r' }, { "rsm-max-points",
				required_argument, 0, 'R' }, { "max-spurious",
				required_argument, 0, 'l' }, { "refinement-strategy",
				required_argument, 0, 'f' }, { "verbosity", required_argument,
						0, 'v' },
				{ "rebuild-intercept", required_argument, 0, 0 }, {
						"rebuild-var-slope", required_argument, 0, 0 }, {
						"mic-attempts", required_argument, 0, 0 }, {
						"pc-slicing",
						no_argument, 0, 0 },
				{ "karr", required_argument, 0, 0 }, { "abstractor",
				required_argument, 0, 0 }, { "z3-random", required_argument, 0,
						0 }, {
				NULL, 0, 0, 0 }, };

		c = getopt_long(argc, argv, "d:c:g:r:R:l:f:v:", long_options,
				&option_index);
		if (c == -1) break;

		switch (c) {
		case 0:
			if (string(long_options[option_index].name)
					== "rebuild-intercept") {
				errno = 0;
				arg = strtol(optarg, NULL, 10);
				if (errno != 0 || arg <= 0) {
					usage(progname);
					return -1;
				}
				opt.rebuildIntercept = (size_t) arg;
				break;
			} else if (string(long_options[option_index].name)
					== "rebuild-var-slope") {
				errno = 0;
				arg = strtol(optarg, NULL, 10);
				if (errno != 0 || arg < 0) {
					usage(progname);
					return -1;
				}
				opt.rebuildVarSlope = (size_t) arg;
				break;
			} else if (string(long_options[option_index].name)
					== "mic-attempts") {
				errno = 0;
				arg = strtol(optarg, NULL, 10);
				if (errno != 0 || arg < 0) {
					usage(progname);
					return -1;
				}
				opt.micAttempts = (size_t) arg;
				break;
			} else if (string(long_options[option_index].name)
					== "pc-slicing") {
				opt.pcSlicing = true;
				break;
			} else if (string(long_options[option_index].name) == "karr") {
				if (string(optarg) == "off") {
					opt.karr = false;
				} else {
					opt.karr = true;
				}
				break;
			} else if (string(long_options[option_index].name)
					== "abstractor") {
				if (string(optarg) == "mathsat") {
					opt.abstractor = "mathsat";
				} else if (string(optarg) == "z3") {
					opt.abstractor = "z3";
				} else {
					usage(progname);
					return -1;
				}
				break;
			} else if (string(long_options[option_index].name) == "z3-random") {
				errno = 0;
				unsigned int rseed = (unsigned int) strtol(optarg, NULL, 10);
				if (errno != 0 || arg < 0) {
					usage(progname);
					return -1;
				}
				opt.z3Random = rseed;
				haveSeed = true;
				break;
			} else {
				usage(progname);
				return -1;
			}
		case 'd':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.maxDepth = arg;
			break;
		case 'c':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.maxCTGs = arg;
			break;
		case 'r':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.halfPlaneMinPoints = arg;
			break;
		case 'R':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.halfPlaneMaxPoints = arg;
			break;
		case 'l':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.maxSpurious = arg;
			break;
		case 'f':
			opt.refinementStrategy = string(optarg);
			break;
		case 'v':
			errno = 0;
			arg = strtol(optarg, NULL, 10);
			if (errno != 0 || arg < 0) {
				usage(progname);
				return -1;
			}
			opt.verbosity = (unsigned) arg;
			break;
		case '?':
		default:
			usage(progname);
			return -1;
		}
	}

	if (!haveSeed) {
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		opt.z3Random = (unsigned int) ts.tv_nsec;
	}

	string prog;

	if (optind == argc - 1) {
		prog = argv[optind];
	} else {
		usage(progname);
		return -1;
	}

	if (opt.halfPlaneMinPoints > 0 && opt.halfPlaneMaxPoints == 0) {
		opt.halfPlaneMaxPoints = opt.halfPlaneMinPoints * 2;
	}

	cout << "maxDepth:                 " << opt.maxDepth << endl;
	cout << "maxCTGs:                  " << opt.maxCTGs << endl;
	cout << "halfPlaneMinPoints:       " << opt.halfPlaneMinPoints << endl;
	cout << "halfPlaneMaxPoints:       " << opt.halfPlaneMaxPoints << endl;
	cout << "maxSpurious:              " << opt.maxSpurious << endl;
	cout << "Refinement Strategy:      " << opt.refinementStrategy << endl;
	cout << "Verbosity:                " << opt.verbosity << endl;
	cout << "Cons. Rebuild Intercept:  " << opt.rebuildIntercept << endl;
	cout << "Cons. Rebuild Var. Slope: " << opt.rebuildVarSlope << endl;
	cout << "MIC Attempts:             " << opt.micAttempts << endl;
	cout << "PC Slicing:               " << opt.pcSlicing << endl;
	cout << "Karr:                     " << opt.karr << endl;
	cout << "Abstractor:               " << opt.abstractor << endl;
	cout << "Z3 random seed:           " << opt.z3Random << endl;

	CTIGAR::Model model(prog, opt);

	bool rv = CTIGAR::check(model, opt);
	cout << "Property holds: " << rv << endl;
	return 0;
}
