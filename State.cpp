/*
 * State.cpp
 *
 *  Created on: Jan 8, 2014
 *      Author: Johannes Birgmeier
 */

#include "State.h"

using namespace std;

namespace CTIGAR {

State::State() :
		domainVersion(0), domain(NULL), successor(0), index(0), used(false), spuriousSucc(
				false), spuriousLevel(0), nSpurious(0) {
}

void State::clear() {
	lifted.clear();
	liftedAst.clear();
	fullAst.clear();
	domainVersion = 0;
	domain = NULL;

	successor = 0;
	full.clear();
	used = false;
	inputs.clear();

	spuriousSucc = false;
	spuriousLevel = 0;
	nSpurious = 0;
}

void State::setAbstraction(AbstractState & full, AbstractState & lifted,
		AbstractDomain * domain) {
	assert(full.size() > 0);
	domainVersion = domain->size();
	this->domain = domain;
	this->fullAst = full;
	this->liftedAst = lifted;
}

bool State::outdated(void) {
	if (domain == NULL) return true;
	assert(domainVersion <= domain->size());
	if (domainVersion == domain->size()) return false;
	return true;
}

AbstractState & State::bestAbstraction() {
	assert(fullAst.size() != 0);
	if (liftedAst.size() == 0) return fullAst;
	return liftedAst;
}

bool State::hasLiftedAst() {
	return (liftedAst.size() != 0);
}

bool State::liftedHasArray() {
	for (AsgnVec::const_iterator it = lifted.begin(); it != lifted.end();
			++it) {
		if (it->getType() == AsgnType::ARRAY_ASGN
				|| it->getType() == AsgnType::DYN_ARRAY_ASGN) return true;
	}
	return false;
}

size_t State::getPcVal() {
	for (AsgnVec::const_iterator it = full.begin(); it != full.end(); ++it) {
		if (it->getVar() == ".s") {
			long l = it->getNumVal();
			assert(l >= 0);
			return (size_t) l;
		}
	}
	assert(false);
	return -1;
}

// WARNING: do not keep reference across newState() calls
State & StateManager::state(size_t sti) {
	assert((sti - 1) < states.size());
	return states[sti - 1];
}

size_t StateManager::newState() {
	if (nextState >= states.size()) {
		states.resize(states.size() + 1);
		states.back().index = states.size();
		states.back().used = false;
	}
	size_t ns = nextState;
	assert(!states[ns].used);
	states[ns].used = true;
	while (nextState < states.size() && states[nextState].used)
		nextState++;
	return ns + 1;
}

void StateManager::delState(size_t sti) {
	State & st = state(sti);
	st.clear();
	if (nextState > st.index - 1) nextState = st.index - 1;
}

void StateManager::resetStates() {
	for (vector<State>::iterator i = states.begin(); i != states.end(); ++i) {
		i->clear();
	}
	nextState = 0;
}

std::ostream & operator<<(std::ostream & stream, const State & asgn) {
	stream << "idx " << asgn.index << ", succ " << asgn.successor << ": ";
	for (AsgnVec::const_iterator it = asgn.full.begin(); it != asgn.full.end();
			++it) {
		stream << *it << ", ";
	}
	return stream;
}

}
