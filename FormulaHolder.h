/*
 * FormulaHolder.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef FORMULA_HOLDER_H_
#define FORMULA_HOLDER_H_

#include <vector>
#include <map>

#include "Model.h"
#include "AbstractDomain.h"

using namespace std;

namespace CTIGAR {

typedef map<string, TermVec> TermConjunctMap;
typedef map<long, TermConjunctMap> CFGTermMap;
typedef map<long, map<string, bool>> LoadedMap;

class TRLoad;

class PCRestriction {
	friend class TransitionHolder;

private:

	bool allStates;
	set<long> pcRestrictions;
	set<long> origRestrictions;
	Model & model;
	Options & opt;
	TermFactory tf;

public:

	PCRestriction(Model & model1, Options & opt1) :
			allStates(false), model(model1), opt(opt1) {
	}

	TermPtr createPcRestriction();

	const set<long> & getOrigRestrs(void) {
		assert(opt.pcSlicing);
		return origRestrictions;
	}

};

class TRLoad {

private:

	LoadedMap loadedMap;

public:

	void reset(long maxPC, const VarTypeMap & variables);

	void load(long pc, string var);

	bool loaded(long pc, string var);

};

class TransitionHolder {

	SMTEnvironment * trEnv;
	Model & model;
	CFGTermMap cfgTermMap;
	size_t nSlices;
	AbstractDomain * d;
	Options & opt;
	TermFactory tf;
	TermVec transitionRelation;

public:
	TransitionHolder(Model & model, AbstractDomain * d, Options & opt);

	virtual ~TransitionHolder();

	void loadTransitionRelation(SMTEnvironment * env);

	size_t loadSlice(SMTEnvironment * env, PCRestriction & restr,
			TRLoad & load, const AbstractState & ast);

	size_t loadSlice(SMTEnvironment * env, PCRestriction & restr,
			TRLoad & load, const AsgnVec & st);

	size_t loadErrorSlice(SMTEnvironment * env, PCRestriction & restr,
			TRLoad & load);

	size_t numSlices(void) {
		return nSlices;
	}

	const TermVec & getTransitionRelation(void);

protected:

	void possiblePcVals(long & min, long & max, set<long> & origRestrictions,
			const AbstractState & ast) const;

	void possiblePcVals(long & min, long & max, const AsgnVec & ast) const;

	size_t loadSlices(SMTEnvironment * env, TRLoad & load,
			const set<long> & preds, const set<string> & vars);

}
;

}

#endif /* FORMULA_HOLDER_H_ */
