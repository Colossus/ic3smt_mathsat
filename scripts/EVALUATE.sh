./MAKE_CSV.sh
./MAKE_TABLES.sh
cd CSV
Rscript Analysis.R
cd ..
d=`date +"%Y-%m-%d"`
./MAKE_REPORT.sh > REPORT/report-$d.tex
cd REPORT
latex report-$d.tex && pdflatex report-$d.tex
acroread report-$d.pdf
