#! /usr/bin/zsh

control_c()
# run if user hits control-c
{
  killall CTIGAR-$executable
  exit $?
}
 
# trap keyboard interrupt (control-c)
trap control_c SIGINT

if [ CTIGAR -ot commit.txt ]
then
  echo "Warning: CTIGAR is older than commit.txt! You should probably recompile first!"
fi

function dobm () {
  echo -n "echo $i cons-concr-pred 1/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 0 -f cons-concr-pred $i > out/"$i"_cons-concr-pred_+Ctg-Geo+RSM.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i cons-concr-pred 1/2 finished" >> /tmp/commands_ctigar_$executable
  echo -n "echo $i cons-concr-pred 2/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 3 -f cons-concr-pred $i > out/"$i"_cons-concr-pred_+Ctg-Geo+RSM+Lazy.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i cons-concr-pred 2/2 finished" >> /tmp/commands_ctigar_$executable

  echo -n "echo $i cons-abstr-pred 1/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 0 -f cons-abstr-pred $i > out/"$i"_cons-abstr-pred_+Ctg-Geo+RSM.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i cons-abstr-pred 1/2 finished" >> /tmp/commands_ctigar_$executable
  echo -n "echo $i cons-abstr-pred 2/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 3 -f cons-abstr-pred $i > out/"$i"_cons-abstr-pred_+Ctg-Geo+RSM+Lazy.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i cons-abstr-pred 2/2 finished" >> /tmp/commands_ctigar_$executable

  echo -n "echo $i lift-laf 1/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 0 -f lift-laf $i > out/"$i"_lift-laf_+Ctg-Geo+RSM.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i lift-laf 1/2 finished" >> /tmp/commands_ctigar_$executable
  echo -n "echo $i lift-laf 2/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 3 -f lift-laf $i > out/"$i"_lift-laf_+Ctg-Geo+RSM+Lazy.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i lift-laf 2/2 finished" >> /tmp/commands_ctigar_$executable

  echo -n "echo $i lift-caf 1/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 0 -f lift-caf $i > out/"$i"_lift-caf_+Ctg-Geo+RSM.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i lift-caf 1/2 finished" >> /tmp/commands_ctigar_$executable
  echo -n "echo $i lift-caf 2/2 ; " >> /tmp/commands_ctigar_$executable
  echo -n "nohup timeout -s INT -k 5 1200 ./CTIGAR-$executable --abstractor mathsat -v 0 -d 1 -c 3 -r 3 -l 3 -f lift-caf $i > out/"$i"_lift-caf_+Ctg-Geo+RSM+Lazy.txt" >> /tmp/commands_ctigar_$executable
  echo " ; echo $i lift-caf 2/2 finished" >> /tmp/commands_ctigar_$executable
}

executable=`cat commit.txt`

rm -f /tmp/commands_ctigar_$executable
rm -f ./CTIGAR-$executable
cp ./CTIGAR ./CTIGAR-$executable
strip ./CTIGAR-$executable

if [ $# -ge 1 ]
then
  for i in $*
  do
    dobm
  done
else
  for i in `ls benchmarks/*.c`
  do
    dobm
  done
fi

cat /tmp/commands_ctigar_$executable | xargs -n 1 -d '\n' -P 8 sh -c

cd tailout
mkdir $executable
if [ $? = 0 ]
then
  cd $executable
  for i in `ls ../../out/benchmarks`
  do              
    tail -n 100 ../../out/benchmarks/$i > $i
  done
fi
