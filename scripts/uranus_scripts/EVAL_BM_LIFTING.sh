#! /usr/bin/zsh

function printt () {
  echo -n " & "
  if [ -f ./out/benchmarks/"$fn"_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep "$sstring"`
    solved="true"
    if [ $sstring = "Elapsed total\\:" ]
    then
      tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep -q "Property holds"
      if [ $? -ne 0 ]
      then
        solved="false"
      fi
    fi
    if [ "$el" != "" ] && [ "$solved" = "true" ]
    then
      t=`echo -n $el | sed "s/$sstring*//g"`
      printf "%-10s" "$t"
    else
      printf "%-10s" " "
    fi
  else
    printf "%-10s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

# echo '\\begin{landscape}'
echo '\\begin{center}'
printf '\\begin{longtable}{l||l|l|l|l|l|l|l|l}\n'
printf "%-25s" "Benchmark"
printf " & "
printf "%-10s" "N"
printf " & "
printf "%-10s" "G"
printf " & "
printf "%-10s" "L"
printf " & "
printf "%-10s" "GL"
echo ' \\\\'
printf '\\hline \n'
printf '\\endhead \n'

s1=0
s2=0
s3=0
s4=0
t1=0
t2=0
t3=0
t4=0

incs1() {
  if [ $solved = "true" ]
  then
    ((s1 = s1 + 1))
    ((t1 = t1 + t))
  fi
}

incs2() {
  if [ $solved = "true" ]
  then
    ((s2 = s2 + 1))
    ((t2 = t2 + t))
  fi
}

incs3() {
  if [ $solved = "true" ]
  then
    ((s3 = s3 + 1))
    ((t3 = t3 + t))
  fi
}

incs4() {
  if [ $solved = "true" ]
  then
    ((s4 = s4 + 1))
    ((t4 = t4 + t))
  fi
}

for i in `ls benchmarks/*.c`
do
  fn=`echo $i | sed 's/benchmarks\///g'`
  toprint=`echo $fn | sed 's/_/\\\\_/g'`
  if [ "$toprint" = "sendmail-mime7to8\\_arr\\_three\\_chars\\_no\\_test\\_ok.c" ]
  then
    toprint="sendmail-mime7to8\\_\\_ok.c"
  fi
  printf "%-25s" $toprint
  printt "lifting_+Ctg-Geo+RSM"
  incs1
  printt "lifting_+Ctg+Geo+RSM"
  incs2
  printt "lifting_+Ctg-Geo+RSM+Lazy"
  incs3
  printt "lifting_+Ctg+Geo+RSM+Lazy"
  incs4
  echo ' \\\\'
done

printf "\\hline\n"
printf "%-25s" "Total"
printf " & "
printf "%-10s" $s1
printf " & "
printf "%-10s" $s2
printf " & "
printf "%-10s" $s3
printf " & "
printf "%-10s" $s4
echo ' \\\\'
printf "%-25s" "Elapsed"
printf " & "
printf "%-10.2f" $t1
printf " & "
printf "%-10.2f" $t2
printf " & "
printf "%-10.2f" $t3
printf " & "
printf "%-10.2f" $t4
echo ' \\\\'

printf '\\end{longtable}\n'
echo '\\end{center}'
# echo '\\end{landscape}'
