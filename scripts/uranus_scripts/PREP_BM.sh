#! /usr/bin/zsh

rm -f benchmarks/*.vars
rm -f benchmarks/*.nondet
rm -f benchmarks/*.asrts
rm -f benchmarks/*.tr
rm -f benchmarks/*.trx
rm -f benchmarks/*.init

rm -f prep.txt

for i in `ls benchmarks`
do
  echo $i
  echo $i >&2
  ./trgen2 benchmarks/$i
done
