#! /usr/bin/zsh

function dobm () {
  echo $i 1/7
  timeout 500 ./CTIGAR $i 0 0 0 0 0 | tee /files/birgmei/out/"$i"_-Ctg-Geo-RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 2/7
  timeout 500 ./CTIGAR $i 1 3 0 0 0 | tee /files/birgmei/out/"$i"_+Ctg-Geo-RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 3/7
  timeout 500 ./CTIGAR $i 1 3 100 0 0 | tee /files/birgmei/out/"$i"_+Ctg+Geo-RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 4/7
  timeout 500 ./CTIGAR $i 1 3 0 3 0 | tee /files/birgmei/out/"$i"_+Ctg-Geo+RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 5/7
  timeout 500 ./CTIGAR $i 1 3 100 3 0 | tee /files/birgmei/out/"$i"_+Ctg+Geo+RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 6/7
  timeout 500 ./CTIGAR $i 1 3 0 3 3 | tee /files/birgmei/out/"$i"_+Ctg-Geo+RSM+Lazy.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
  echo $i 7/7
  timeout 500 ./CTIGAR $i 1 3 100 3 3 | tee /files/birgmei/out/"$i"_+Ctg+Geo+RSM+Lazy.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec"
}

if [ $# -ge 1 ]
then
  for i in $*
  do
    dobm
  done
else
  for i in `ls benchmarks/*.c`
  do
    dobm
  done
fi
