#! /usr/bin/zsh

function printt () {
  if [ -f ./out/benchmarks/"$fn"_consecution_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/benchmarks/"$fn"_consecution_"$1".txt | grep "$sstring"`
    if [ $? -eq 0 ]
    then
      t=`echo -n $el | sed "s/$sstring*//g"`
      printf "%-12s" $t
    else
#      if [ "$sstring" = "Elapsed total\\:" ]
#      then
#        printf "%-12s" "1200"
#      else
      printf "%-12s" " "
#      fi
    fi
  else
    printf "%-12s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

printf "%-12s" "N"
printf " , "
printf "%-12s" "G"
printf " , "
printf "%-12s" "L"
printf " , "
printf "%-12s" "GL"
printf " , "
printf "%-12s" "CPA"
echo

for i in `ls benchmarks/*.c`
do
  fn=`echo $i | sed 's/benchmarks\///g'`
  printt "+Ctg-Geo+RSM"
  printf " , "
  printt "+Ctg+Geo+RSM"
  printf " , "
  printt "+Ctg-Geo+RSM+Lazy"
  printf " , "
  printt "+Ctg+Geo+RSM+Lazy"
  if [ $sstring = "Elapsed total\\:" ]
  then
    echo -n " , "
    time=`cat out/cpachecker/$fn | egrep "user" | sed 's/\(.*\)\(user.*\)/\1/g'`
    if [ "$time" = "" ]
    then
      time=" "
    fi
    cat out/cpachecker/$fn | egrep -q "Verification result: (SAFE|UNSAFE)"
    if [ $? -eq 0 ]
    then
      printf "%-12s" "$time"
    else
      if [ "$time" = " " ]
      then
        printf "%-12s" "1200"
      else
        printf "%-12s" "$time"
      fi
    fi
  fi
  if [ "$sstring" = "Property holds:" ]
  then
    echo -n " , "
    cat out/cpachecker/$fn | egrep -q SAFE
    if [ $? -eq 0 ]
    then
      printf "%-12s" "1"
    else
      printf "%-12s" " "
    fi
  fi
  echo
done
