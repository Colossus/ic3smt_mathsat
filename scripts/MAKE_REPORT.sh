#! /usr/bin/zsh

echo '\\documentclass{article}

\\usepackage[table]{xcolor}
\\usepackage{soul}
\\usepackage{enumitem}
\\usepackage{varwidth}
\\usepackage{amsmath}
\\usepackage{longtable}
\\usepackage{lscape}
% define lightgray
\\definecolor{lightgray}{gray}{0.9}
% alternate rowcolors for all long-tables
\\let\\oldlongtable\\longtable
\\let\\endoldlongtable\\endlongtable
\\renewenvironment{longtable}{\\rowcolors{2}{white}{lightgray}\\oldlongtable} {
\\endoldlongtable}
\\usepackage{makeidx}
\\usepackage{tikz}
\\usetikzlibrary{shapes,arrows}
\\usepackage{hyperref}

\\title{CTIGAR Benchmark Auto-Report}

\\begin{document}
\\maketitle
\\tableofcontents
'

repl () {
if [ $fn = absliftsucc_consecution.csv ]
then
  toprint="Percent of Successful Abstract Liftings (Consecution)"
elif [ $fn = absliftsucc_lifting.csv ]
then
  toprint="Percent of Successful Abstract Liftings (Lifting)"
elif [ $fn = abslifttry_consecution.csv ]
then
  toprint="Number of Abstract Lifting Tries (Consecution)"
elif [ $fn = abslifttry_lifting.csv ]
then
  toprint="Number of Abstract Lifting Tries (Lifting)"
elif [ $fn = abstrctns_consecution.csv ]
then
  toprint="Number of Abstractions (Consecution)"
elif [ $fn = abstrctns_lifting.csv ]
then
  toprint="Number of Abstractions (Lifting)"
elif [ $fn = coiload_consecution.csv ]
then
  toprint="COI Load (Consecution)"
elif [ $fn = coiload_lifting.csv ]
then
  toprint="COI Load (Lifting)"
elif [ $fn = consrebuild_consecution.csv ]
then
  toprint="Number of Consecution Solver Rebuilds (Consecution)"
elif [ $fn = consrebuild_lifting.csv ]
then
  toprint="Number of Consecution Solver Rebuilds (Lifting)"
elif [ $fn = ctgs_consecution.csv ]
then
  toprint="CTGs (Consecution)"
elif [ $fn = ctgs_lifting.csv ]
then
  toprint="CTGs (Lifting)"
elif [ $fn = ctis_consecution.csv ]
then
  toprint="CTIs (Consecution)"
elif [ $fn = ctis_lifting.csv ]
then
  toprint="CTIs (Lifting)"
elif [ $fn = domainpr_consecution.csv ]
then
  toprint="Num. Domain Predicates (Consecution)"
elif [ $fn = domainpr_lifting.csv ]
then
  toprint="Num. Domain Predicates (Lifting)"
elif [ $fn = fpclauses_consecution.csv ]
then
  toprint="Fixed Point Num. Clauses (Consecution)"
elif [ $fn = fpclauses_lifting.csv ]
then
  toprint="Fixed Point Num. Clauses (Lifting)"
elif [ $fn = geomgen_consecution.csv ]
then
  toprint="Num. Successful Geom. Generalization (Consecution)"
elif [ $fn = geomgen_lifting.csv ]
then
  toprint="Num. Successful Geom. Generalizations (Lifting)"
elif [ $fn = intjoins_consecution.csv ]
then
  toprint="Int. Joins (Consecution)"
elif [ $fn = intjoins_lifting.csv ]
then
  toprint="Int. Joins (Lifting)"
elif [ $fn = intmics_consecution.csv ]
then
  toprint="Int. Mics (Consecution)"
elif [ $fn = intmics_lifting.csv ]
then
  toprint="Int. Mics (Lifting)"
elif [ $fn = k_consecution.csv ]
then
  toprint="Last Frontier (Consecution)"
elif [ $fn = k_lifting.csv ]
then
  toprint="Last Frontier (Lifting)"
elif [ $fn = maxtrdepth_consecution.csv ]
then
  toprint="Max. Trace Depth (Consecution)"
elif [ $fn = maxtrdepth_lifting.csv ]
then
  toprint="Max. Trace Depth (Lifting)"
elif [ $fn = miccalls_consecution.csv ]
then
  toprint="MIC Calls (Consecution)"
elif [ $fn = miccalls_lifting.csv ]
then
  toprint="MIC Calls (Lifting)"
elif [ $fn = micssec_consecution.csv ]
then
  toprint="MICs/Sec (Consecution)"
elif [ $fn = micssec_lifting.csv ]
then
  toprint="MICs/Sec (Lifting)"
elif [ $fn = queries_consecution.csv ]
then
  toprint="Num. Queries (Consecution)"
elif [ $fn = queries_lifting.csv ]
then
  toprint="Num. Queries (Lifting)"
elif [ $fn = queriessec_consecution.csv ]
then
  toprint="Queries/Sec (Consecution)"
elif [ $fn = queriessec_lifting.csv ]
then
  toprint="Queries/Sec (Lifting)"
elif [ $fn = redcores_consecution.csv ]
then
  toprint="Red. Cores (Consecution)"
elif [ $fn = redcores_lifting.csv ]
then
  toprint="Red. Cores (Lifting)"
elif [ $fn = rsm_consecution.csv ]
then
  toprint="Sucessful RSM (Consecution)"
elif [ $fn = rsm_lifting.csv ]
then
  toprint="Successful RSM (Lifting)"
elif [ $fn = sat_consecution.csv ]
then
  toprint="Runtime Percent in SMT Solver (Consecution)"
elif [ $fn = sat_lifting.csv ]
then
  toprint="Runtime Percent in SMT Solver (Lifting)"
elif [ $fn = slices_consecution.csv ]
then
  toprint="Num. Slices (Consecution)"
elif [ $fn = slices_lifting.csv ]
then
  toprint="Num. Slices (Lifting)"
elif [ $fn = solved_consecution.csv ]
then
  toprint="Solved Instances (Consecution)"
elif [ $fn = solved_lifting.csv ]
then
  toprint="Solved Instances (Lifting)"
elif [ $fn = spurref_consecution.csv ]
then
  toprint="Num. Spurious Refinements (Consecution)"
elif [ $fn = spurref_lifting.csv ]
then
  toprint="Num. Spurious Refinements (Lifting)"
elif [ $fn = times_consecution.csv ]
then
  toprint="Runtimes (Consecution)"
elif [ $fn = times_lifting.csv ]
then
  toprint="Runtimes (Lifting)"
fi
}

echo '\\section{Runtime Results}'
echo '\\subsection{Number of Solved Instances (Consecution)}'
cat CSV/sums.solved.consecution.tex

echo '\\subsection{Number of Solved Instances (Lifting)}'
cat CSV/sums.solved.lifting.tex

echo '\\subsection{Runtime Confusion Matrix (Consecution)}'
cat CSV/times_consecution.comparison.tex

echo '\\subsection{Runtime Confusion Matrix (Lifting)}'
cat CSV/times_lifting.comparison.tex

echo '\\subsection{Runtime Results Table Data (Consecution)}'
cat TABLES/times_consecution.csv

echo '\\subsection{Runtime Results Table Data (Lifting)}'
cat TABLES/times_lifting.csv

for i in `ls CSV/*.csv`
do
fn=`echo $i | sed 's/CSV\///g'`
repl
echo '\\section{'$toprint'}'
unset toprint
echo '\\subsection{Table Data}'
cat TABLES/$fn
echo '\\subsection{Graph For All Instances}'
cat CSV/$fn.tex
echo '\\subsection{Graph For Solved Instances}'
cat CSV/$fn.solved.tex
echo '\\subsection{Graph For Unsolved Instances}'
cat CSV/$fn.unsolved.tex
done

echo '\\section{Benchmark Sources}'
for i in `ls benchmarks/*.c`
do
fn=`echo $i | sed 's/benchmarks\///g'`
toprint=`echo $fn | sed 's/_/\\\\_/g'`
echo '\\subsection{'$toprint'}'
echo '\\begin{verbatim}'
cat $i | tr '\t' '  '
echo '\\end{verbatim}'
done

echo '\\end{document}'
