for i in `ls *.csv`                                                                      
do
  echo 'tikz("'$i'.tex", width=6, height=6)'
  echo 'par(mar=c(5,5,2,2), ps=7)'
  echo 'plot.multi.dens(na.omit('$i'[c(1,2,3,4)]),xlab="")'
  echo 'legend("'topright'", col=colors, legend=colnames('$i'),pch=c(9,9,9,9), y.intersp=2)'
  echo 'dev.off()'
  echo
  echo 'tikz("'$i'.solved.tex", width=6, height=6)'
  echo 'par(mar=c(5,5,2,2), ps=7)'
  echo 'plot.multi.dens(na.omit('$i'.solved[c(1,2,3,4)]),xlab="")'
  echo 'legend("'topright'", col=colors, legend=colnames('$i'),pch=c(9,9,9,9), y.intersp=2)'
  echo 'dev.off()'
  echo
  echo 'tikz("'$i'.unsolved.tex", width=6, height=6)'
  echo 'par(mar=c(5,5,2,2), ps=7)'
  echo 'plot.multi.dens(na.omit('$i'.unsolved[c(1,2,3,4)]),xlab="")'
  echo 'legend("'topright'", col=colors, legend=colnames('$i'),pch=c(9,9,9,9), y.intersp=2)'
  echo 'dev.off()'
  echo
done

