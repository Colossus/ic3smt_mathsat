echo 1/25
./EVAL_BM_CONSECUTION.sh > TABLES/times_consecution.csv
./EVAL_BM_LIFTING.sh > TABLES/times_lifting.csv
echo 2/25
./EVAL_BM_CONSECUTION.sh "Property holds:" > TABLES/solved_consecution.csv
./EVAL_BM_LIFTING.sh "Property holds:" > TABLES/solved_lifting.csv
echo 3/25
./EVAL_BM_CONSECUTION.sh "K: " > TABLES/k_consecution.csv
./EVAL_BM_LIFTING.sh "K: " > TABLES/k_lifting.csv
echo 4/25
./EVAL_BM_CONSECUTION.sh "\\% SAT: " > TABLES/sat_consecution.csv
./EVAL_BM_LIFTING.sh "\\% SAT: " > TABLES/sat_lifting.csv
echo 5/25
./EVAL_BM_CONSECUTION.sh "# Queries: " > TABLES/queries_consecution.csv
./EVAL_BM_LIFTING.sh "# Queries: " > TABLES/queries_lifting.csv
echo 6/25
./EVAL_BM_CONSECUTION.sh "# CTIs: " > TABLES/ctis_consecution.csv
./EVAL_BM_LIFTING.sh "# CTIs: " > TABLES/ctis_lifting.csv
echo 7/25
./EVAL_BM_CONSECUTION.sh "# CTGs: " > TABLES/ctgs_consecution.csv
./EVAL_BM_LIFTING.sh "# CTGs: " > TABLES/ctgs_lifting.csv
echo 8/25
./EVAL_BM_CONSECUTION.sh "# mic calls: " > TABLES/miccalls_consecution.csv
./EVAL_BM_LIFTING.sh "# mic calls: " > TABLES/miccalls_lifting.csv
echo 9/25
./EVAL_BM_CONSECUTION.sh "Queries\/sec: " > TABLES/queriessec_consecution.csv
./EVAL_BM_LIFTING.sh "Queries\/sec: " > TABLES/queriessec_lifting.csv
echo 10/25
./EVAL_BM_CONSECUTION.sh "Mics\/sec: " > TABLES/micssec_consecution.csv
./EVAL_BM_LIFTING.sh "Mics\/sec: " > TABLES/micssec_lifting.csv
echo 11/25
./EVAL_BM_CONSECUTION.sh "# Red. cores: " > TABLES/redcores_consecution.csv
./EVAL_BM_LIFTING.sh "# Red. cores: " > TABLES/redcores_lifting.csv
echo 12/25
./EVAL_BM_CONSECUTION.sh "# Int. joins: " > TABLES/intjoins_consecution.csv
./EVAL_BM_LIFTING.sh "# Int. joins: " > TABLES/intjoins_lifting.csv
echo 13/25
./EVAL_BM_CONSECUTION.sh "# Int. mics:  " > TABLES/intmics_consecution.csv
./EVAL_BM_LIFTING.sh "# Int. mics:  " > TABLES/intmics_lifting.csv
echo 14/25
./EVAL_BM_CONSECUTION.sh "# Domain Pr.: " > TABLES/domainpr_consecution.csv
./EVAL_BM_LIFTING.sh "# Domain Pr.: " > TABLES/domainpr_lifting.csv
echo 15/25
./EVAL_BM_CONSECUTION.sh "# Spur. Ref.: " > TABLES/spurref_consecution.csv
./EVAL_BM_LIFTING.sh "# Spur. Ref.: " > TABLES/spurref_lifting.csv
echo 16/25
./EVAL_BM_CONSECUTION.sh "# Geom. Gen.: " > TABLES/geomgen_consecution.csv
./EVAL_BM_LIFTING.sh "# Geom. Gen.: " > TABLES/geomgen_lifting.csv
echo 17/25
./EVAL_BM_CONSECUTION.sh "# RSM: " > TABLES/rsm_consecution.csv
./EVAL_BM_LIFTING.sh "# RSM: " > TABLES/rsm_lifting.csv
echo 18/25
./EVAL_BM_CONSECUTION.sh "# Abstrctns: " > TABLES/abstrctns_consecution.csv
./EVAL_BM_LIFTING.sh "# Abstrctns: " > TABLES/abstrctns_lifting.csv
echo 19/25
./EVAL_BM_CONSECUTION.sh "# MaxTrDepth: " > TABLES/maxtrdepth_consecution.csv
./EVAL_BM_LIFTING.sh "# MaxTrDepth: " > TABLES/maxtrdepth_lifting.csv
echo 20/25
./EVAL_BM_CONSECUTION.sh "# FPClauses: " > TABLES/fpclauses_consecution.csv
./EVAL_BM_LIFTING.sh "# FPClauses: " > TABLES/fpclauses_lifting.csv
echo 21/25
./EVAL_BM_CONSECUTION.sh "# AbsLiftTry: " > TABLES/abslifttry_consecution.csv
./EVAL_BM_LIFTING.sh "# AbsLiftTry: " > TABLES/abslifttry_lifting.csv
echo 22/25
./EVAL_BM_CONSECUTION.sh "\\% AbsLiftSucc:" > TABLES/absliftsucc_consecution.csv
./EVAL_BM_LIFTING.sh "\\% AbsLiftSucc:" > TABLES/absliftsucc_lifting.csv
echo 23/25
./EVAL_BM_CONSECUTION.sh "# ConsRebuild:" > TABLES/consrebuild_consecution.csv
./EVAL_BM_LIFTING.sh "# ConsRebuild:" > TABLES/consrebuild_lifting.csv
echo 24/25
./EVAL_BM_CONSECUTION.sh "# Slices: " > TABLES/slices_consecution.csv
./EVAL_BM_LIFTING.sh "# Slices: " > TABLES/slices_lifting.csv
echo 25/25
./EVAL_BM_CONSECUTION.sh "\\% COILoad: " > TABLES/coiload_consecution.csv
./EVAL_BM_LIFTING.sh "\\% COILoad: " > TABLES/coiload_lifting.csv
