#ifndef MAIN_H_
#define MAIN_H_

using namespace std;

namespace CTIGAR {

struct Options {
	size_t maxDepth = 1;
	size_t maxCTGs = 3;
	size_t halfPlaneMinPoints = 3;
	size_t halfPlaneMaxPoints = 0;
	size_t maxSpurious = 0;
	string refinementStrategy = "cons-concr-pred";
	unsigned verbosity = 0;
	size_t rebuildIntercept = 1000;
	size_t rebuildVarSlope = 200;
	size_t micAttempts = 1 << 20;
	bool pcSlicing = false;
	bool karr = true;
	string abstractor = "z3";
	unsigned int z3Random = 0;
};

}

#endif
