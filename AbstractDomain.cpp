/*
 * AbstractDomain.cpp
 *
 *  Created on: May 14, 2014
 *      Author: birgmei
 */

#include "AbstractDomain.h"

namespace CTIGAR {

AbstractDomain::AbstractDomain(Model & model1, SMTEnvironment * env1,
		const Options & opt1) :
		model(model1), opt(opt1), negInit(0), env(env1), nAbstractions(0) {
	domain = new Domain(env, opt1);
}

AbstractDomain::~AbstractDomain() {
	delete domain;
	delete env;
}

void AbstractDomain::abstract(AbstractState & ret, const AsgnVec & state) {
	// assert(state.size() == model.varsToPrime.size());
	assert(ret.size() == 0);
	++nAbstractions;
	env->push();
	set<string> stateVars;
	for (AsgnVec::const_iterator it = state.begin(); it != state.end(); ++it) {
		TermPtr term = tf.asgnToTerm(*it);
		env->assertFormula(term);
		stateVars.insert(it->getVar());
		if (it->getType() == AsgnType::DYN_ARRAY_ASGN)
			stateVars.insert(it->getArraySizeVar());
	}

	set<string> boolVars;
	size_t numPreds = 0;
	vector<size_t> asserted;
	for (size_t i = 0; i < size(); ++i) {
		const set<string> & predVars = domain->node(i).getVars();
		set<string> varDiff;
		set_difference(predVars.begin(), predVars.end(), stateVars.begin(),
				stateVars.end(), inserter(varDiff, varDiff.begin()));
		if (!varDiff.empty())
			continue;

		if (model.getHasArrays()) {
			// supporters are predicates that must also have no more variables than the state
			// additionally, they must be true in order for the predicate to appear in the final AST
			// they're supposed to work as array index verifiers: if the predicate accesses an array index
			// that's out-of-bounds w/r/t the current concrete state array, the predicate does not appear
			// in the AST. Neither should the predicate appear in the AST if it's not known that the index
			// is inside the array bounds.
			const set<size_t> & supporters = domain->node(i).getSupporters();
			vector<size_t> sDiff;
			set_difference(supporters.begin(), supporters.end(),
					asserted.begin(), asserted.end(),
					inserter(sDiff, sDiff.begin()));
			if (!sDiff.empty())
				continue;
		}

		++numPreds;
		boolVars.insert(domain->node(i).getBoolVar()->getName());
		env->assertFormula(domain->node(i).getBoolTermEquiv());
		asserted.push_back(i);
	}

	assert(env->sat());
	SatModel mod;
	env->getBoolModel(mod, boolVars);
	env->pop();
	satModelToAst(ret, mod);
	unsigned rem = 0;
	if (model.getHasArrays()) {
		rem = removeUnsupported(ret, asserted);
	}
	assert(ret.size() == numPreds - rem);
	if (opt.pcSlicing)
		reducePcRestrictions(ret);
}

unsigned AbstractDomain::removeUnsupported(AbstractState & ret,
		const vector<size_t> & asserted) {
	unsigned rem = 0;
	for (vector<size_t>::const_iterator pred = asserted.begin();
			pred != asserted.end(); ++pred) {
		const set<size_t> & supporters = domain->node(*pred).getSupporters();
		for (set<size_t>::const_iterator supporter = supporters.begin();
				supporter != supporters.end(); ++supporter) {
			AbstractState::iterator it = find(ret.begin(), ret.end(),
					(long) ((*supporter) + 1));
			if (it == ret.end()) {
				if (opt.verbosity >= 3) {
					cout << (*pred) + 1 << " is unsupported by "
							<< ((*supporter) + 1)
							<< " and must therefore be removed." << endl;
				}
				++rem;
				it = find(ret.begin(), ret.end(), (long) ((*pred) + 1));
				if (it != ret.end()) {
					ret.erase(it);
					break;
				}
				it = find(ret.begin(), ret.end(), (long) (-((*pred) + 1)));
				assert(it != ret.end());
				ret.erase(it);
				break;
			}
		}
	}
	return rem;
}

bool AbstractDomain::refine(TermPtr term) {
	size_t ret = domain->add(term, model);
	return ret != ((size_t) -1);
}

void AbstractDomain::toTermVec(TermVec & vec, const AbstractState & ast,
		PredicateMap * pm) const {
	for (size_t i = 0; i < ast.size(); ++i) {
		assert(ast[i] != 0);
		assert(((unsigned ) abs(ast[i])) <= size());
		assert(ast[i] != 0);
		TermPtr t;
		if (ast[i] > 0) {
			t = predicate(ast[i]);
			vec.push_back(t);
		} else {
			t = tf.mkNot(predicate(abs(ast[i])));
			vec.push_back(t);
		}
		if (pm) {
			pm->insert(PredicateMap::value_type(t, ast[i]));
		}
	}
}

TermPtr AbstractDomain::toTerm(const AbstractState & ast) const {
	TermVec termVec;
	toTermVec(termVec, ast);
	return tf.mkAnd(termVec);
}

// PRE: idx must be positive

TermPtr AbstractDomain::predicate(size_t idx) const {
	// subtract 1: idx starts from 1 since the index's
	// sign tells us whether to take it positively or negatively
	return domain->term(idx - 1);
}

TermPtr AbstractDomain::literal(long idx) const {
	assert(idx != 0);
	if (idx < 0) {
		return tf.mkNot(domain->term(abs(idx) - 1));
	} else {
		return domain->term(idx - 1);
	}
}

void AbstractDomain::getOccurringVariables(set<string> & vars,
		const AbstractState & ast) const {
	for (AbstractState::const_iterator it = ast.begin(); it != ast.end();
			++it) {
		size_t idx = abs(*it) - 1;
		const set<string> & domainVars = domain->node(idx).getVars();
		vars.insert(domainVars.begin(), domainVars.end());
	}
}

void AbstractDomain::getOccurringVariables(set<string> & vars,
		const long pred) const {
	assert(vars.size() == 0);
	size_t idx = abs(pred) - 1;
	const set<string> & domainVars = domain->node(idx).getVars();
	vars.insert(domainVars.begin(), domainVars.end());
}

void AbstractDomain::loadTrx(void) {
	for (TermVec::const_iterator it = model.getTrxs().begin();
			it != model.getTrxs().end(); ++it) {
		TermPtr trx = *it;
		if (trx->type == TermType::EQ) {
			TermPtr cmp = tf.mkComparison(ComparisonType::LEQ, trx->getArg(0),
					trx->getArg(1));
			refine(cmp);
			cmp = tf.mkComparison(ComparisonType::GEQ, trx->getArg(0),
					trx->getArg(1));
			refine(cmp);
		} else {
			refine(trx);
		}
	}
}

void AbstractDomain::reducePcRestrictions(AbstractState & ast) {
	long minV, maxV;
	minV = 0;
	maxV = model.getMaxPC();
	long bestMin = 0, bestMax = 0;
	set<long> toErase;
	for (AbstractState::const_iterator it = ast.begin(); it != ast.end();
			++it) {
		assert(*it != 0);
		set<string> variables;
		getOccurringVariables(variables, *it);
		if (variables.find(".s") == variables.end() || variables.size() > 1)
			continue;
		TermPtr lit = literal(abs(*it));
		// this works only with mathsat! other solvers
		// might not treat all inequalities as LEQ!
		if (lit->type == TermType::LEQ) {
			TermPtr l = lit->getArg(0);
			TermPtr r = lit->getArg(1);
			if (l->isVar()) {
				long val = r->toLong();
				assert(val >= 0);
				assert(val <= model.getMaxPC());
				if (*it > 0) {
					if (val < maxV) {
						assert(bestMax == 0 || bestMax != bestMin);
						maxV = val;
						toErase.insert(bestMax);
						bestMax = *it;
					} else {
						toErase.insert(*it);
					}
				} else {
					if (val + 1 > minV) {
						assert(bestMax == 0 || bestMax != bestMin);
						minV = val + 1;
						toErase.insert(bestMin);
						bestMin = *it;
					} else {
						toErase.insert(*it);
					}
				}
			} else {
				assert(r->isVar());
				long val = l->toLong();
				assert(val >= 0);
				assert(val <= model.getMaxPC());
				if (*it > 0) {
					if (val > minV) {
						assert(bestMax == 0 || bestMax != bestMin);
						minV = val;
						toErase.insert(bestMin);
						bestMin = *it;
					} else {
						toErase.insert(*it);
					}
				} else {
					if (val - 1 < maxV) {
						assert(bestMax == 0 || bestMax != bestMin);
						maxV = val - 1;
						toErase.insert(bestMax);
						bestMax = *it;
					} else {
						toErase.insert(*it);
					}
				}
			}
		} else if (lit->type == TermType::EQ) {
			if (*it < 0)
				continue;
			TermPtr l = lit->getArg(0);
			TermPtr r = lit->getArg(1);
			long val;
			if (l->isVar()) {
				val = r->toLong();
			} else {
				assert(r->isVar());
				val = l->toLong();
			}
			assert(val >= 0);
			assert(val <= model.getMaxPC());
			maxV = minV = val;
			toErase.insert(bestMax);
			toErase.insert(bestMin);
			bestMax = bestMin = *it;
		} else
			continue;
	}
	assert(minV <= maxV);

	if (opt.verbosity >= 3)
		cout << "Deleting redundant PC restrictions from AST" << endl;
	ast.erase(
			std::remove_if(ast.begin(), ast.end(),
					[&toErase](long l) {return toErase.find(l) != toErase.end();}),
			ast.end());
}

const Predicate & AbstractDomain::node(size_t idx) {
	return domain->node(idx - 1);
}

void AbstractDomain::satModelToAst(AbstractState & ast, const SatModel & mod) {
	for (SatModel::const_iterator it = mod.begin(); it != mod.end(); ++it) {
		string var = it->getVar();
		if (it->isTrue()) {
			ast.push_back(domain->boolVarIdx(var) + 1);
		} else {
			ast.push_back(-(domain->boolVarIdx(var) + 1));
		}
	}
}

void AbstractDomain::loadRecommendedAbstractions(void) {
	for (TermVec::const_iterator it =
			model.getRecommendedAbstractions().begin();
			it != model.getRecommendedAbstractions().end(); ++it) {
		if (opt.verbosity >= 1)
			cout << "Loading recommended abstraction " << *it << endl;
		refine(*it);
	}
}

IneqDomain::IneqDomain(Model & model1, SMTEnvironment * env1, Options & opt1) :
		AbstractDomain(model1, env1, opt1) {
	const TermVec & init = model.getInitialCondition();
	TermPtr i = tf.mkAnd(init);
	size_t initIdx = AbstractDomain::domain->add(i, model1);
	assert(initIdx != ((size_t ) -1));
	negInit = -(initIdx + 1);

	for (VarPrimeMap::iterator it1 = model.vars.toPrime.begin();
			it1 != model.vars.toPrime.end(); ++it1) {
		char first1 = it1->first.c_str()[0];
		if (first1 == '.')
			continue;
		if (model.vars.all[it1->first] != VarType::INT)
			continue;
		for (VarPrimeMap::iterator it2 = it1; it2 != model.vars.toPrime.end();
				++it2) {
			if (it1 == it2)
				continue;
			if (model.vars.all[it2->first] != VarType::INT)
				continue;
			char first2 = it2->first.c_str()[0];

			if (first2 == '.')
				continue;
			TermPtr term = tf.mkComparison(ComparisonType::LT,
					tf.mkIntVar(it1->first), tf.mkIntVar(it2->first));
			refine(term);
		}
	}
}

IneqDomain::~IneqDomain() {
}

KarrIneqDomain::KarrIneqDomain(Model & model1, SMTEnvironment * env1,
		Options & opt1) :
		IneqDomain(model1, env1, opt1) {
	vector<string> vars;
	for (VarTypeMap::const_iterator it = model.vars.program.begin();
			it != model.vars.program.end(); ++it) {
		if (it->first == ".s" || it->second != VarType::INT)
			continue;
		vars.push_back(it->first);
	}

	for (map<long, vector<Affine>>::const_iterator it = model.getLines().begin();
			it != model.getLines().end(); ++it) {
		for (vector<Affine>::const_iterator it2 = it->second.begin();
				it2 != it->second.end(); ++it2) {
			assert(it2->size() == vars.size() + 1);
			TermPtr term = tf.mkInt(0);
			for (size_t i = 1; i < it2->size(); ++i) {
				TermPtr coeff = tf.mkInt((*it2)[i]);
				TermPtr var = tf.mkIntVar(vars[i - 1]);
				term = tf.mkPlus(term, tf.mkTimes(coeff, var));
			}
			TermPtr coeff = tf.mkInt(-((*it2)[0]));
			TermPtr cmp1 = tf.mkComparison(ComparisonType::LEQ, coeff, term);
			TermPtr cmp2 = tf.mkComparison(ComparisonType::GEQ, coeff, term);
			refine(cmp1);
			refine(cmp2);
		}
	}

	loadTrx();
}

KarrIneqDomain::~KarrIneqDomain() {
}

DefaultDomain::DefaultDomain(Model & model1, SMTEnvironment * env1,
		Options & opt1) :
		AbstractDomain(model1, env1, opt1) {
	const TermVec & init = model.getInitialCondition();
	TermPtr i = tf.mkAnd(init);
	size_t initIdx = AbstractDomain::domain->add(i, model1);
	assert(initIdx != ((size_t ) -1));
	negInit = -(initIdx + 1);
}

DefaultDomain::~DefaultDomain() {
}

}

