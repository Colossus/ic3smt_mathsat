/*
 * KarrCFG.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: johannes
 */

#include <assert.h>
#include <iostream>

#include "KarrCFG.h"

using namespace std;

namespace CTIGAR {

void subtract(Affine & ret, const Affine & a, const Affine & b) {
	assert(a.size() == b.size());
	assert(ret.empty());
	for (size_t i = 0; i < a.size(); ++i) {
		ret.push_back(a[i] - b[i]);
	}
}

void subtract(Affine & inOut, const Affine & b) {
	assert(inOut.size() == b.size());
	for (size_t i = 0; i < inOut.size(); ++i) {
		inOut[i] = inOut[i] - b[i];
	}
}

void add(Affine & inOut, const Affine & b) {
	assert(inOut.size() == b.size());
	for (size_t i = 0; i < inOut.size(); ++i) {
		inOut[i] = inOut[i] + b[i];
	}
}

void multiply(Affine & ret, const Affine & a, long factor) {
	assert(ret.empty());
	for (size_t i = 0; i < a.size(); ++i) {
		ret.push_back(a[i] * factor);
	}
}

void multiply(Affine & a, long factor) {
	for (size_t i = 0; i < a.size(); ++i) {
		a[i] = a[i] * factor;
	}
}

long gcd(long a, long b) {
	while (true) {
		if (a == 0)
			return b;
		b %= a;
		if (b == 0)
			return a;
		a %= b;
	}
	assert(false);
	return 0;
}

long lcm(long a, long b) {
	long temp = gcd(a, b);
	return temp ? (a / temp * b) : 0;
}

void printAffine(const Affine & aff, const vector<string> & vars) {
	if (aff.empty()) {
		cout << "Empty affine" << endl;
	} else if (aff.size() == vars.size() + 1) {
		for (size_t i = 1; i <= vars.size(); ++i) {
			cout << aff[i] << vars[i - 1] << " + ";
		}
		cout << aff[0] << endl;
	} else {
		assert(aff.size() == vars.size());
		size_t i;
		for (i = 0; i < vars.size() - 1; ++i) {
			cout << vars[i] << " = " << aff[i] << " | ";
		}
		cout << vars[i] << " = " << aff[i] << endl;
	}
}

KarrCFGFactory::KarrCFGFactory(const VarTypeMap & programVariables,
		const VarTypeMap & inputVariables, const Options & opt) :
		env(NULL), initPc(-1), opt(opt) {
	assert(!programVariables.empty());
	size_t i = 0;
	for (VarTypeMap::const_iterator it = programVariables.begin();
			it != programVariables.end(); ++it) {
		if (it->first == ".s" || it->second != VarType::INT)
			continue;
		this->programVariables.push_back(it->first);
		varIndexMap.insert(map<string, size_t>::value_type(it->first, i));
		++i;
	}
	this->inputVariables = inputVariables;
	VarTypeMap allVariables;
	for (VarTypeMap::const_iterator it = inputVariables.begin();
			it != inputVariables.end(); ++it) {
		allVariables.insert(VarTypeMap::value_type(it->first, it->second));
	}
	for (VarTypeMap::const_iterator it = programVariables.begin();
			it != programVariables.end(); ++it) {
		allVariables.insert(VarTypeMap::value_type(it->first, it->second));
		allVariables.insert(
				VarTypeMap::value_type(it->first + "_p", it->second));
	}
	env = new MSatSMTEnvironment("karr", allVariables, opt);
}

KarrCFGFactory::~KarrCFGFactory() {
	for (vector<CFGNode *>::const_iterator it = allNodes.begin();
			it != allNodes.end(); ++it) {
		delete *it;
	}
	for (vector<CFGTrans *>::const_iterator it = allTrans.begin();
			it != allTrans.end(); ++it) {
		delete *it;
	}
}

void KarrCFGFactory::karrSucc(long pc, long succPc, string condition) {
	createNode(pc);
	createNode(succPc);
	createTrans(pc, condition);

	pair<int, string> p(pc, condition);
	CFGTrans * trans = pcConditionToTrans[p];
	trans->next = pcToNode[succPc];
	pcToNode[pc]->succs.push_back(trans);
}

void KarrCFGFactory::karrLoad(long pc, string condition, string var,
		string function) {
	TermPtr term = env->parsePredicate(function);
	createTrans(pc, condition);

	pair<int, string> p(pc, condition);
	CFGTrans * trans = pcConditionToTrans[p];
	size_t idx = varIndexMap[var];
	Affine aff;
	dissolveTerm(aff, term);
	trans->stmt[idx] = aff;
}

void KarrCFGFactory::karrInit(string initString) {
	TermPtr term = env->parsePredicate(initString);
	assert(term->type == TermType::EQ);
	TermPtr l = term->getArg(0);
	TermPtr r = term->getArg(1);
	TermPtr var, num;
	if (l->isVar()) {
		var = l;
		num = r;
	} else {
		var = r;
		num = l;
	}
	assert(var->isVar() && num->type == TermType::INTEGER);
	assert(var->getName() == ".s");
	long val = num->toLong();
	initPc = val;
}

void KarrCFGFactory::printCFG(void) {
	assert(pcToNode[initPc] != NULL);
	CFGNode * init = pcToNode[initPc];
	set<CFGNode *> visited;
	printNode(init, visited);
}

void KarrCFGFactory::printNode(CFGNode * node, set<CFGNode *> & visited) {
	if (visited.find(node) != visited.end())
		return;
	visited.insert(node);
	for (vector<CFGTrans *>::const_iterator it = node->succs.begin();
			it != node->succs.end(); ++it) {
		CFGTrans * trans = *it;
		CFGNode * next = trans->next;
		if (opt.verbosity >= 3)
			cout << node->pc << " -> " << next->pc << endl;
		assert(trans->stmt.size() == programVariables.size());
		for (size_t i = 0; i < trans->stmt.size(); ++i) {
			cout << programVariables[i] << " = ";
			printAffine(trans->stmt[i], programVariables);
		}
		printNode(next, visited);
	}
}

void KarrCFGFactory::dissolveTerm(Affine & ret, TermPtr term) {
	assert(ret.empty());
	ret.resize(programVariables.size() + 1);
	TermVec mults;
	tf.dissolveOps(mults, term, TermType::PLUS);
	try {
		for (TermVec::const_iterator it = mults.begin();
				it != mults.end(); ++it) {
			if ((*it)->type == TermType::TIMES) {
				dissolveTimes(ret, *it);
			} else if ((*it)->type == TermType::INTEGER) {
				dissolveNumeral(ret, *it);
			} else if ((*it)->isVar()) {
				dissolveVar(ret, *it);
			} else {
				assert(false);
			}
		}
	} catch (const InputVariableException &) {
		ret.clear();
	}
}

void KarrCFGFactory::dissolveTimes(Affine & ret, TermPtr term)
		throw (InputVariableException) {
	TermPtr l = term->getArg(0);
	TermPtr r = term->getArg(1);
	TermPtr num, var;
	if (l->type == TermType::INTEGER) {
		num = l;
		var = r;
	} else if (r->type == TermType::INTEGER) {
		num = r;
		var = l;
	} else {
		assert(false);
	}
	long factor = num->toLong();

	if (var->isVar()) {
		string varName = var->getName();
		if (inputVariables.find(varName) != inputVariables.end())
			throw InputVariableException();
		add(ret, varName, factor);
	} else {
		Affine sub;
		dissolveTerm(sub, var);
		multiply(sub, factor);
		CTIGAR::add(ret, sub);
	}
}

void KarrCFGFactory::dissolveNumeral(Affine & ret, TermPtr term)
		throw (InputVariableException) {
	long intercept = term->toLong();
	ret[0] += intercept;
}

void KarrCFGFactory::dissolveVar(Affine & ret, TermPtr term)
		throw (InputVariableException) {
	string varName = term->getName();
	if (inputVariables.find(varName) != inputVariables.end())
		throw InputVariableException();
	add(ret, varName, 1);
}

void KarrCFGFactory::add(Affine & ret, const string & varName, long factor) {
	size_t idx = varIndexMap[varName] + 1;
	ret[idx] += factor;
}

void KarrCFGFactory::createNode(long pc) {
	if (pcToNode.find(pc) == pcToNode.end()) {
		CFGNode * node = new CFGNode(pc);
		pcToNode[pc] = node;
		allNodes.push_back(node);
	}
}

void KarrCFGFactory::createTrans(long pc, string condition) {
	pair<int, string> p(pc, condition);
	if (pcConditionToTrans.find(p) == pcConditionToTrans.end()) {
		CFGTrans * trans = new CFGTrans(programVariables);
		pcConditionToTrans[p] = trans;
		allTrans.push_back(trans);
	}
}

Karr::Karr(KarrCFGFactory * factory, Options & opt) :
		vars(factory->programVariables), init(
				factory->pcToNode[factory->initPc]), inputVars(
				factory->inputVariables), opt(opt) {

}

Karr::~Karr() {
}

void Karr::collectDeps(set<size_t> & deps, const vector<Affine> & vecs) {
	assert(!vecs.empty());
	for (size_t i = 0; i < vecs[0].size(); ++i) {
		deps.insert(i);
	}
	for (vector<Affine>::const_iterator it = vecs.begin(); it != vecs.end();
			++it) {
		size_t i = firstNonZeroIndex(*it);
		deps.erase(i);
		if (opt.verbosity >= 3) {
			printAffine(*it, vars);
			cout << "Erasing " << i << endl;
		}
	}
	if (opt.verbosity >= 3) {
		for (set<size_t>::const_iterator it = deps.begin(); it != deps.end();
				++it) {
			cout << *it << " " << endl;
		}
	}
}

void Karr::extractLines(map<long, vector<Affine>> & lines, CFGNode * v) {
	Affine line1;
	const Affine & origin = g[v].front();
	line1.push_back(1);
	line1.insert(line1.end(), origin.begin(), origin.end());
	const vector<Affine> & vecs = gb_p[v];
	map<size_t, Affine> nzMap;
	for (vector<Affine>::const_iterator it = vecs.begin(); it != vecs.end();
			++it) {
		Affine line;
		line.push_back(0);
		line.insert(line.end(), it->begin(), it->end());
		size_t nz = firstNonZeroIndex(line);
		subtractToZero(line1, nz, line);
		nzMap.insert(map<size_t, Affine>::value_type(nz, line));
	}
	vector<Affine> matrix;
	matrix.push_back(line1);
	for (size_t i = 0; i < line1.size(); ++i) {
		const Affine & line = nzMap[i];
		if (!line.empty())
			matrix.push_back(line);
	}
	set<size_t> dependents;
	collectDeps(dependents, matrix);
	if (dependents.empty())
		return;
	long l = 1;
	for (vector<Affine>::iterator it = matrix.begin(); it != matrix.end();
			++it) {
		size_t nz = firstNonZeroIndex(*it);
		if ((*it)[nz] < 0) {
			multiply(*it, -1);
		}
		l = lcm(l, (*it)[nz]);
		assert(l > 0);
	}
	for (vector<Affine>::iterator it = matrix.begin(); it != matrix.end();
			++it) {
		size_t nz = firstNonZeroIndex(*it);
		multiply(*it, l / (*it)[nz]);
	}
	if (opt.verbosity >= 3) {
		cout << "Matrix: " << endl;
		for (vector<Affine>::const_iterator it2 = matrix.begin();
				it2 != matrix.end(); ++it2) {
			cout << "\t";
			printAffine(*it2, vars);
		}
	}
	for (set<size_t>::const_iterator it = dependents.begin();
			it != dependents.end(); ++it) {
		Affine base;
		vector<Affine>::const_iterator curLine = matrix.begin();
		for (size_t i = 0; i < line1.size(); ++i) {
			if (i == *it) {
				base.push_back(-l);
			} else if (dependents.find(i) != dependents.end()) {
				base.push_back(0);
			} else {
				base.push_back((*curLine)[*it]);
				++curLine;
			}
		}
		assert(curLine == matrix.end());
		lines[v->pc].push_back(base);
		assert(base.size() == vars.size() + 1);
		if (opt.verbosity >= 3) {
			cout << "Found line: ";
			printAffine(base, vars);
		}
	}
}

void Karr::getLines(map<long, vector<Affine>> & lines) {
	for (map<CFGNode *, vector<Affine>>::const_iterator it = g.begin();
			it != g.end(); ++it) {
		if (opt.verbosity >= 3) {
			cout << "At PC = " << it->first->pc << endl;
			for (vector<Affine>::const_iterator it2 = it->second.begin();
					it2 != it->second.end(); ++it2) {
				cout << "\t";
				printAffine(*it2, vars);
			}
			cout << endl;
			const vector<Affine> & vec = gb_p[it->first];
			for (vector<Affine>::const_iterator it2 = vec.begin();
					it2 != vec.end(); ++it2) {
				cout << "\t";
				printAffine(*it2, vars);
			}
		}
		if (it->second.size() <= 1)
			continue;
		extractLines(lines, it->first);
	}
}

void Karr::perform(void) {
	initialize();
	while (!w.empty()) {
		pair<CFGNode *, Affine> ux = *(w.begin());
		w.erase(w.begin());
		CFGNode * u = ux.first;
		Affine x = ux.second;
		for (vector<CFGTrans *>::const_iterator it = u->succs.begin();
				it != u->succs.end(); ++it) {
			const vector<Affine> & s = (*it)->stmt;
			vector<Affine> ts;
			apply(ts, s, x);
			CFGNode * v = (*it)->next;
			for (vector<Affine>::iterator t = ts.begin(); t != ts.end(); ++t) {
				if (addToHull(*t, v)) {
					w.insert(pair<CFGNode *, Affine>(v, *t));
					if (opt.verbosity >= 3) {
						cout << "Adding at " << v->pc << " ";
						printAffine(*t, vars);
					}
				}
			}
		}
	}
}

void Karr::printTerms(const map<long, vector<Affine>> & lines) {
	for (map<long, vector<Affine>>::const_iterator it = lines.begin();
			it != lines.end(); ++it) {
		cout << "At PC = " << it->first << endl;
		for (vector<Affine>::const_iterator it2 = it->second.begin();
				it2 != it->second.end(); ++it2) {
			printAffine(*it2, vars);
		}
	}
}

void Karr::initialize(void) {
	assert(g.empty());
	assert(w.empty());
	assert(init != NULL);
	Affine zero;
	zero.resize(vars.size());
	addToHull(zero, init);
	w.insert(pair<CFGNode *, Affine>(init, zero));
	for (size_t i = 0; i < vars.size(); ++i) {
		zero[i] = 1;
		addToHull(zero, init);
		w.insert(pair<CFGNode *, Affine>(init, zero));
		zero[i] = 0;
	}
}

void Karr::apply(vector<Affine> & ret, const vector<Affine> & ss,
		const Affine & x) {
	assert(ss.size() == x.size());
	assert(ret.empty());
	Affine r;
	r.resize(x.size());
	ret.push_back(r);
	for (size_t i = 0; i < ss.size(); ++i) {
		const Affine & s = ss[i];
		if (s.empty()) {
			// we have a nondet assignment to this variable;
			// branch with var set to 1 and var set to 0
			size_t retSize = ret.size();
			for (size_t j = 0; j < retSize; ++j) {
				ret[j][i] = 0;
				Affine rj = ret[j];
				rj[i] = 1;
				ret.push_back(rj);
			}
		} else {
			assert(s.size() == x.size() + 1);
			long xj = s[0];
			for (size_t j = 1; j < s.size(); ++j) {
				xj += s[j] * x[j - 1];
			}
			for (vector<Affine>::iterator it = ret.begin(); it != ret.end();
					++it) {
				assert(it->size() == x.size());
				(*it)[i] = xj;
			}
		}
	}
}

bool Karr::isZero(const Affine & x) {
	for (size_t i = 0; i < x.size(); ++i) {
		if (x[i] != 0)
			return false;
	}
	return true;
}

size_t Karr::firstNonZeroIndex(const Affine & x) {
	assert(!x.empty());
	for (size_t i = 0; i < x.size(); ++i) {
		if (x[i] != 0)
			return i;
	}
	assert(false);
	return 0;
}

void Karr::subtractToZero(Affine & inOut, const size_t index,
		const Affine & toSubtract) {
	assert(inOut.size() == toSubtract.size());
	assert(index < inOut.size());
	if (inOut[index] == 0)
		return;
	long l = lcm(abs(inOut[index]), abs(toSubtract[index]));
	assert(l != 0);
	Affine toSubtractMultiple;
	multiply(toSubtractMultiple, toSubtract, l / toSubtract[index]);
	multiply(inOut, l / inOut[index]);
	if (inOut[index] < 0) {
		multiply(inOut, -1);
	}
	if (toSubtractMultiple[index] < 0) {
		multiply(toSubtractMultiple, -1);
	}
	subtract(inOut, toSubtractMultiple);
}

bool Karr::addToHull(const Affine & t, CFGNode * v) {
	if (g[v].empty()) {
		g[v].push_back(t);
		return true;
	}
	Affine x0 = g[v].front();
	Affine x;
	subtract(x, t, x0);
	const vector<Affine> & b_p = gb_p[v];
	for (vector<Affine>::const_iterator it = b_p.begin(); it != b_p.end();
			++it) {
		size_t i = firstNonZeroIndex(*it);
		subtractToZero(x, i, *it);
	}
	if (isZero(x))
		return false;
	g[v].push_back(t);

	addToBase(x, v);
	return true;
}

void Karr::addToBase(Affine & x_p, CFGNode * v) {
	vector<Affine> & b_p = gb_p[v];
	size_t nz = firstNonZeroIndex(x_p);
	if (x_p[nz] < 0) {
		multiply(x_p, -1);
	}
	for (size_t i = 0; i < b_p.size(); ++i) {
		Affine & xi = b_p[i];
		if (xi[nz] == 0)
			continue;
		long l = lcm(abs(x_p[nz]), abs(xi[nz]));
		Affine toSubtractMultiple;
		multiply(toSubtractMultiple, x_p, l / x_p[nz]);
		Affine newXi;
		if (xi[nz] < 0) {
			multiply(newXi, xi, -1);
		} else {
			newXi = xi;
		}
		multiply(newXi, l / newXi[nz]);
		subtract(newXi, toSubtractMultiple);
		assert(newXi[nz] == 0);
		b_p[i] = newXi;
	}
	b_p.push_back(x_p);
}

}
