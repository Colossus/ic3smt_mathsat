printtex() {
  echo "\\documentclass{standalone}
  \\usepackage{tikz}
  \\usetikzlibrary{calc}

  \\input{../macros}

  \\begin{document}" > ~/MathSat/cav14-ic3-cegar/pics/$1
  cat ~/MathSat/ic3_smt/CSV/$1 >> ~/MathSat/cav14-ic3-cegar/pics/$1
  echo "\\end{document}" >> ~/MathSat/cav14-ic3-cegar/pics/$1
}

cp CSV/times_cons.comparison.tex CSV/scatter_cons.tex
cp CSV/times_lift.comparison.tex CSV/scatter_lift.tex

# printtex scatter_cons.tex
# printtex scatter_lift.tex
printtex percent_lifted.tex
printtex preds_runtime.tex
printtex unlifted.tex
