Own:
f91
mod3

Beautiful interpolants:
xy0
xy10
xy4
xyz
xyz2
dillig03
dillig05
dillig09
dillig09 without infinite loop
dillig09 without infinite loop and without unnecessary assignment in last loop
dillig17
dillig28

http://www.cfdvs.iitb.ac.in/~bhargav/dagger.php

fig1a
f2

http://pub.ist.ac.at/~agupta/invgen/

heapsort
up

Configurations:
3 3 0 0
3 3 100 3
3 3 0 3
3 3 100 0
0 0 100 3

Additionally, with and without lazy refinement

Compare with: CPAChecker, UFO, InvGen

Useless, but working:
dillig01
dillig07
dillig15
ex1 (useless)
