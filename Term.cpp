/*
 * Term.cpp
 *
 *  Created on: Apr 7, 2014
 *      Author: Johannes Birgmeier
 */

#include "Term.h"

#include <assert.h>
#include <stddef.h>
#include <iterator>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

namespace CTIGAR {

ReturnType::E returnType(TermType::E termType) {
	switch (termType) {
	case TermType::NOTERM:
	case TermType::ERROR:
		assert(false);
		return ReturnType::INT;
	case TermType::BOOL_VAR:
	case TermType::EQ:
	case TermType::LEQ:
	case TermType::AND:
	case TermType::OR:
	case TermType::NOT:
	case TermType::TRUE:
	case TermType::FALSE:
		return ReturnType::BOOL;
	case TermType::INT_VAR:
	case TermType::PLUS:
	case TermType::TIMES:
	case TermType::SELECT:
	case TermType::INTEGER:
		return ReturnType::INT;
	case TermType::ARRAY_VAR:
	case TermType::STORE:
		return ReturnType::ARRAY;
	default:
		assert(false);
		return ReturnType::INT;
	}
}

Term::~Term() {
}

/*
 *
 MOD_CONGR
 */

const string Term::toString(void) const {
	switch (type) {
	case TermType::BOOL_VAR:
	case TermType::INT_VAR:
	case TermType::ARRAY_VAR:
		return getName();
	case TermType::EQ:
		return "(= " + getArg(0)->toString() + " " + getArg(1)->toString() + ")";
	case TermType::LEQ:
		return "(<= " + getArg(0)->toString() + " " + getArg(1)->toString()
				+ ")";
	case TermType::AND:
		return "(and " + getArg(0)->toString() + " " + getArg(1)->toString()
				+ ")";
	case TermType::OR:
		return "(or " + getArg(0)->toString() + " " + getArg(1)->toString()
				+ ")";
	case TermType::NOT:
		return "(not " + getArg(0)->toString() + ")";
	case TermType::PLUS:
		return "(+ " + getArg(0)->toString() + " " + getArg(1)->toString() + ")";
	case TermType::TIMES:
		return "(* " + getArg(0)->toString() + " " + getArg(1)->toString() + ")";
	case TermType::SELECT:
		return "(select " + getArg(0)->toString() + " " + getArg(1)->toString()
				+ ")";
	case TermType::STORE:
		return "(store " + getArg(0)->toString() + " " + getArg(1)->toString()
				+ " " + getArg(2)->toString() + ")";
	case TermType::INTEGER:
		return to_string(val);
	case TermType::TRUE:
		return "true";
	case TermType::FALSE:
		return "false";
	case TermType::UNARY_MINUS:
		return "(- " + getArg(0)->toString() + ")";
	case TermType::MOD_CONGR:
		return "(mod_congr_" + to_string(toLong()) + " " + getArg(0)->toString()
				+ getArg(1)->toString() + ")";
	case TermType::FORALL: {
		stringstream ss;
		ss << "(forall (";
		for (vector<string>::const_iterator it = boundVars.begin();
				it != boundVars.end(); ++it) {
			ss << "(" << *it << " Int)";
		}
		ss << ") ";
		assert(args.size() == 1);
		ss << args[0]->toString() << ")";
		return ss.str();
		break;
	}
	case TermType::BOUND_VAR:
		return getName();
	default:
		assert(false);
		return "";
	}
}

void Term::findVars(set<string> & vars) const {
	switch (type) {
	case TermType::BOOL_VAR:
	case TermType::INT_VAR:
	case TermType::ARRAY_VAR:
		vars.insert(name);
		break;
	default:
		for (TermVec::const_iterator it = args.begin(); it != args.end();
				++it) {
			(*it)->findVars(vars);
		}
		break;
	}
}

std::ostream & operator<<(std::ostream & stream, const Term & t) {
	return stream << t.toString();
}

void TermFactory::dissolveOps(TermVec & ds, TermPtr term,
		TermType::E kind) const {
	assert(kind != TermType::NOTERM);
	bool recurse = term->type == kind;
	if (recurse) {
		dissolveOps(ds, term->getArg(0), kind);
		dissolveOps(ds, term->getArg(1), kind);
	} else {
		ds.push_back(term);
	}
}

void TermFactory::toTermVec(TermVec & ret, const AsgnVec & in) const {
	assert(ret.empty());
	for (AsgnVec::const_iterator it = in.begin(); it != in.end(); ++it) {
		ret.push_back(asgnToTerm(*it));
	}
}

TermPtr TermFactory::mkTrue(void) const {
	return make_shared<Term>(Term(TermType::TRUE));
}

TermPtr TermFactory::mkFalse(void) const {
	return make_shared<Term>(Term(TermType::FALSE));
}

TermPtr TermFactory::mkIntVar(string name) const {
	Term ret(TermType::INT_VAR);
	ret.name = name;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkArrayVar(string name) const {
	Term ret(TermType::ARRAY_VAR);
	ret.name = name;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkConstArray(TermPtr val) const {
	Term ret(TermType::CONST_ARRAY);
	ret.args.push_back(val);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkBoundVar(string name) const {
	Term ret(TermType::BOUND_VAR);
	ret.name = name;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkInt(const long l) const {
	Term ret(TermType::INTEGER);
	ret.val = l;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkArraySelect(TermPtr var, TermPtr idx) const {
	Term ret(TermType::SELECT);
	ret.args.push_back(var);
	ret.args.push_back(idx);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkArrayStore(TermPtr var, TermPtr idx, TermPtr val) const {
	Term ret(TermType::STORE);
	ret.args.push_back(var);
	ret.args.push_back(idx);
	ret.args.push_back(val);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkBoolVar(string name) const {
	Term ret(TermType::BOOL_VAR);
	ret.name = name;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkNot(TermPtr term) const {
	Term ret(TermType::NOT);
	if (term->type == TermType::NOT) return term->getArg(0);
	ret.args.push_back(term);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::deepNegate(TermPtr term) const {
	switch (term->type) {
	case TermType::TRUE:
		return mkFalse();
	case TermType::FALSE:
		return mkTrue();
	case TermType::EQ:
		return mkComparison(ComparisonType::NEQ, term->getArg(0),
				term->getArg(1));
	case TermType::BOOL_VAR:
		return mkNot(term);
	case TermType::LEQ:
		return mkComparison(ComparisonType::GT, term->getArg(0),
				term->getArg(1));
	case TermType::AND: {
		Term ret(TermType::OR);
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			ret.args.push_back(deepNegate(*it));
		}
		return make_shared<Term>(ret);
	}
	case TermType::OR: {
		Term ret(TermType::AND);
		for (TermVec::const_iterator it = term->getArgs().begin();
				it != term->getArgs().end(); ++it) {
			ret.args.push_back(deepNegate(*it));
		}
		return make_shared<Term>(ret);
	}
	case TermType::NOT:
		return term->getArg(0);
	default:
		assert(false);
		return mkErrorTerm();
	}
}

TermPtr TermFactory::mkOr(const TermVec & args) const {
	assert(args.size() > 0);
	if (args.size() == 1) return args[0];
	TermPtr ret = mkFalse();
	for (TermVec::const_iterator it = args.begin(); it != args.end(); ++it) {
		if ((*it)->type == TermType::TRUE) return mkTrue();
		if ((*it)->type == TermType::FALSE) continue;
		ret = mkOr(ret, *it);
	}
	return ret;
}

TermPtr TermFactory::mkAnd(const TermVec & args) const {
	assert(args.size() > 0);
	if (args.size() == 1) return args[0];
	TermPtr ret = mkTrue();
	for (TermVec::const_iterator it = args.begin(); it != args.end(); ++it) {
		if ((*it)->type == TermType::FALSE) return mkFalse();
		if ((*it)->type == TermType::TRUE) continue;
		ret = mkAnd(ret, *it);
	}
	return ret;
}

TermPtr TermFactory::mkOr(TermPtr arg1, TermPtr arg2) const {
	if (arg1->type == TermType::TRUE) return mkTrue();
	if (arg2->type == TermType::TRUE) return mkTrue();
	if (arg1->type == TermType::FALSE) return arg2;
	if (arg2->type == TermType::FALSE) return arg1;
	Term ret(TermType::OR);
	ret.args.push_back(arg1);
	ret.args.push_back(arg2);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkAnd(TermPtr arg1, TermPtr arg2) const {
	if (arg1->type == TermType::TRUE) return arg2;
	if (arg2->type == TermType::TRUE) return arg1;
	if (arg1->type == TermType::FALSE) return mkFalse();
	if (arg2->type == TermType::FALSE) return mkFalse();
	Term ret(TermType::AND);
	ret.args.push_back(arg1);
	ret.args.push_back(arg2);
	return make_shared<Term>(ret);
}

// EQ, NEQ, GEQ, LEQ, LT, GT
TermPtr TermFactory::mkComparison(ComparisonType::E kind, TermPtr arg0,
		TermPtr arg1) const {
	if (arg0->type == TermType::INTEGER && arg1->type == TermType::INTEGER) {
		bool val = false;
		switch (kind) {
		case ComparisonType::EQ:
			val = (arg0->toLong() == arg1->toLong());
			break;
		case ComparisonType::NEQ:
			val = (arg0->toLong() != arg1->toLong());
			break;
		case ComparisonType::GEQ:
			val = (arg0->toLong() >= arg1->toLong());
			break;
		case ComparisonType::LEQ:
			val = (arg0->toLong() <= arg1->toLong());
			break;
		case ComparisonType::LT:
			val = (arg0->toLong() < arg1->toLong());
			break;
		case ComparisonType::GT:
			val = (arg0->toLong() > arg1->toLong());
			break;
		default:
			assert(false);
		}
		return val ? mkTrue() : mkFalse();
	}

	switch (kind) {
	case ComparisonType::EQ: {
		Term ret(TermType::EQ);
		ret.args.push_back(arg0);
		ret.args.push_back(arg1);
		return make_shared<Term>(ret);
	}
	case ComparisonType::NEQ: {
		return mkNot(mkComparison(ComparisonType::EQ, arg0, arg1));
	}
	case ComparisonType::GEQ: {
		return mkComparison(ComparisonType::LEQ, arg1, arg0);
	}
	case ComparisonType::LEQ: {
		Term ret(TermType::LEQ);
		ret.args.push_back(arg0);
		ret.args.push_back(arg1);
		return make_shared<Term>(ret);
	}
	case ComparisonType::LT:
		return mkNot(mkComparison(ComparisonType::GEQ, arg0, arg1));
	case ComparisonType::GT:
		return mkNot(mkComparison(ComparisonType::LEQ, arg0, arg1));
	default:
		assert(false);
		return mkErrorTerm();
	}
}

TermPtr TermFactory::mkPlus(TermPtr arg1, TermPtr arg2) const {
	if (arg1->type == TermType::INTEGER && arg1->toLong() == 0) return arg2;
	if (arg2->type == TermType::INTEGER && arg2->toLong() == 0) return arg1;
	Term ret(TermType::PLUS);
	ret.args.push_back(arg1);
	ret.args.push_back(arg2);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkTimes(TermPtr arg1, TermPtr arg2) const {
	if (arg1->type == TermType::INTEGER && arg1->toLong() == 0) return mkInt(0);
	if (arg2->type == TermType::INTEGER && arg2->toLong() == 0) return mkInt(0);
	if (arg1->type == TermType::INTEGER && arg1->toLong() == 1) return arg2;
	if (arg2->type == TermType::INTEGER && arg2->toLong() == 1) return arg1;
	Term ret(TermType::TIMES);
	ret.args.push_back(arg1);
	ret.args.push_back(arg2);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkMinus(TermPtr arg) const {
	Term ret(TermType::UNARY_MINUS);
	ret.args.push_back(arg);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkMod(long mod, TermPtr arg0, TermPtr arg1) {
	Term ret(TermType::MOD_CONGR);
	ret.args.push_back(arg0);
	ret.args.push_back(arg1);
	ret.val = mod;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::negClause(const AsgnVec & latches) const {
	TermPtr cls = mkFalse();
	for (AsgnVec::const_iterator it = latches.begin(); it != latches.end();
			++it) {
		TermPtr term = asgnToTerm(*it);
		cls = mkOr(cls, mkNot(term));
	}
	return cls;
}

TermPtr TermFactory::negClause(const TermVec & ast) const {
	TermPtr cls = mkFalse();
	for (TermVec::const_iterator it = ast.begin(); it != ast.end(); ++it) {
		cls = mkOr(cls, mkNot(*it));
	}
	return cls;
}

TermPtr TermFactory::copyApp(TermPtr term, TermVec args) const {
	Term ret(term->type);
	ret.args = args;
	ret.boundVars = term->boundVars;
	ret.name = term->name;
	ret.val = term->val;
	return make_shared<Term>(ret);
}

TermPtr TermFactory::mkErrorTerm(void) const {
	return make_shared<Term>(Term(TermType::ERROR));
}

TermPtr TermFactory::mkForAll(const vector<string> & boundVars,
		TermPtr formula) const {
	Term ret(TermType::FORALL);
	ret.boundVars = boundVars;
	ret.args.push_back(formula);
	return make_shared<Term>(ret);
}

TermPtr TermFactory::asgnToTerm(const Assignment & asgn) const {
	TermPtr ret;
	if (asgn.getType() == AsgnType::INT_ASGN) {
		TermPtr number = mkInt(asgn.getNumVal());
		TermPtr var = mkIntVar(asgn.getVar());
		ret = mkComparison(ComparisonType::EQ, var, number);
	} else if (asgn.getType() == AsgnType::BOOL_ASGN) {
		if (asgn.isTrue())
			ret = mkBoolVar(asgn.getVar());
		else
			ret = mkNot(mkBoolVar(asgn.getVar()));
	} else if (asgn.getType() == AsgnType::ARRAY_ASGN) {
		TermPtr var = mkArrayVar(asgn.getVar());
		TermVec andSelects;
		const vector<long> & arrayVal = asgn.getArrayVal();
		for (size_t i = 0; i < arrayVal.size(); ++i) {
			andSelects.push_back(
					mkComparison(ComparisonType::EQ,
							mkArraySelect(var, mkInt(i)), mkInt(arrayVal[i])));
		}
		assert(andSelects.size() > 0);
		ret = mkAnd(andSelects);
	} else if (asgn.getType() == AsgnType::DYN_ARRAY_ASGN) {
		TermPtr var = mkArrayVar(asgn.getVar());
		TermVec andSelects;
		const vector<long> & arrayVal = asgn.getArrayVal();
		for (size_t i = 0; i < arrayVal.size(); ++i) {
			andSelects.push_back(
					mkComparison(ComparisonType::EQ,
							mkArraySelect(var, mkInt(i)), mkInt(arrayVal[i])));
		}
		andSelects.push_back(
				mkComparison(ComparisonType::EQ,
						mkIntVar(asgn.getArraySizeVar()),
						mkInt(asgn.getArraySize())));
		ret = mkAnd(andSelects);
	} else {
		assert(false);
	}
	return ret;
}

} /* namespace CTIGAR */
