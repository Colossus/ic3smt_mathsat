/*
 * SMTWrapper.h
 *
 *  Created on: Sep 5, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef Z3WRAPPER_H_
#define Z3WRAPPER_H_

#include <gmp.h>
#include <stddef.h>
#include <z3.h>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "Assignment.h"
#include "Term.h"

#include "SMTEnvironment.h"

using namespace std;

namespace CTIGAR {

typedef map<TermPtr, Z3_ast> ToZ3Map;
typedef map<size_t, TermPtr> FromZ3Map;

typedef map<string, Z3_func_decl> Z3TypeMap;

class Z3SMTEnvironment: public SMTEnvironment {

	Z3_context ctx;
	Z3_solver slv;

protected:
	VarTypeMap declaredVars;
	Z3TypeMap modelVars;
	vector<Z3_ast> boolConsts;

	vector<Z3_symbol> varSymbols;
	vector<Z3_func_decl> varDecls;

	DynSizeMap dynArrayBounds;
	ReverseSizeMap boundsToArray;
	FixedSizeMap fixedArrayBounds;
	InputSizeMap inputArrayBounds;

	ToZ3Map toZ3;

	TermPtr fromZ3Term(Z3_ast term, vector<string> * boundIndices);

	Z3_ast mkModCongr(long mod, Z3_ast arg0, Z3_ast arg1) const;

	Z3_ast mkTrue(void) const;

	Z3_ast mkFalse(void) const;

	Z3_ast mkIntVar(string name) const;

	Z3_ast mkArrayVar(string name) const;

	Z3_ast mkInt(const long l) const;

	Z3_ast mkArraySelect(Z3_ast var, Z3_ast idx) const;

	Z3_ast mkBoundVar(const string & name,
			const vector<string> & boundVars) const;

	Z3_ast mkForAll(const vector<string> & boundVars, Z3_ast body) const;

	Z3_ast mkArrayStore(Z3_ast var, Z3_ast idx, Z3_ast val) const;

	Z3_ast mkBoolVar(string name) const;

	Z3_ast mkErrorTerm(void) const;

	Z3_ast mkNot(Z3_ast term) const;

	Z3_ast mkOr(size_t size, Z3_ast * args) const;

	Z3_ast mkAnd(size_t size, Z3_ast * args) const;

	Z3_ast mkOr(Z3_ast arg1, Z3_ast arg2) const;

	Z3_ast mkAnd(Z3_ast arg1, Z3_ast arg2) const;

	Z3_ast mkComparison(ComparisonType::E kind, Z3_ast arg0, Z3_ast arg1) const;

	Z3_ast mkMinus(Z3_ast arg) const;

	Z3_ast mkPlus(Z3_ast arg1, Z3_ast arg2) const;

	Z3_ast mkTimes(Z3_ast arg1, Z3_ast arg2) const;

	Z3_ast copyFrom(const Z3SMTEnvironment * src, Z3_ast term) const;

	string varName(Z3_ast term) const;

	bool termIsBoundVar(Z3_ast term) const;

	bool termIsKind(Z3_ast term, TermType::E kind) const;

	bool termIsComparison(Z3_ast term, ComparisonType::E kind) const;

	bool termIsConstArray(Z3_ast term) const;

	bool termIsVar(Z3_ast term) const;

	bool termIsImplication(Z3_ast term) const;

	bool termIsArrayRead(Z3_ast term) const;

	bool termIsArrayWrite(Z3_ast term) const;

	bool termIsNumeral(Z3_ast term) const;

	bool termIsTrue(Z3_ast term) const;

	bool termIsFalse(Z3_ast term) const;

	void getArgs(vector<Z3_ast> & args, Z3_ast term) const;

	long termToLong(Z3_ast term) const;

	bool isErrorTerm(Z3_ast term) const;

	Z3_ast copyApp(Z3_ast term, size_t numArgs, Z3_ast * args) const;

	void findVars(set<string> & occuringVars, Z3_ast term) const;

	void termToNumber(mpz_t * number, Z3_ast term) const;

	void loadNamedTerms(map<string, Z3_ast> & namedMap,
			const string & namedStr) const;

	string numeralToString(Z3_ast term) const;

	Z3_ast termGetArg(Z3_ast term, size_t idx) const;

	Z3_ast getArg(Z3_ast term, size_t idx) const;

	size_t termId(Z3_ast term) const;

	void checkError() const;

	Z3_ast mkActLitAst(void);

	bool extractInteger(long & ret, const Z3_model & m, TermPtr & sharedTerm,
			bool full);

	bool extractBool(bool & ret, const Z3_model & m, Z3_func_decl & varDecl,
			bool full);

	bool extractArrayElem(long & ret, const Z3_model & m,
			Z3_func_decl & varDecl, long idx, bool full);

	string termToString(Z3_ast term) const;

	Z3_ast toZ3Term(const TermPtr term, vector<string> * boundVars);

public:

	Z3SMTEnvironment(string name, const VarTypeMap & vars, const Options & opt,
			DynSizeMap & dynBoundsMap, FixedSizeMap & fixedBoundsMap,
			InputSizeMap & inputBoundsMap);

	~Z3SMTEnvironment();

	TermPtr mkActLit(void);

	virtual TermPtr parsePredicate(const string & str);

	TermPtr fromSmtLib2(string str);

	virtual void assertFormula(TermPtr ast);

	virtual size_t assertFormula(TermPtr ast, TermPtr actLit);

	void assertFormulas(vector<size_t> & ids, const TermVec & ts);

	size_t assertAndTrack(TermPtr ast);

	virtual void release(TermPtr actLit);

	virtual void push(void);

	virtual void pop(void);

	virtual bool sat(void);

	virtual bool sat(const TermVec & assumptions);

	virtual void getBoolModel(SatModel & mod, set<string> & important);

	void getUnsatAssumps(vector<size_t> & assumps) const;

	virtual void getModel(SatModel & mod, bool full);

	size_t termNumArgs(Z3_ast term) const;

	virtual void printAssertedTerms(void) const;

	virtual void declareIntVar(string varName, bool inModel);

	virtual void declareBoolVar(string varName, bool inModel);

	void declareDynArray(string varName, bool inModel, string boundVar);

	void declareInputArray(string varName, bool inModel, TermPtr boundFunction);

	void declareFixedArray(string varName, bool inModel, size_t bound);

	void setRandomSeed(int seed) const;

};

}

#endif /* SMTWRAPPER_H_ */
