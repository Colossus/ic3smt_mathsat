/*
 * Assignment.h
 *
 *  Created on: Aug 29, 2013
 *      Author: Johannes Birgmeier
 */

#ifndef ASSIGNMENT_H_
#define ASSIGNMENT_H_

#include <vector>
#include <string>
#include <gmp.h>
#include <map>
#include <set>
#include <assert.h>

using namespace std;

namespace CTIGAR {

struct AsgnType {
	enum E {
		INT_ASGN, BOOL_ASGN, ARRAY_ASGN, DYN_ARRAY_ASGN
	};
};

class Assignment {

protected:
	string var;
	AsgnType::E asgnType;

	bool boolVal;
	long numVal;
	vector<long> arrayVal;
	string sizeVar;
	long arraySize;

public:

	friend std::ostream & operator<<(std::ostream & stream,
			const Assignment & asgn);

	Assignment(string var, mpz_t val);

	Assignment(string var, long val);

	Assignment(string var, bool val);

	Assignment(string var, const map<long, long> & arrayVal);

	Assignment(string var, const map<long, long> & arrayVal, string sizeVar,
			long arraySize);

	const string & getVar(void) const {
		return var;
	}

	AsgnType::E getType(void) const {
		return asgnType;
	}

	long getNumVal() const {
		assert(asgnType == AsgnType::INT_ASGN);
		return numVal;
	}

	bool isTrue() const {
		assert(asgnType == AsgnType::BOOL_ASGN);
		return boolVal;
	}

	const vector<long> & getArrayVal() const {
		assert(
				asgnType == AsgnType::ARRAY_ASGN
						|| asgnType == AsgnType::DYN_ARRAY_ASGN);
		return arrayVal;
	}

	const string & getArraySizeVar() const {
		return sizeVar;
	}

	long getArraySize() const {
		return arraySize;
	}

};

typedef vector<Assignment> AsgnVec;
typedef set<Assignment> AsgnSet;
typedef set<Assignment> SatModel;

void classifyPoints(map<set<string>, set<AsgnSet> > & pointClasses,
		const set<AsgnSet> points);

std::ostream & operator<<(std::ostream & stream, const Assignment & asgn);

bool operator==(const Assignment & asgn1, const Assignment & asgn2);

bool operator<(const Assignment & asgn1, const Assignment & asgn2);

typedef vector<long> AbstractState;

static bool _AbstractStateComp(const AbstractState & v1,
		const AbstractState & v2) {
	if (v1.size() < v2.size())
		return true;
	if (v1.size() > v2.size())
		return false;
	for (size_t i = 0; i < v1.size(); ++i) {
		if (v1[i] < v2[i])
			return true;
		if (v2[i] < v1[i])
			return false;
	}
	return false;
}

class AbstractStateComp {
public:
	bool operator()(const AbstractState & v1, const AbstractState & v2) const {
		return _AbstractStateComp(v1, v2);
	}
};

}

#endif /* ASSIGNMENT_H_ */
