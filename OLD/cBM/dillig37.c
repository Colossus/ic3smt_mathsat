/*
 * Taken from "Counterexample Driven Refinement for Abstract Interpretation" (TACAS'06) by Gulavani
 */

void main() {
  int x;
  int m;
  int n;
  x = m = 0;
  while(x<=n-1) {
     if(*) {
	m = x;
     }
     x= x+1;
  }
  if(x < n)
    return 0;
  assert(n<1 || m > -1);
  assert(n<1 || m < n);
}
