#! /usr/bin/zsh

function dobm () {
  echo $i INT1/2
  timeout 200 ./CTIGAR $i 1 3 0 3 0 | tee /files/birgmei/out/"$i"_+Ctg-Geo+RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec|Refining|Redundant"
  echo $i INT2/2
  timeout 200 ./CTIGAR $i 1 3 100 3 0 | tee /files/birgmei/out/"$i"_+Ctg+Geo+RSM.txt | egrep "^Level|Elapsed|Property|#|SAT|/sec|Refining|Redundant"
}

if [ $# -ge 1 ]
then
  for i in $*
  do
    dobm
  done
else
  rm -f /files/birgmei/out/intBM/*
  for i in `ls intBM/*.c`
  do
    dobm
  done
fi
