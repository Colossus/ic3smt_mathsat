int main() {
  int c, n, on;

  n = on;
  c = 1;
  while (c != 0) {
    if (n > 100) {
      n = n - 10;
      c = c - 1;
    } else {
      n = n + 11;
      c = c + 1;
    }
  }
  if (on > 100) {
    c = on - 10;
    assert(n == c);
  } else {
    assert(n == 91);
  }
}
