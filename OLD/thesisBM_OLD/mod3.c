main() {
  int y, z;

  if (z < 2) return 0;

  z = z * 3 + 1;
  y = z;
  while (z > 2) {
    z = z - 3;
    y = y - 3;
  }

  assert(y == 1 % 3);
}
