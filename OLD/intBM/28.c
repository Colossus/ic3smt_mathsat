/*
 * From CAV'12 by Sharma et al.
 */

main() {
  int x, y, n;
  x=0;
  y=0;
  n=0;
  while(*) {
      x++;
      y++;
  }
  while(x <= n - 1 || x >= n + 1) {
      x--;
      y--;
  }
  if(x != n)
    return 0;
  assert(y == n);
}
