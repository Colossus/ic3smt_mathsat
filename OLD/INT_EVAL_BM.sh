#! /usr/bin/zsh

function printt () {
  echo -n " & "
  if [ -f /files/birgmei/out/"$i"_"$1".txt ]
  then
    el=`tail -q -n 200 /files/birgmei/out/"$i"_"$1".txt | grep "$sstring"`
    if [ $? -eq 0 ]
    then
      t=`echo -n $el | sed "s/$sstring *//g"`
      printf "%-10s" $t
    else
      printf "%-10s" " "
    fi
  else
    printf "%-10s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

printf "%-25s" "Benchmark"
printf " & "
printf "%-12s" "+Ctg-Geo+RSM"
printf " & "
printf "%-12s" "+Ctg+Geo+RSM"
echo ' \\\\'
printf '\\hline \\\\\n'

for i in `ls intBM/*.c`
do
  printf "%-25s" $i
  printt "+Ctg-Geo+RSM"
  printt "+Ctg+Geo+RSM"
  echo ' \\\\'
done
