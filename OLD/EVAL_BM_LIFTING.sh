#! /usr/bin/zsh

function printt () {
  echo -n " & "
  if [ -f ./out/benchmarks/"$fn"_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep "$sstring"`
    solved="true"
    if [ $sstring = "Elapsed total\\:" ]
    then
      tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep -q "Property holds"
      if [ $? -ne 0 ]
      then
        solved="false"
      fi
    fi
    if [ "$el" != "" ] && [ "$solved" = "true" ]
    then
      t=`echo -n $el | sed "s/$sstring*//g"`
      printf "%-10s" "$t"
    else
      printf "%-10s" " "
    fi
  else
    printf "%-10s" " "
    solved="false"
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

# echo '\\begin{landscape}'
echo '\\begin{center}'
printf '\\begin{longtable}{l||l|l|l|l|l|l|l|l}\n'
printf "%-25s" "Benchmark"
printf " & "
printf "%-10s" "N"
printf " & "
printf "%-10s" "G"
printf " & "
printf "%-10s" "L"
printf " & "
printf "%-10s" "GL"
printf " & "
printf "%-10s" "CPAChecker"
echo ' \\\\'
printf '\\hline \n'
printf '\\endhead \n'

s1=0
s2=0
s3=0
s4=0
s5=0
t1=0
t2=0
t3=0
t4=0
t5=0

incs1() {
  if [ $solved = "true" ]
  then
    ((s1 = s1 + 1))
    ((t1 = t1 + t))
  fi
}

incs2() {
  if [ $solved = "true" ]
  then
    ((s2 = s2 + 1))
    ((t2 = t2 + t))
  fi
}

incs3() {
  if [ $solved = "true" ]
  then
    ((s3 = s3 + 1))
    ((t3 = t3 + t))
  fi
}

incs4() {
  if [ $solved = "true" ]
  then
    ((s4 = s4 + 1))
    ((t4 = t4 + t))
  fi
}

for i in `ls benchmarks/*.c`
do
  fn=`echo $i | sed 's/benchmarks\///g'`
  toprint=`echo $fn | sed 's/_/\\\\_/g'`
  if [ "$toprint" = "sendmail-mime7to8\\_arr\\_three\\_chars\\_no\\_test\\_ok.c" ]
  then
    toprint="sendmail-mime7to8\\_\\_ok.c"
  fi
  printf "%-25s" $toprint
  printt "lifting_+Ctg-Geo+RSM"
  incs1
  printt "lifting_+Ctg+Geo+RSM"
  incs2
  printt "lifting_+Ctg-Geo+RSM+Lazy"
  incs3
  printt "lifting_+Ctg+Geo+RSM+Lazy"
  incs4
  if [ $sstring = "Elapsed total\\:" ]
  then
    echo -n " & "
    time=`cat out/cpachecker/$fn | egrep "user" | sed 's/\(.*\)\(user.*\)/\1/g'`
    if [ "$time" = "" ]
    then
      time=" "
    fi
    cat out/cpachecker/$fn | egrep -q "Verification result: (SAFE|UNSAFE)"
    if [ $? -eq 0 ]
    then
      printf "%-12s" "$time"
      ((s5 = s5 + 1))
      ((t5 = t5 + time))
    else
      if [ "$time" = " " ]
      then
        printf "%-12s" " "
      else
        printf "%-12s" "*"
      fi
    fi
  fi
  if [ $sstring = "Property holds:" ]
  then
    echo -n " & "
    cat out/cpachecker/$fn | egrep -q SAFE
    if [ $? -eq 0 ]
    then
      printf "%-12s" "1"
      ((s5 = s5 + 1))
      ((t5 = t5 + 1))
    else
      printf "%-12s" " "
    fi
  fi
  echo ' \\\\'
done

a1=0
if [ $s1 -gt 0 ]
then
  ((a5 = $t1 / $s1))
fi
a2=0
if [ $s2 -gt 0 ]
then
  ((a5 = $t2 / $s2))
fi
a3=0
if [ $s3 -gt 0 ]
then
  ((a5 = $t3 / $s3))
fi
a4=0
if [ $s4 -gt 0 ]
then
  ((a5 = $t4 / $s4))
fi
a5=0
if [ $s5 -gt 0 ]
then
  ((a5 = $t5 / $s5))
fi

printf "\\hline\n"
printf "%-25s" "Total"
printf " & "
printf "%-10s" $s1
printf " & "
printf "%-10s" $s2
printf " & "
printf "%-10s" $s3
printf " & "
printf "%-10s" $s4
printf " & "
printf "%-10s" $s5
echo ' \\\\'
printf "%-25s" "Sum"
printf " & "
printf "%-10.2f" $t1
printf " & "
printf "%-10.2f" $t2
printf " & "
printf "%-10.2f" $t3
printf " & "
printf "%-10.2f" $t4
printf " & "
printf "%-10.2f" $t5
echo ' \\\\'
printf "%-25s" "Average"
printf " & "
printf "%-10.2f" $a1
printf " & "
printf "%-10.2f" $a2
printf " & "
printf "%-10.2f" $a3
printf " & "
printf "%-10.2f" $a4
printf " & "
printf "%-10.2f" $a5
echo ' \\\\'

printf '\\end{longtable}\n'
echo '\\end{center}'
# echo '\\end{landscape}'
