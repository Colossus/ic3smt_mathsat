#! /usr/bin/zsh

rm -rf thesisBM/*.vars
rm -rf thesisBM/*.nondet
rm -rf thesisBM/*.asrts
rm -rf thesisBM/*.abstr
rm -rf thesisBM/*.tr

for i in `ls thesisBM`
do
  echo $i
  ./trgen2 thesisBM/$i
done

