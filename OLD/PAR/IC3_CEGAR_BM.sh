#! /usr/bin/zsh

control_c()
# run if user hits control-c
{
  killall IC3_CEGAR-$$
  exit $?
}
 
# trap keyboard interrupt (control-c)
trap control_c SIGINT

function dobm () {
  # echo -n "echo $i 1/7 ; " >> /tmp/commands_birgmei_$$
  # echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 0 0 0 0 0 > out/"$i"_-Ctg-Geo-RSM.txt" >> /tmp/commands_birgmei_$$
  # echo " ; echo $i 1/7 finished" >> /tmp/commands_birgmei_$$
  # echo -n "echo $i 2/7 ; " >> /tmp/commands_birgmei_$$
  # echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 0 0 0 > out/"$i"_+Ctg-Geo-RSM.txt"  >> /tmp/commands_birgmei_$$
  # echo " ; echo $i 2/7 finished" >> /tmp/commands_birgmei_$$
  # echo -n "echo $i 3/7 ; " >> /tmp/commands_birgmei_$$
  # echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 100 0 0 > out/"$i"_+Ctg+Geo-RSM.txt" >> /tmp/commands_birgmei_$$
  # echo " ; echo $i 3/7 finished" >> /tmp/commands_birgmei_$$
  echo -n "echo $i 4/7 ; " >> /tmp/commands_birgmei_$$
  echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 0 3 0 > out/"$i"_+Ctg-Geo+RSM.txt" >> /tmp/commands_birgmei_$$
  echo " ; echo $i 4/7 finished" >> /tmp/commands_birgmei_$$
  echo -n "echo $i 5/7 ; " >> /tmp/commands_birgmei_$$
  echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 100 3 0 > out/"$i"_+Ctg+Geo+RSM.txt" >> /tmp/commands_birgmei_$$
  echo " ; echo $i 5/7 finished" >> /tmp/commands_birgmei_$$
  echo -n "echo $i 6/7 ; " >> /tmp/commands_birgmei_$$
  echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 0 3 3 > out/"$i"_+Ctg-Geo+RSM+Lazy.txt" >> /tmp/commands_birgmei_$$
  echo " ; echo $i 6/7 finished" >> /tmp/commands_birgmei_$$
  echo -n "echo $i 7/7 ; " >> /tmp/commands_birgmei_$$
  echo -n "timeout 1200 ./IC3_CEGAR-$$ $i 1 3 100 3 3 > out/"$i"_+Ctg+Geo+RSM+Lazy.txt" >> /tmp/commands_birgmei_$$
  echo " ; echo $i 7/7 finished" >> /tmp/commands_birgmei_$$
}

rm -f /tmp/commands_birgmei_$$
rm -f ./IC3_CEGAR-$$
cp ./IC3_CEGAR ./IC3_CEGAR-$$

if [ $# -ge 1 ]
then
  for i in $*
  do
    dobm
  done
else
  for i in `ls thesisBM/*.c`
  do
    dobm
  done
fi

cat /tmp/commands_birgmei_$$ | xargs -n 1 -d '\n' -P 28 sh -c
