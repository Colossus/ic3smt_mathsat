#! /usr/bin/zsh

function printt () {
  echo -n " & "
  if [ -f ./out/"$i"_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/"$i"_"$1".txt | grep "$sstring"`
    if [ $? -eq 0 ]
    then
      t=`echo -n $el | sed "s/$sstring *//g"`
      printf "%-12s" $t
    else
      printf "%-12s" " "
    fi
  else
    printf "%-12s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\:"
else
  sstring=$1
fi

printf '\\begin{tabular}{l||l|l|l|l|l|l|l}\n'
printf "%-25s" "Benchmark"
printf " & "
# printf "%-12s" "-C-G-R"
# printf " & "
# printf "%-12s" "+C-G-R"
# printf " & "
# printf "%-12s" "+C+G-R"
# printf " & "
printf "%-12s" "+C-G+R"
printf " & "
printf "%-12s" "+C+G+R"
printf " & "
printf "%-12s" "+C-G+R+L"
printf " & "
printf "%-12s" "+C+G+R+L"
echo ' \\\\'
printf '\\hline \n'

for i in `ls thesisBM/*.c`
do
  toprint=`echo $i | sed 's/thesisBM\///g'`
  printf "%-25s" $toprint
#   printt "-Ctg-Geo-RSM"
#   printt "+Ctg-Geo-RSM"
#   printt "+Ctg+Geo-RSM"
  printt "+Ctg-Geo+RSM"
  printt "+Ctg+Geo+RSM"
  printt "+Ctg-Geo+RSM+Lazy"
  printt "+Ctg+Geo+RSM+Lazy"
  echo ' \\\\'
done
printf '\\end{tabular}\n'
