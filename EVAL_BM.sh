#! /usr/bin/zsh

function printt () {
  echo -n " & "
  solved="false"
  t=0
  if [ -f ./out/benchmarks/"$fn"_"$1".txt ]
  then
    el=`tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep "$sstring"`
    if [ $sstring = "Elapsed total\\: " ]
    then
      toSeek="Property holds: "
    else
      toSeek=$sstring
    fi
    tail -q -n 200 ./out/benchmarks/"$fn"_"$1".txt | grep -q "$toSeek"
    if [ $? -eq 0 ]
    then
      solved="true"
    fi
    if [ "$el" != "" ] && [ "$solved" = "true" ]
    then
      t=`echo -n $el | sed "s/$sstring*//g"`
      printf "%-10s" "$t"
    else
      printf "%-10s" " "
    fi
  else
    printf "%-10s" " "
  fi
}

if [ $# -eq 0 ]
then
  sstring="Elapsed total\\: "
else
  sstring=$1
fi

# echo '\\begin{landscape}'
echo '\\begin{center}'
printf '\\begin{longtable}{l||l|l|l|l|l|l|l}\n'
printf "%-25s" "Benchmark"
printf " & "
printf "%-10s" "CE"
printf " & "
printf "%-10s" "CL"
printf " & "
printf "%-10s" "CAE"
printf " & "
printf "%-10s" "CAL"
printf " & "
printf "%-10s" "LLE"
printf " & "
printf "%-10s" "LLL"
printf " & "
printf "%-10s" "LCE"
printf " & "
printf "%-10s" "LCL"
printf " & "
printf "%-10s" "CPAChecker"
echo ' \\\\'
printf '\\hline \n'
printf '\\endhead \n'

s1=0
s2=0
s3=0
s4=0
s5=0
s6=0
s7=0
s8=0
s9=0
t1=0
t2=0
t3=0
t4=0
t5=0
t6=0
t7=0
t8=0
t9=0

incs1() {
  if [ $solved = "true" ]
  then
    ((s1 = s1 + 1))
    ((t1 = t1 + t))
  fi
}

incs2() {
  if [ $solved = "true" ]
  then
    ((s2 = s2 + 1))
    ((t2 = t2 + t))
  fi
}

incs3() {
  if [ $solved = "true" ]
  then
    ((s3 = s3 + 1))
    ((t3 = t3 + t))
  fi
}

incs4() {
  if [ $solved = "true" ]
  then
    ((s4 = s4 + 1))
    ((t4 = t4 + t))
  fi
}

incs5() {
  if [ $solved = "true" ]
  then
    ((s5 = s5 + 1))
    ((t5 = t5 + t))
  fi
}

incs6() {
  if [ $solved = "true" ]
  then
    ((s6 = s6 + 1))
    ((t6 = t6 + t))
  fi
}

incs7() {
  if [ $solved = "true" ]
  then
    ((s7 = s7 + 1))
    ((t7 = t7 + t))
  fi
}

incs8() {
  if [ $solved = "true" ]
  then
    ((s8 = s8 + 1))
    ((t8 = t8 + t))
  fi
}

for i in `ls benchmarks/*.c`
do
  fn=`echo $i | sed 's/benchmarks\///g'`
  toprint=`echo $fn | sed 's/_/\\\\_/g'`
  if [ "$toprint" = "sendmail-mime7to8\\_arr\\_three\\_chars\\_no\\_test\\_ok.c" ]
  then
    toprint="sendmail-mime7to8\\_\\_ok.c"
  fi
  printf "%-25s" $toprint
  printt "cons-concr-pred_+Ctg-Geo+RSM"
  incs1
  printt "cons-concr-pred_+Ctg-Geo+RSM+Lazy"
  incs2
  printt "cons-abstr-pred_+Ctg-Geo+RSM"
  incs3
  printt "cons-abstr-pred_+Ctg-Geo+RSM+Lazy"
  incs4
  printt "lift-laf_+Ctg-Geo+RSM"
  incs5
  printt "lift-laf_+Ctg-Geo+RSM+Lazy"
  incs6
  printt "lift-caf_+Ctg-Geo+RSM"
  incs7
  printt "lift-caf_+Ctg-Geo+RSM+Lazy"
  incs8
  if [ $sstring = "Elapsed total\\: " ]
  then
    echo -n " & "
    time=`cat out/cpachecker/$fn | egrep "user" | sed 's/\(.*\)\(user.*\)/\1/g'`
    if [ "$time" = "" ]
    then
      time=" "
    fi
    cat out/cpachecker/$fn | egrep -q "Verification result: (SAFE|UNSAFE)"
    if [ $? -eq 0 ]
    then
      printf "%-12s" "$time"
      ((s9 = s9 + 1))
      ((t9 = t9 + time))
    else
      if [ "$time" = " " ]
      then
        printf "%-12s" " "
      else
        printf "%-12s" "*"
      fi
    fi
  fi
  if [ $sstring = "Property holds:" ]
  then
    echo -n " & "
    cat out/cpachecker/$fn | egrep -q SAFE
    if [ $? -eq 0 ]
    then
      printf "%-12s" "1"
      ((s9 = s9 + 1))
      ((t9 = t9 + 1))
    else
      printf "%-12s" " "
    fi
  fi
  echo ' \\\\'
done

((a1 = $t1 / $s1))
((a2 = $t2 / $s2))
((a3 = $t3 / $s3))
((a4 = $t4 / $s4))
((a5 = $t5 / $s5))
((a6 = $t6 / $s6))
((a7 = $t7 / $s7))
((a8 = $t8 / $s8))
a9=0
if [ $s9 -gt 0 ]
then
  ((a9 = $t9 / $s9))
fi

printf "\\hline\n"
printf "%-25s" "Total"
printf " & "
printf "%-10s" $s1
printf " & "
printf "%-10s" $s2
printf " & "
printf "%-10s" $s3
printf " & "
printf "%-10s" $s4
printf " & "
printf "%-10s" $s5
printf " & "
printf "%-10s" $s6
printf " & "
printf "%-10s" $s7
printf " & "
printf "%-10s" $s8
printf " & "
printf "%-10s" $s9
echo ' \\\\'
printf "%-25s" "Sum"
printf " & "
printf "%-10.2f" $t1
printf " & "
printf "%-10.2f" $t2
printf " & "
printf "%-10.2f" $t3
printf " & "
printf "%-10.2f" $t4
printf " & "
printf "%-10.2f" $t5
printf " & "
printf "%-10.2f" $t6
printf " & "
printf "%-10.2f" $t7
printf " & "
printf "%-10.2f" $t8
printf " & "
printf "%-10.2f" $t9
echo ' \\\\'
printf "%-25s" "Average"
printf " & "
printf "%-10.2f" $a1
printf " & "
printf "%-10.2f" $a2
printf " & "
printf "%-10.2f" $a3
printf " & "
printf "%-10.2f" $a4
printf " & "
printf "%-10.2f" $a5
printf " & "
printf "%-10.2f" $a6
printf " & "
printf "%-10.2f" $a7
printf " & "
printf "%-10.2f" $a8
printf " & "
printf "%-10.2f" $a9
echo ' \\\\'

printf '\\end{longtable}\n'
echo '\\end{center}'
# echo '\\end{landscape}'
