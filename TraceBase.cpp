/*
 * TraceBase.cpp
 *
 *  Created on: May 19, 2014
 *      Author: birgmei
 */

#include "TraceBase.h"

#include <iostream>
#include <assert.h>

#include "State.h"

using namespace std;

namespace CTIGAR {

std::ostream & operator<<(std::ostream & stream, const TraceBase & trB) {
	for (TraceBaseMap::const_iterator it = trB.getMap().begin();
			it != trB.getMap().end(); ++it) {
		stream << "(TraceBase) " << it->first << ": " << endl;
		for (set<AsgnSet>::const_iterator it2 = it->second.begin();
				it2 != it->second.end(); ++it2) {
			stream << "(TraceBase) \t" << *it2 << endl;
		}
	}
	return stream;
}

void TraceBase::fillInState(size_t pc, const AsgnVec & st, size_t minSize) {
	AsgnSet av;
	for (AsgnVec::const_iterator it = st.begin(); it != st.end(); ++it) {
		av.insert(*it);
	}
	if (av.size() >= minSize)
		tbm[pc].insert(av);
}

}
