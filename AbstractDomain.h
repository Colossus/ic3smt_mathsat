/*
 * AbstractDomain.h
 *
 *  Created on: May 14, 2014
 *      Author: birgmei
 */

#ifndef ABSTRACTDOMAIN_H_
#define ABSTRACTDOMAIN_H_

#include <stddef.h>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <vector>

#include "Assignment.h"
#include "Domain.h"
#include "Model.h"
#include "Term.h"

namespace CTIGAR {

class AbstractDomain {
protected:
	Domain * domain;
	Model & model;

	const Options & opt;
	TermFactory tf;

	long negInit;

	void reducePcRestrictions(AbstractState & ast);

public:
	SMTEnvironment * env;
	unsigned nAbstractions;

	AbstractDomain(Model & model, SMTEnvironment * env, const Options & opt);

	virtual ~AbstractDomain();

	long negatedInitial(void) {
		return negInit;
	}

	void abstract(AbstractState & ret, const AsgnVec & state);

	void toTermVec(TermVec & vec, const AbstractState & st, PredicateMap * pm =
	NULL) const;

	TermPtr toTerm(const AbstractState & ast) const;

	bool refine(TermPtr negInterpolant);

	TermPtr predicate(size_t idx) const;

	TermPtr literal(long idx) const;

	const Predicate & node(size_t idx);

	void satModelToAst(AbstractState & ret, const SatModel & in);

	unsigned removeUnsupported(AbstractState & ret,
			const vector<size_t> & asserted);

	friend std::ostream & operator<<(std::ostream & stream,
			const AbstractDomain & d) {
		for (vector<Predicate *>::const_iterator it =
				d.domain->getPreds().begin(); it != d.domain->getPreds().end();
				++it) {
			stream << *(*it) << endl;
		}
		return stream;
	}

	size_t size(void) const {
		return domain->size();
	}

	void getOccurringVariables(set<string> & vars,
			const AbstractState & ast) const;

	void getRelevantVariables(set<string> & vars,
			const AbstractState & ast) const;

	void getOccurringVariables(set<string> & vars, const long pred) const;

	void loadTrx(void);

	void loadRecommendedAbstractions(void);

};

class IneqDomain: public AbstractDomain {

public:
	IneqDomain(Model & model, SMTEnvironment * env, Options & opt);

	virtual ~IneqDomain();
}
;

class KarrIneqDomain: public IneqDomain {

public:
	KarrIneqDomain(Model & model, SMTEnvironment * env, Options & opt);

	virtual ~KarrIneqDomain();
}
;

class DefaultDomain: public AbstractDomain {

public:
	DefaultDomain(Model & model, SMTEnvironment * env, Options & opt);

	virtual ~DefaultDomain();
};

}

#endif /* ABSTRACTDOMAIN_H_ */
