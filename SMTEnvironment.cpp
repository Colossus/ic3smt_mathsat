/*
 * SMTEnvironment.cpp
 *
 *  Created on: Apr 15, 2014
 *      Author: birgmei
 */

#include "SMTEnvironment.h"
#include <sys/times.h>

namespace CTIGAR {

SMTEnvironment::SMTEnvironment(const string & name, const Options & opt) :
		name(name), opt(opt), timer(time()), t(0), nQuery(0), numBT(0), nActLit(
				0) {
}

SMTEnvironment::~SMTEnvironment() {
}

clock_t SMTEnvironment::time() {
	struct tms time1;
	times(&time1);
	return time1.tms_utime;
}

void SMTEnvironment::startTimer() {
	timer = time();
}

void SMTEnvironment::endTimer(void) {
	t += (time() - timer);
}

clock_t SMTEnvironment::satTime() {
	return t;
}

void SMTEnvironment::setSatTime(clock_t time1) {
	t = time1;
}

void SMTEnvironment::setNQuery(size_t oldQuery) {
	nQuery = oldQuery;
}

size_t SMTEnvironment::getNQuery() {
	return nQuery;
}

int SMTEnvironment::getNumBT(void) {
	return numBT;
}

void SMTEnvironment::releaseMultiple(const TermVec & actLits) {
	for (TermVec::const_iterator it = actLits.begin();
			it != actLits.end(); ++it) {
		release(*it);
	}
}

} /* namespace CTIGAR */
