/*
 * util.cpp
 *
 *  Created on: Jan 3, 2014
 *      Author: johannes
 */

#include "myutil.h"

#include <sstream>

using namespace std;

namespace CTIGAR {

void split(set<string> & elems, const string & s, char delim) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.insert(item);
	}
}

bool isNumber(string str) {
	for (string::const_iterator it = str.begin(); it != str.end(); ++it) {
		if (!isdigit(*it))
			return false;
	}
	return true;
}

}
