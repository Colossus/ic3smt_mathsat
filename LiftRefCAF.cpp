/*
 * ConsecutionRefinement.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

using namespace std;

#include "LiftRefCAF.h"

namespace CTIGAR {

LiftRefCAF::LiftRefCAF(Model & model, AbstractDomain & d, Options & opt) :
		LiftRef(model, d, opt) {
}

LiftRefCAF::~LiftRefCAF() {
}

// Process obligations according to priority.
bool LiftRefCAF::handleObligations(PriorityQueue obls) {
	while (!obls.empty()) {
		PriorityQueue::iterator obli = obls.begin();
		Obligation obl = *obli;
		assert(obl.level <= k);

		updateAbstraction(obl.state);
		// Is it the beginning of a counterexample trace?
		if (obl.level == 0) {
			bool init = true;
			if (smgr.state(obl.state).hasLiftedAst()) {
				init = initiation(smgr.state(obl.state).liftedAst);
			} else {
				init = initiation(smgr.state(obl.state).lifted);
			}

			if (!init) {
				if (smgr.state(obl.state).nSpurious == 0) {
					cexState = obl.state;
					return false;
				}
				backtrackRefine(obl, obls);
				continue;
			}
		}

		if (verbosity >= 1)
			cout << "Trying to prove abstract consecution at level "
					<< obl.level << " with state " << smgr.state(obl.state)
					<< endl;

		size_t abstractPred;
		size_t concretePred;
		AbstractState core;
		assert(obl.state != 0);
		if (abstractConsecution(obl.level,
				smgr.state(obl.state).bestAbstraction(), obl.state, &core,
				&abstractPred)) {
			generalizeErase(obl, obli, obls, core);
		} else if (smgr.state(obl.state).hasLiftedAst()) {
			if (!predecessor(obl, obls, abstractPred)) return false;
		} else if (consecution(obl.level, smgr.state(obl.state).lifted,
				obl.state, &concretePred)) {
			if (smgr.state(obl.state).nSpurious >= maxSpurious || obl.level == 0
					|| !initiation(smgr.state(abstractPred).lifted)) {
				// If the most precise abstraction admits spurious transitions,
				// refine the domain to eliminate those
				elimSpuriousTrans(obl.state);
				// clear queue: refinements might shrink successor states, thus making their alternate predecessors worthless
				obls.clear();
			} else {
				spuriousPredecessor(obl, obls, abstractPred);
			}
		} else {
			if (!predecessor(obl, obls, concretePred)) return false;
		}
	}
	return true;
}

}
