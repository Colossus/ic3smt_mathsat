/*
 * ConsecutionRefinement.h
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#ifndef CONSREFCONCRPRED_H_
#define CONSREFCONCRPRED_H_

#include "ConsRef.h"

using namespace std;

namespace CTIGAR {

class ConsRefConcrPred: public ConsRef {

public:
	ConsRefConcrPred(Model & model, AbstractDomain & d, Options & opt);

	virtual ~ConsRefConcrPred();

protected:
	virtual bool checkCEXTrace(void);

	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls);

	// returns true if state or some successor was updated, and false otherwise
	virtual bool updateAbstraction(size_t st);

};

}

#endif /* CONSECUTIONREFINEMENT_H_ */
