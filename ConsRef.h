/*
 * ConsecutionRefinement.h
 *
 *  Created on: Jan 9, 2014
 *      Author: johannes
 */

#ifndef ConsRef_H_
#define ConsRef_H_

#include <memory>

#include "CTIGAR.h"
#include "Term.h"
#include "Model.h"
#include "main.h"
#include "Assignment.h"

using namespace std;

namespace CTIGAR {

class ConsRef: public CTIGAR {

protected:

	TermFactory tf;

public:
	ConsRef(Model & model, AbstractDomain & d, Options & opt);

	virtual ~ConsRef();

protected:

	// Process obligations according to priority.
	virtual bool handleObligations(PriorityQueue obls) = 0;

	void elimSpuriousTrans(size_t st, size_t level, bool enforceRefinement =
			true);

	void removeObligations(size_t st, size_t depth, PriorityQueue & obls);

	virtual void backtrackRefine(const Obligation & obl, PriorityQueue & obls,
			bool enforceRefinement = true);

	// returns true if state or some successor was updated, and false otherwise
	virtual bool updateAbstraction(size_t st) = 0;

	bool interpolate(TermPtr & ret, size_t j, const AsgnVec & s);

};

}

#endif /* CONSECUTIONREFINEMENT_H_ */
